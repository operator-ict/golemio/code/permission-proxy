# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.4.2] - 2024-01-17

### Changed

-   Mark Permission Proxy as deprecated and no longer maintained ([general#498](https://gitlab.com/operator-ict/golemio/code/general/-/issues/498))

### Removed

-   Links to Apiary documentation

## [2.4.1] - 2023-07-31

### Added

-   reloading definitions trough pub sub redis ([permission-proxy#159](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/159))

### Changed

-   NodeJS 18.17.0

## [2.4.0] - 2023-07-17

### Changed

-   Run dockerized service via node process ([permission-proxy#158](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/158))

### Added

-   Opentelemetry

## [2.3.10] - 2023-06-19

### Changed

-   Change invite email template ([admin-panel-frontend#4](https://gitlab.com/operator-ict/golemio/code/admin-panel-frontend/-/issues/4))
-   Replace Number() with parseInt()

## [2.3.9] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.3.8] - 2023-06-05

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.3.7] - 2023-05-24

### Added

-   Inspect utils for remote debugging

## [2.3.6] - 2023-05-10

### Added

-   Rate limit on role ([permission-proxy#150](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/150))

## [2.3.5] - 2023-04-26

-   No changelog

## [2.3.4] - 2023-04-12

### Changed

-   Update view v_containers_users ([p0149](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/283))

## [2.3.3] - 2023-03-29

### Added

-   Min-max scope accepts date and datetime value ([admin-panel-frontend#19](https://gitlab.com/operator-ict/golemio/code/admin-panel-frontend/-/issues/3))

## [2.3.2] - 2023-03-15

-   No changelog

## [2.3.1] - 2023-03-06

### Changed

-   Update view v_containers_users, new role_id

## [2.3.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Node.js-next to v19.6.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.2.1] - 2023-01-30

### Added

-   Added view v_gbi_daily_login_counts ([Golemio BI - frontend#26](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/26))

## [2.2.0] - 2023-01-23

### Changed

-   Changed view v_gbi_last_login (https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/24)
-   Docker image optimization
-   Migrate to npm

## [2.1.1] - 2023-01-18

### Added

-   Changed (v_users_golemio_bi) and added new view (v_gbi_last_login) (operator-ict/golemio/code/golemio-bi/frontend#24)

### Changed

-   Changed view v_containers_users (https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/263)

## [2.1.0] - 2023-01-13

### Added

-   Add PROXY_PAYLOAD_LIMIT to axios proxy calls ([#153](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/153))

## [2.0.21] - 2022-12-13

### Changed

-   Update Sequelize / imported from Core Module

## [2.0.20] - 2022-11-29

### Added

-   Golemio BI user Login Event Logger ([Golemio BI - backend#38](https://gitlab.com/operator-ict/golemio/code/golemio-bi/backend/-/issues/38))

### Changed

-   Limit serve-static middleware only to certain routes ([vp/og#1](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/-/issues/1))

## [2.0.19] - 2022-11-14

### Added

-   Add view `v_users_golemio_bi` ([gbi-fe#21](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/21))

### Changed

-   Update TypeScript to v4.7.2

## [2.0.18] - 2022-10-04

### Fixed

-   Forward content-type from non-gzipped error responses ([core#40](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/40))

## [2.0.17] - 2022-09-21

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418)

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [2.0.16] - 2022-09-01

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))

### Fixed

-   Forward headers from gzipped error responses ([pid#153](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/153))

## [2.0.15] - 2022-06-09

### Changed

-   View v_containers_users filters out deleted and non-active users
-   Update dependencies: redis to 6.2 and postgres to 13.6, update typescript

## [2.0.14] - 2022-05-02

### Removed

-   ip-filter dependency
-   ip and x-forwarded-for limitations ([#149](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/149))

### Changed

-   Use ESLint, update dev dependencies

## [2.0.13] - 2022-03-16

### Fixed

-   Fix api key revoking ([#151](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/151))

## [2.0.12] - 2022-03-02

### Fixed

-   Fix proxy api call controller (disable decompression by axios)

## [2.0.11] - 2022-03-02

### Fixed

-   Fix axios abort controller

## [2.0.10] - 2022-03-01

### Changed

-   Update to Node.js 16 ([permission-proxy#146](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/146))

## [2.0.9] - 2022-02-16

### Remove

-   Stop serving x-powered-by header
-   Request-promise deprecated to Axios ([permission-proxy#147](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/147))

