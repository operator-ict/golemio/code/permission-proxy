TRUNCATE
    rate_limit,
    forgotten_password,
    invitation,
    route,
    api_key,
    user_role,
    role,
    scope,
    "user",
    referer_whitelist
    RESTART IDENTITY;

INSERT INTO "user" (email, password, name, surname, admin, invitation_accepted_at) VALUES
('admin@admin.com', '$2b$10$depMND/SzWEWCGN9nynBT.9AdJx48.NWeQImIW535IvZGWFna8CGW', 'Admin', 'admin', true, NOW()),
('admin@operatoricte2e.cz', '$2a$10$8QN.yfwQNGAdslkfamljq.4DWppqJCBNDAshB8cRn0pXp2Zqmhy82', 'Admine2e', 'admine2e', true, NOW()),
('user@user.com', '$2b$10$AWQKgIZzCvVXXtAwmYGWwuK0r/pAfgOKOhxi.x0RQs19lnVHLuse.', 'user', 'user', false, NOW());

INSERT INTO route (endpoint, target_url, method, target_path, public) VALUES
('/traffic-cameras', 'http://localhost:3100', 'GET', NULL, false),
('/faulty', 'http://localhost:3100', 'GET', '/faulty', true),
('/faulty-gzipped', 'http://localhost:3100', 'GET', '/faulty-gzipped', true);

INSERT INTO role (label, description, "default") VALUES
('Kamery', '', false),
('Opendatar', 'Default role pro uzivatele, kteri se registruji pres GUI.', true);

INSERT INTO scope (limitations, role_id, route_id) VALUES
('[
  {
    "name": "district",
    "block": true,
    "values": [
      "praha-7",
      "praha-4"
    ],
    "type": "in-array"
  }
]' :: jsonb, 1, 1);

INSERT INTO user_role (role_id, user_id) VALUES (1, 3);

INSERT INTO referer_whitelist (referer) VALUES ('http://domain1.cz/api-keys'), ('https://domain2/client-panel');
