-- Table: referer_whitelist
CREATE TABLE IF NOT EXISTS "referer_whitelist"
(
    id serial CONSTRAINT referer_whitelist_pk PRIMARY KEY,
    referer varchar(255) NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS referer_whitelist_referer_uindex
    ON referer_whitelist (referer);


-- Table: user
CREATE TABLE IF NOT EXISTS "user"
(
    id serial PRIMARY KEY,
    email varchar(100) NOT NULL,
    password text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    invitation_accepted_at timestamp with time zone,
    name varchar(50),
    surname varchar(50),
    admin boolean NOT NULL DEFAULT false,
    initial_referer integer,
    CONSTRAINT user_initial_referer_fkey FOREIGN KEY (initial_referer) REFERENCES referer_whitelist (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS user_email_uindex ON "user"(email) WHERE (deleted_at IS NULL);


-- Table: role
CREATE TABLE IF NOT EXISTS "role"
(
    id serial PRIMARY KEY,
    label varchar(64) NOT NULL,
    parent_id integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    created_by_user_id integer,
    updated_by_user_id integer,
    description text,
    "default" boolean DEFAULT false,
    CONSTRAINT role_role_id_fk FOREIGN KEY (parent_id)
        REFERENCES role (id),
    CONSTRAINT role_created_by_fk FOREIGN KEY (created_by_user_id)
        REFERENCES "user" (id),
    CONSTRAINT role_updated_by_fk FOREIGN KEY (updated_by_user_id)
        REFERENCES "user" (id)
);


-- Table: user_role
CREATE TABLE IF NOT EXISTS "user_role"
(
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    created_by_user_id integer,
    updated_by_user_id integer,
    UNIQUE (user_id, role_id),
    CONSTRAINT user_role_user_id_fk FOREIGN KEY (user_id)
        REFERENCES "user" (id),
    CONSTRAINT user_role_role_id_fk FOREIGN KEY (role_id)
        REFERENCES role (id),
    CONSTRAINT user_role_created_by_fk FOREIGN KEY (created_by_user_id)
        REFERENCES "user" (id),
    CONSTRAINT user_role_updated_by_fk FOREIGN KEY (updated_by_user_id)
        REFERENCES "user" (id)
);


-- Table: api_key
CREATE TABLE IF NOT EXISTS "api_key"
(
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    created_at timestamp,
    updated_at timestamp,
    deleted_at timestamp,
    code text,
    UNIQUE (code),
    CONSTRAINT api_key_user_id_fk FOREIGN KEY (user_id)
        REFERENCES "user" (id)
        ON DELETE CASCADE
);


-- Table: route
CREATE TABLE IF NOT EXISTS "route"
(
    id serial PRIMARY KEY,
    endpoint varchar(100) NOT NULL,
    method varchar(10) NOT NULL,
    target_url text NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    created_by_user_id integer,
    updated_by_user_id integer,
    target_path text,
    "public" boolean DEFAULT false,
    is_redirect boolean DEFAULT false,
    UNIQUE (endpoint, method),
    CONSTRAINT route_created_by_fk FOREIGN KEY (created_by_user_id)
        REFERENCES "user" (id),
    CONSTRAINT route_updated_by_fk FOREIGN KEY (updated_by_user_id)
        REFERENCES "user" (id)
);


-- Table: invitation
CREATE TABLE IF NOT EXISTS "invitation"
(
    id serial PRIMARY KEY,
    user_id integer NOT NULL REFERENCES "user",
    code varchar(50) NOT NULL,
    expiration timestamp with time zone NOT NULL,
    UNIQUE (user_id),
    UNIQUE (code)
);


-- Table: scope
CREATE TABLE IF NOT EXISTS "scope"
(
    id serial PRIMARY KEY,
    limitations jsonb NOT NULL DEFAULT '[]'::jsonb,
    role_id integer NOT NULL,
    route_id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    created_by_user_id integer,
    updated_by_user_id integer,
    UNIQUE (role_id, route_id),
    CONSTRAINT scope_role_id_fk FOREIGN KEY (role_id)
        REFERENCES role (id),
    CONSTRAINT scope_route_id_fk FOREIGN KEY (route_id)
        REFERENCES route (id)
        ON DELETE CASCADE,
    CONSTRAINT scope_created_by_fk FOREIGN KEY (created_by_user_id)
        REFERENCES "user" (id),
    CONSTRAINT scope_updated_by_fk FOREIGN KEY (updated_by_user_id)
        REFERENCES "user" (id)
);


-- Table: forgotten_password
CREATE TABLE IF NOT EXISTS "forgotten_password"
(
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    code varchar(50) NOT NULL,
    expiration timestamp with time zone NOT NULL,
    UNIQUE (user_id),
    UNIQUE (code),
    CONSTRAINT forgotten_password_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES "user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


-- Table: rate_limit
CREATE TABLE IF NOT EXISTS "rate_limit"
(
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    "limit" integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    timewindow integer NOT NULL,
    CONSTRAINT rate_limit_user_id_fk FOREIGN KEY (user_id)
        REFERENCES "user" (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS rate_limit_user_id_uindex
    ON rate_limit (user_id);


-- Insert default 'Opendatar' role if default does not exist
INSERT INTO role (label, created_at, updated_at, description, "default")
SELECT 'Opendatar', now(), now(), 'Default role pro uzivatele, kteri se registruji pres GUI.', true
WHERE
    NOT EXISTS (
        SELECT "default" FROM role WHERE "default" = true
    );
