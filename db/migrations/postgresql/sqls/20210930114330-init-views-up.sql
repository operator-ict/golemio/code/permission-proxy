-- View: v_all_user_role_route_distinct
CREATE OR REPLACE VIEW v_all_user_role_route_distinct AS
    SELECT DISTINCT u.id AS user_id,
        rt.endpoint,
        rt.method,
        rt.id AS route_id
    FROM "user" u
         LEFT JOIN user_role ur ON u.id = ur.user_id
         LEFT JOIN role r ON r.id = ur.role_id
         JOIN scope s ON s.role_id = r.id OR s.role_id = r.parent_id
         LEFT JOIN route rt ON rt.id = s.route_id
    WHERE u.deleted_at IS NULL AND r.deleted_at IS NULL AND s.deleted_at IS NULL;


-- View: v_user_role_route
CREATE OR REPLACE VIEW v_user_role_route AS
    SELECT u.id AS user_id,
        u.email,
        r.label,
        rt.endpoint,
        rt.method,
        rt.target_url,
        s.limitations,
        r.id AS role_id,
        rt.id AS route_id,
        r.parent_id AS role_parent_id
    FROM "user" u
        LEFT JOIN user_role ur ON u.id = ur.user_id
        LEFT JOIN role r ON r.id = ur.role_id
        JOIN scope s ON s.role_id = r.id
        LEFT JOIN route rt ON rt.id = s.route_id
    WHERE u.deleted_at IS NULL
        AND r.deleted_at IS NULL
        AND s.deleted_at IS NULL;


-- View: v_duplicate_limitations
CREATE OR REPLACE VIEW v_duplicate_limitations AS
    SELECT jsonb_agg(distinct roles) AS role_ids,
        jsonb_array_length(jsonb_agg(distinct roles)) duplicates,
        email,
        endpoint,
        method,
        target_url,
        limit_type
    FROM (
        SELECT jsonb_agg(role_id) AS role_ids,
            v_user_role_route.email,
            v_user_role_route.endpoint,
            v_user_role_route.method,
            v_user_role_route.target_url,
            specs -> 'type' AS limit_type
        FROM v_user_role_route,
            jsonb_array_elements(v_user_role_route.limitations) AS specs
        GROUP BY (
            v_user_role_route.email,
            v_user_role_route.endpoint,
            v_user_role_route.method,
            v_user_role_route.target_url,
            specs -> 'type'
        )
        UNION ALL (
            SELECT jsonb_agg(parent.id),
                v_user_role_route.email,
                v_user_role_route.endpoint,
                v_user_role_route.method,
                v_user_role_route.target_url,
                specs -> 'type' AS limit_type
            FROM v_user_role_route
            LEFT JOIN role AS parent ON parent.id = v_user_role_route.role_parent_id
            LEFT JOIN scope s ON s.role_id = parent.id
            LEFT JOIN route rt ON rt.id = s.route_id,
                jsonb_array_elements(s.limitations) AS specs
            GROUP BY (
                v_user_role_route.email,
                v_user_role_route.endpoint,
                v_user_role_route.method,
                v_user_role_route.target_url,
                specs -> 'type'
            )
        )
    ) t,
    jsonb_array_elements(t.role_ids) AS roles
    GROUP BY (
        email,
        endpoint,
        method,
        target_url,
        limit_type
    )
    HAVING jsonb_array_length(jsonb_agg(distinct roles)) > 1;


-- View: v_golemio_bi_api_users
CREATE OR REPLACE VIEW v_golemio_bi_api_users AS
    SELECT
        signups.date,
        CASE
            WHEN signups.initial_referer = 1 THEN 'Golemio API'
            WHEN signups.initial_referer = 2 THEN 'Golemio BI'
        END AS service,
        signups.new_invitations,
        signups.new_signups,
        totals.total_users
    FROM
    -- signups
        (
            SELECT
                COUNT(1) AS new_invitations,
                COUNT(1) filter(WHERE invitation_accepted_at IS NOT NULL) AS new_signups,
                date_trunc('day', created_at) AS date,
                initial_referer
            FROM
                "user"
            WHERE
                initial_referer IS NOT NULL
            GROUP BY
                initial_referer,
                date_trunc('day', created_at)
            ORDER BY
                initial_referer,
                date
        ) signups
    INNER JOIN
    -- total users
        (
            SELECT
                SUM(COUNT(1)) OVER (partition BY initial_referer ORDER BY date_trunc('day', created_at)) AS total_users,
                date_trunc('day', created_at) AS date,
                initial_referer
            FROM
                "user"
            WHERE
                deleted_at IS NULL
                AND initial_referer IS NOT NULL
                AND invitation_accepted_at IS NOT NULL
            GROUP BY
                initial_referer,
                date_trunc('day', created_at)
            ORDER BY
                initial_referer,
                date
        ) totals
    ON signups.date = totals.date AND signups.initial_referer = totals.initial_referer;
