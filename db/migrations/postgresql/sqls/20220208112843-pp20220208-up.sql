/* Replace with your SQL commands */
create view v_containers_users as 
with waste_users as (	
	select
		u.email,r.label
	from user_role ur 
	left join "user" u on ur.user_id = u.id
	left join "role" r on ur.role_id = r.id
	where r.label in ('SvozOdpadu - export - NEVEŘEJNÉ','SvozOdpadu - NEVEŘEJNÉ','SvozOdpadu Serviceboard - NEVEŘEJNÉ')
	)
	select wu.email,string_agg(wu.label,'; ') as roles
	from waste_users wu
	group by wu.email
	order by wu.email;

