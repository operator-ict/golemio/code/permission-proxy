-- public.v_containers_users source

drop VIEW v_containers_users;
CREATE OR REPLACE VIEW v_containers_users
AS 
WITH waste_users AS (
         SELECT u.email,
            r.label,
            u.name,u.surname 
           FROM user_role ur
             LEFT JOIN "user" u ON ur.user_id = u.id
             LEFT JOIN role r ON ur.role_id = r.id
          WHERE r.label::text = ANY (ARRAY['SvozOdpadu - NEVEŘEJNÉ'::character varying, 'SvozOdpadu Serviceboard - NEVEŘEJNÉ'::character varying]::text[])
        )
 SELECT wu.email,wu.name,wu.surname,
    string_agg(wu.label::text, '; '::text) AS roles
   FROM waste_users wu
  GROUP BY wu.email,wu.name,wu.surname
  ORDER BY wu.email;
