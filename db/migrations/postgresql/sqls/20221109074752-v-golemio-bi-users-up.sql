CREATE OR REPLACE VIEW v_users_golemio_bi AS
SELECT 
   email,
   name,
   surname,
   label AS role,
   ru.endpoint 
FROM user_role ur
	LEFT JOIN "user" u ON ur.user_id = u.id
        LEFT JOIN role r ON ur.role_id = r.id
        left join scope s on s.role_id = r.id 
        left join route ru on ru.id = s.route_id
where  
    u.deleted_at IS NULL 
    AND u.invitation_accepted_at IS NOT NULL;
