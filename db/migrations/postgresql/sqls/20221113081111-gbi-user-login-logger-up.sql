-- Table: user_login_event
CREATE TABLE IF NOT EXISTS "user_login_event"
(
    user_id integer NOT NULL,
    login_referer varchar,
    created_at timestamp with time zone NOT NULL,

    
    CONSTRAINT user_login_event_pkey PRIMARY KEY ("user_id", "created_at")
);
