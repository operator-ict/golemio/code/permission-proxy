DROP VIEW v_users_golemio_bi;

CREATE OR REPLACE VIEW v_users_golemio_bi
AS SELECT u.email,
    u.name,
    u.surname,
    r.label AS role,
    ru.endpoint
   FROM user_role ur
     LEFT JOIN "user" u ON ur.user_id = u.id
     LEFT JOIN role r ON ur.role_id = r.id
     LEFT JOIN scope s ON s.role_id = r.id
     LEFT JOIN route ru ON ru.id = s.route_id
  WHERE u.deleted_at IS NULL AND u.invitation_accepted_at IS NOT NULL;

  DROP VIEW v_gbi_last_login;
  