DROP VIEW v_users_golemio_bi;

CREATE or REPLACE view v_users_golemio_bi 
AS SELECT 
u.id,
u.email,
    u.name,
    u.surname,
    u.created_at  as registered_at,
    u.invitation_accepted_at  as activated_at,
    r.label AS role,
    ru.endpoint
   FROM user_role ur
     LEFT JOIN "user" u ON ur.user_id = u.id
     LEFT JOIN "role" r ON ur.role_id = r.id
     LEFT JOIN "scope" s ON s.role_id = r.id
     LEFT JOIN "route" ru ON ru.id = s.route_id
  WHERE u.deleted_at IS NULL AND u.invitation_accepted_at IS NOT null;

CREATE OR REPLACE VIEW v_gbi_last_login AS 
select user_id,login_referer as last_logged_referer,
case when login_referer ='https://bi.rabin.golemio.cz/' then max(ule.created_at) 
else null 
end as last_logged_at 
from user_login_event ule
where login_referer ='https://bi.rabin.golemio.cz/'
group by user_id,login_referer
order by user_id;

