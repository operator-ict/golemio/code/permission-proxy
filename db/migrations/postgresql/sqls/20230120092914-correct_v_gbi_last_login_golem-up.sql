DROP VIEW v_gbi_last_login;

CREATE OR REPLACE VIEW v_gbi_last_login
AS SELECT ule.user_id,
    ule.login_referer AS last_logged_referer,
        CASE
            WHEN ule.login_referer::text = 'https://bi.rabin.golemio.cz/'::text or ule.login_referer::text ='https://bi.golemio.cz/' THEN max(ule.created_at)
            ELSE NULL::timestamp with time zone
        END AS last_logged_at
   FROM user_login_event ule
  WHERE ule.login_referer::text = 'https://bi.rabin.golemio.cz/'::text or ule.login_referer::text = 'https://bi.golemio.cz/'::text
  GROUP BY ule.user_id, ule.login_referer
  ORDER BY ule.user_id;
