CREATE OR REPLACE VIEW v_gbi_daily_login_counts
as SELECT ule.user_id,
       ule.created_at::date as login_date,
       count(ule.created_at) as login_count
FROM user_login_event ule
WHERE ule.login_referer::text = 'https://bi.rabin.golemio.cz/'::text OR ule.login_referer::text = 'https://bi.golemio.cz/'::text
group by ule.user_id, ule.created_at::date
