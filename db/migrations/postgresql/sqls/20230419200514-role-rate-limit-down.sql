ALTER TABLE rate_limit DROP CONSTRAINT chk_reference_id;

DROP INDEX IF EXISTS rate_limit_role_id_uindex;

ALTER TABLE rate_limit DROP CONSTRAINT rate_limit_role_id_fk;

ALTER TABLE rate_limit DROP COLUMN IF EXISTS role_id;

DELETE FROM rate_limit WHERE user_id IS NULL;
ALTER TABLE rate_limit ALTER COLUMN user_id SET NOT NULL;
