ALTER TABLE rate_limit ALTER COLUMN user_id DROP NOT NULL;

ALTER TABLE rate_limit ADD COLUMN role_id integer;

ALTER TABLE rate_limit ADD CONSTRAINT rate_limit_role_id_fk FOREIGN KEY (role_id)
    REFERENCES "role" (id);

CREATE UNIQUE INDEX rate_limit_role_id_uindex ON rate_limit (role_id);

ALTER TABLE rate_limit ADD CONSTRAINT chk_reference_id
    CHECK ((user_id IS NULL) <> (role_id IS NULL));
