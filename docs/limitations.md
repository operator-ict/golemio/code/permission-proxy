# Limitations

### Property `block`

In case that the property block is set to true, and the limitation condition is not satisfied the proxy will return `409`. If the property would've been set to false, the limitation would be replaced with only the values that are allowed.

*Example:*

* **district** (`{block: true, values: ['praha-1']}`) - trying to access `?district=praha-2` -> `403`
* **limit** (`{block: false, values [{min: 100, max: 200}]}`) - trying to access `?limit=500000` -> `?limit=200`

### Property `source`

The property source defines the source of the value which should be limited. For now is possible to set source as

- **query (default)** - processing the limitation on the url query, e.g. `lamps/?code=123`
- **params** - processing the limitation on the url param, e.g. `lamps/123`

## List of limitations:

* `in-array` - checks if the requested value is amongst allowed enumeration of values
* `min-max` - checks if the requested value in the bounds of min / max values, provides default value if specified
