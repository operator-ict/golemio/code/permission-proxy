"use strict";

import * as express from "@golemio/core/dist/shared/express";
import { AbstractGolemioError, GeneralError, HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import * as cors from "cors";
import * as fs from "fs";
import * as http from "http";
import * as path from "path";
import { config } from "./core/config";
import { postgresConnector, redisConnector } from "./core/connectors";
import { cache, errorMessages, eventEmitter, log, requestLogger } from "./core/helpers";
import { modelManager } from "./core/models";
import { proxy } from "./proxy";

import { RedisPubSubChannel } from "./core/helpers/pubsub/RedisPubSubChannel";
import { accessTokenRouter } from "./resources/access_token";
import { apiKeyRouter } from "./resources/api_key";
import { changePasswordRouter } from "./resources/change_password";
import { forgottenPasswordRouter } from "./resources/forgotten_password";
import { invitationRouter } from "./resources/invitation";
import { rateLimitRouter } from "./resources/rate_limit";
import { refererWhitelistRouter } from "./resources/referer_whitelist";
import { roleRouter } from "./resources/role";
import { RouteRouter } from "./resources/route";
import { scopeRouter } from "./resources/scope";
import { userRouter } from "./resources/user";
import { userRoleRouter } from "./resources/user_role";

type LogLevels = "error" | "warn" | "info" | "debug" | "silly";
export default class App {
    // Create a new express application instance
    public express: express.Application = express();
    // The port the express app will listen on
    public port: number = parseInt(config.port || "3007", 10);

    public pubSubChannel: RedisPubSubChannel | undefined;

    private server: http.Server;

    private commitSHA: string;

    constructor() {
        //
    }

    public start = async (): Promise<void> => {
        try {
            this.commitSHA = await this.loadCommitSHA();
            log.info(`Commit SHA: ${this.commitSHA}`);
            await this.database();
            this.middlewares();
            this.routes();

            /** Reloading proxy definitions from module Proxy */
            eventEmitter.emit("proxy:reload");

            /** Reloading token blacklist from module Policy */
            eventEmitter.emit("token_blacklist:reload");

            /** Enabling Express `trust proxy` */
            this.express.enable("trust proxy");

            this.server = http.createServer(this.express);
            // Setup error handler hook on server error
            this.server.on("error", (err: Error) => {
                // TODO
                // ErrorHandler.handle(new CustomError("Could not start a server", false, "App", 1, err));
                log.error("Could not start a server");
            });
            // Serve the application at the given port
            this.server.listen(this.port, () => {
                // Success callback
                log.info(`Listening at http://localhost:${this.port}/`);
            });
        } catch (error) {
            // TODO
            log.error(error);
        }
    };

    public stop = async (): Promise<void> => {
        this.server.close();
    };

    private database = async () => {
        /** Postgres initialization */
        await postgresConnector.connect();

        /** Redis initialization */
        if (config.redis_enable) {
            await redisConnector.connect();

            await this.initializeDefinitionsReload();
        } else {
            /* Checking if REDIS_ENABLE is true because of rate-limiting */
            log.warn("Rate limiter is not going to work because REDIS_ENABLE is not true", {
                meta: { REDIS_ENABLE: config.redis_enable },
            });
        }

        /** Cache initialization */
        cache.init();

        /** Models initialization */
        modelManager.init();
    };

    private middlewares = () => {
        /** basic Express settings */
        this.express.disable("x-powered-by");
        this.express.use(requestLogger);

        /** CORS registering to Express */
        this.express.use(
            cors({
                allowedHeaders: [
                    "Origin",
                    "X-Requested-With",
                    "Content-Type",
                    "Accept",
                    "Access-Token",
                    "pragma",
                    "cache-control",
                    config.token_header,
                ],
                credentials: true,
                exposedHeaders: ["X-Count", "X-Items-Per-Page", "X-Page"],
            })
        );
    };

    private routes = () => {
        /**
         * registering proxy to Express
         * listening to events: `proxy:reload`
         */
        proxy.init(this.express);

        /** middleware only for gateway endpoints */
        const jsonParser = express.json({
            limit: config.payload_limit, // Reject payload bigger than limit
        });

        /** loading and registering all resources to Express */
        const routePrefix: string = "/gateway";
        const routeRouter = new RouteRouter(this.pubSubChannel);

        this.express.use(routePrefix, jsonParser, accessTokenRouter);
        this.express.use(routePrefix, jsonParser, apiKeyRouter);
        this.express.use(routePrefix, jsonParser, forgottenPasswordRouter);
        this.express.use(routePrefix, jsonParser, changePasswordRouter);
        this.express.use(routePrefix, jsonParser, invitationRouter);
        this.express.use(routePrefix, jsonParser, rateLimitRouter);
        this.express.use(routePrefix, jsonParser, refererWhitelistRouter);
        this.express.use(routePrefix, jsonParser, roleRouter);
        this.express.use(routePrefix, jsonParser, routeRouter.router);
        this.express.use(routePrefix, jsonParser, scopeRouter);
        this.express.use(routePrefix, jsonParser, userRouter);
        this.express.use(routePrefix, jsonParser, userRoleRouter);

        // Create base url route handler
        const statusUrls = [`${routePrefix}/`, `${routePrefix}/health-check`, `${routePrefix}/status`];
        this.express.get(statusUrls, (req, res, next) => {
            log.silly("Health check/status called.");
            res.json({
                app_name: "Golemio Data Platform Permission Proxy",
                commit_sha: this.commitSHA,
                status: "Up",
                // Current app version (fron environment variable) according to package.json version
                version: config.app_version,
            });
        });

        /** static files */
        this.express.use("/.well-known/security.txt", express.static("public/.well-known/security.txt"));
        this.express.use("/robots.txt", express.static("public/robots.txt"));

        /** ignore automatic favicon requests from some browsers */
        this.express.get("/favicon.ico", (_req, res) => res.sendStatus(204));

        /** registering 404 Not Found route to Express */
        this.express.use("*", (req: express.Request, res: express.Response) => {
            throw new GeneralError(
                "Route not found.",
                this.constructor.name,
                new Error(`Called ${req.method} ${req.originalUrl}`),
                404
            );
        });

        /** registering error handling to Express */
        // use centralized error handler
        this.express.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            let logLevel: LogLevels = "error";

            if (typeof err === "number") {
                return res.sendStatus(err);
            }

            let currentError: Error = null;

            if (err instanceof AbstractGolemioError) {
                currentError = err;

                if (["401", "400", "403", "404", "410", "409", "429"].includes(`${err.status}`)) {
                    logLevel = "warn";
                }
            } else if (err) {
                if (err.name === "SequelizeUniqueConstraintError") {
                    const e: string[] = err.errors.map((item) => `${item.path}_already_exist`);
                    currentError = new GeneralError("Unique constraint error", this.constructor.name, e.join(", "), 409);
                } else if (err.name === "SequelizeForeignKeyConstraintError") {
                    currentError = new GeneralError(
                        "Foreign key constraint error",
                        this.constructor.name,
                        `err_wrong_${err.index}`,
                        404
                    );
                } else if (err.type === "entity.too.large") {
                    currentError = new GeneralError(
                        "Entity too large",
                        this.constructor.name,
                        errorMessages.request_entity_too_large,
                        413
                    );
                }
            }

            if (!currentError) {
                currentError = new GeneralError(
                    `General server error while calling ${req.method} ${req.originalUrl}`,
                    this.constructor.name,
                    err,
                    500
                );
            }

            currentError.stack = err.stack;

            const errObject: IGolemioError = HTTPErrorHandler.handle(currentError, log, logLevel);

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };

    /**
     * Load the Commit SHA of the current build
     *
     * Only to be used at startup, not in runtime of the application.
     */
    private loadCommitSHA = async (): Promise<string> => {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(path.join(__dirname, "..", "commitsha"), (err: NodeJS.ErrnoException | null, data: Buffer) => {
                if (err) {
                    return resolve(undefined);
                }
                return resolve(data.toString());
            });
        });
    };

    private async initializeDefinitionsReload() {
        this.pubSubChannel = new RedisPubSubChannel(redisConnector.getConnection());
        const subscriber = this.pubSubChannel.createSubscriber();
        await subscriber.subscribe();
        log.debug("Redis subscriber subscribed to channel for reloading proxy definitions.");
        subscriber.listen(() => {
            log.info("Redis subscriber received a message to reload proxy definitions.");
            eventEmitter.emit("proxy:reload");
        });
    }
}
