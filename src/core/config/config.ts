"use strict";

export const config = {
    app_name: process.env.APP_NAME || "permission-proxy",
    app_version: process.env.npm_package_version,
    forgotten_password_expiration: process.env.FORGOTTEN_PASSWORD_EXPIRATION
        ? parseInt(process.env.FORGOTTEN_PASSWORD_EXPIRATION, 10)
        : 172800,
    invitation_expiration: process.env.INVITATION_EXPIRATION ? parseInt(process.env.INVITATION_EXPIRATION, 10) : 1209600,
    log_level: process.env.LOG_LEVEL || "info",
    mailer_enable: process.env.MAILER_ENABLE === "true" || false,
    mailer_from: process.env.MAILER_FROM,
    mailer_transport: {
        auth: {
            pass: process.env.MAILER_PASSWORD,
            user: process.env.MAILER_USERNAME,
        },
        host: process.env.MAILER_HOST,
        port: process.env.MAILER_PORT,
    },
    node_env: process.env.NODE_ENV,
    payload_limit: process.env.PAYLOAD_LIMIT,
    port: process.env.PORT,
    proxy_payload_limit: process.env.PROXY_PAYLOAD_LIMIT || "10MB",

    postgres_connection: process.env.POSTGRES_CONN,
    postgres_pool_max_connections: process.env.POSTGRES_POOL_MAX_CONNECTIONS
        ? parseInt(process.env.POSTGRES_POOL_MAX_CONNECTIONS, 10)
        : 10,
    postgres_test_schema: process.env.TEST_DATABASE_SCHEMA || "test",
    postgres_timezone: process.env.DATABASE_TIMEZONE || "Europe/Prague",

    rate_limit_prefix: process.env.RATE_LIMIT_PREFIX || "permission-proxy-rate-limiter",

    redis_connection: process.env.REDIS_CONN,
    redis_enable: process.env.REDIS_ENABLE === "true" || false,
    redis_ttl: process.env.REDIS_TTL ? parseInt(process.env.REDIS_TTL, 10) : 1000,

    token_algorithm: process.env.TOKEN_ALGORITHM || "HS256",
    token_expiration: process.env.TOKEN_EXPIRATION ? parseInt(process.env.TOKEN_EXPIRATION, 10) : 86400,
    token_has_cert: process.env.TOKEN_HAS_CERT === "true" || false,
    token_header: process.env.TOKEN_NAME || "x-access-token",
    token_issuer: process.env.TOKEN_ISSUER || "golemio",
    token_secret: process.env.TOKEN_SECRET_OR_CERT_PATH || "megasecretforcert",
    inspector: {
        host: process.env.INSPECTOR_HOST,
        port: process.env.INSPECTOR_PORT,
    },
};
