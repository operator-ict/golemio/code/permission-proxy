"use strict";

import { Sequelize } from "@golemio/core/dist/shared/sequelize";
import { config } from "../config";
import { log } from "../helpers";

class PostgresConnector {
    private connection: Sequelize | undefined;

    public connect = async (): Promise<Sequelize> => {
        try {
            if (this.connection) {
                return this.connection;
            }

            if (!config.postgres_connection) {
                // throw new CustomError("The ENV variable POSTGRES_CONN cannot be undefined.", true,
                //     this.constructor.name, 6003);
                throw new Error("The ENV variable POSTGRES_CONN cannot be undefined.");
            }

            const dbSchema = config.node_env === "test" ? config.postgres_test_schema : "public";
            log.info(`Using "${dbSchema}" database schema!`);

            this.connection = new Sequelize(config.postgres_connection, {
                define: {
                    freezeTableName: true,
                    schema: dbSchema,
                    underscored: true, // automatically set field option for all attributes to snake cased name
                },
                dialect: "postgres",
                dialectOptions: {
                    decimalNumbers: true,
                },
                logging: log.silly, // logging by Logger::silly
                //operatorsAliases: false, // disable aliases
                pool: {
                    acquire: 20000,
                    idle: 20000,
                    max: config.postgres_pool_max_connections,
                    min: 0,
                },
                retry: {
                    match: [
                        /SequelizeConnectionError/,
                        /SequelizeConnectionRefusedError/,
                        /SequelizeHostNotFoundError/,
                        /SequelizeHostNotReachableError/,
                        /SequelizeInvalidConnectionError/,
                        /SequelizeConnectionTimedOutError/,
                        /TimeoutError/,
                    ],
                    max: 8,
                },
                timezone: config.postgres_timezone,
            });

            if (!this.connection) {
                throw new Error("Connection is undefined.");
            }

            await this.connection.authenticate();
            log.info("Connected to PostgreSQL!");
            return this.connection;
        } catch (err) {
            // throw new CustomError("Error while connecting to PostgreSQL.", false,
            //     this.constructor.name, 1001, err);
            throw new Error("Error while connecting to PostgreSQL.");
        }
    };

    public getConnection = (): Sequelize => {
        if (!this.connection) {
            // throw new CustomError("Sequelize connection does not exist. First call connect() method.", false,
            //    this.constructor.name, 1002);
            throw new Error("Sequelize connection does not exist. First call connect() method.");
        }
        return this.connection;
    };
}

const postgresConnector = new PostgresConnector();

export { postgresConnector };
