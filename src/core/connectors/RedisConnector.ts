"use strict";

import * as Redis from "ioredis";
import { config } from "../config";
import { log } from "../helpers";

class RedisConnector {
    private connection: Redis.Redis;

    public connect = async (): Promise<Redis.Redis> => {
        try {
            if (this.connection) {
                return this.connection;
            }

            if (!config.redis_connection) {
                // throw new CustomError("The ENV variable REDIS_CONN cannot be undefined.", true,
                //    this.constructor.name, 6003);
                throw new Error("The ENV variable REDIS_CONN cannot be undefined.");
            }

            this.connection = new Redis(config.redis_connection);

            if (!this.connection) {
                throw new Error("Connection is undefined.");
            }

            this.connection.on("error", (err) => {
                // throw new CustomError("Error while connecting to Redis.", false,
                //     this.constructor.name, 1001, err);
                throw new Error("Error while connecting to Redis.");
            });
            this.connection.on("connect", () => {
                log.info("Connected to Redis!");
            });

            return this.connection;
        } catch (err) {
            // throw new CustomError("Error while connecting to Redis.", false,
            //     this.constructor.name, 1001, err);
            throw err;
        }
    };

    public getConnection = (): Redis.Redis => {
        if (!this.connection) {
            // throw new CustomError("Redis connection does not exists. First call connect() method.", false,
            //     this.constructor.name, 1002);
            throw new Error("Redis connection does not exists. First call connect() method.");
        }
        return this.connection;
    };

    public isConnected = (): boolean => {
        return this.connection ? true : false;
    };
}

const redisConnector = new RedisConnector();

export { redisConnector };
