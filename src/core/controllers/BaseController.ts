"use strict";

import { Response } from "@golemio/core/dist/shared/express";

export class BaseController {
    /**
     * Enriches the response by the headers `x-count`, `x-items-per-page` a `x-page`
     */
    protected setPaginationHeaders = (res: Response, count: number, limit: string, page: string): Response => {
        res.set({
            "x-count": count,
            "x-items-per-page": limit,
            "x-page": page,
        });
        return res;
    };
}
