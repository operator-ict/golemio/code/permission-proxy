"use strict";

// TODO: Use api-cache as in OG, I think it can be +- copied

import { Redis } from "ioredis";
import { config } from "../config";
import { redisConnector } from "../connectors";
import { log, MemoryStore } from "./";

/**
 * Redis Cache interface
 * defines methods that manipulates with redis
 */
export interface ICache {
    get: (index: string | number) => Promise<any>;
    set: (index: string | number, value: any, ttl: number) => Promise<any>;
}

/**
 * Cache using Redis store
 */
class Cache {
    private store: Redis | MemoryStore;

    public init = () => {
        this.store = config.redis_enable ? redisConnector.getConnection() : new MemoryStore();
    };

    /**
     * Getting cache object for manipulating with store
     *
     * TODO in case of `namespace` is better to use `hset` and `hget`
     */
    public getCache = (namespace: string): ICache => ({
        get: (index: string | number): Promise<any> => {
            log.debug("Getting from cache", { meta: { namespace, index } });
            return new Promise((resolve, reject) => {
                this.store.get(`${namespace}_${index}`, (err, value) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(value ? JSON.parse(value) : value);
                });
            });
        },
        set: (index: string | number, value: any, ttl: number = config.redis_ttl): Promise<any> => {
            log.debug("Storing into cache", { meta: { namespace, index, value } });
            return new Promise((resolve, reject) => {
                if (ttl === 0) {
                    return this.store.set(`${namespace}_${index}`, JSON.stringify(value), (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(value);
                    });
                }

                this.store.setex(`${namespace}_${index}`, ttl, JSON.stringify(value), (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(value);
                });
            });
        },
    });
}

const cache = new Cache();

export { cache };
