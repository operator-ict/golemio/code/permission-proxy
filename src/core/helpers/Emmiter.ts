"use strict";

import { EventEmitter } from "events";
import { proxy } from "../../proxy";
import { accessPolicyMiddleware } from "../middlewares";
import { log, mailer } from "./";

/**
 * Custom EventEmitter class
 */
export class Emitter extends EventEmitter {
    /**
     * Promisified emit function
     */
    public emitThen = function emitThen(event: string | symbol, ...args: any[]): Promise<any> {
        try {
            return Promise.all(this.listeners(event).map((listener) => listener.apply(this, args)));
        } catch (error) {
            return Promise.reject(error);
        }
    };
}

const eventEmitter = new Emitter();

eventEmitter.on("mail:send", (options) =>
    mailer
        .sendMail(options.recipients, options.templateName, options.locals)
        .then((result) => log.debug(result))
        .catch((err) => {
            log.error(err);
        })
);

// TODO: shouldn't this be done in separate resource/class Blacklist?
// maybe not
eventEmitter.on("token_blacklist:reload", () => {
    return accessPolicyMiddleware.reloadBlacklistTokenCache();
});

// TODO: do we have to do this in Policy.init?
// seems like weird dependency, can be called from anywhere
// shouldn't it be done in resource api_keys, or blacklist?
// maybe not
eventEmitter.on("api_key:revoke", ({ token }) => {
    return accessPolicyMiddleware.addBlacklistedToken(token);
});

eventEmitter.on("proxy:reload", () => proxy.reloadDefinitions());

export { eventEmitter };
