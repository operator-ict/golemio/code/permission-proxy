import { createLogger, createRequestLogger } from "@golemio/core/dist/helpers";
import { config } from "../config";

const logger = createLogger({
    logLevel: config.log_level,
    nodeEnv: config.node_env as string,
    projectName: "permission-proxy",
});

const requestLogger = createRequestLogger(config.node_env, logger);

export { logger as log, requestLogger };
