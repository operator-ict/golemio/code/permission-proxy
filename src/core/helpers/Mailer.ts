"use strict";

import * as Email from "email-templates";
import * as nodemailer from "nodemailer";
import * as path from "path";
import { config } from "../config";

/** Directory containing email templates */
const templateDir = path.join(__dirname, "..", "..", "templates");

/**
 * Class for sending emails
 * Used in eventEmitter by event `mail:send`
 */
class Mailer {
    private transporter: object | nodemailer.Transporter = {
        jsonTransport: true,
    };

    constructor() {
        if (config.mailer_enable) {
            if (config.mailer_transport) {
                this.transporter = nodemailer.createTransport(config.mailer_transport as any); // TODO fix types
            }
        }
    }

    /**
     * Email sending
     */
    public sendMail = (recipients: string, templateName: string, locals: any = {}): Promise<any> => {
        const email = new Email({
            message: {
                from: config.mailer_from,
            },
            // send: this.config.enable
            send: true,
            transport: this.transporter,
            views: { root: templateDir },
        });

        return email.send({
            locals,
            message: {
                to: recipients,
            },
            template: templateName,
        });
    };
}

const mailer = new Mailer();

export { mailer };
