"use strict";

import { log } from "./";

/**
 * TODO is it somewhere used?
 */
export class MemoryStore {
    private cache: any = {};

    public set = (key, value, cb) => {
        log.debug("storing value", { meta: { key, value } });
        this.cache[key] = value;
        cb();
    };

    public setex = (key, ttl, value, cb) => {
        log.debug("storing x value", { meta: { key, value } });
        this.cache[key] = value;
        cb();
    };

    public get = (key, cb) => {
        const value = this.cache[key];
        log.debug("getting value", { meta: { key, value } });
        cb(null, value);
    };
}
