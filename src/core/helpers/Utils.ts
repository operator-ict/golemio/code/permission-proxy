export const intersection = (arrays: any[][]) => {
    return arrays.reduce((a, b) => a.filter((c) => b.includes(c)));
};

export const clamp = (number: number, boundOne: number, boundTwo: number) => {
    if (!boundTwo) {
        return Math.max(number, boundOne) === boundOne ? number : boundOne;
    } else if (Math.min(number, boundOne) === number) {
        return boundOne;
    } else if (Math.max(number, boundTwo) === number) {
        return boundTwo;
    }
    return number;
};

export const extend = (target, ...sources) => {
    const length = sources.length;

    if (length < 1 || target == null) return target;
    for (let i = 0; i < length; i++) {
        const source = sources[i];

        for (const key in source) {
            target[key] = source[key];
        }
    }
    return target;
};

export const getUniqueBy = <T extends Record<string, never>>(arr: T[], prop: string): T[] => {
    const set = new Set<any>();
    return arr.filter((o) => !set.has(o[prop]) && set.add(o[prop]));
};

export const unionBy = (...arrays) => {
    const iteratee = arrays.pop();

    if (Array.isArray(iteratee)) {
        return [];
    }

    return [...arrays].flat().filter(
        (
            (set) => (o) =>
                set.has(iteratee(o)) ? false : set.add(iteratee(o))
        )(new Set())
    );
};

export const getMaxRateLimit = (rateLimitRole: any[]) => {
    let max: { timewindow: number; limit: number; role_id: number };

    for (const item of rateLimitRole) {
        if (!item) {
            return null;
        }

        max = max ? (item.timewindow / item.limit > max.timewindow / max.limit ? item : max) : item;
    }

    return max;
};
