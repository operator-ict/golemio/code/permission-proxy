import { PubSubChannel } from "@golemio/core/dist/helpers/data-access/pubsub/AbstractPubSubChannel";

import { Redis as RedisInstance } from "ioredis";
import { config } from "../../config/config";
import { log } from "../Logger";
import { RedisSubscriber } from "./RedisSubscriber";

export class RedisPubSubChannel extends PubSubChannel {
    private readonly publisherConnection: RedisInstance;
    private readonly channel: string = "permission-proxy-refresh";

    constructor(publisherConnection: RedisInstance) {
        super();
        this.publisherConnection = publisherConnection;
    }

    public async publishMessage(message: string): Promise<void> {
        await this.publisherConnection.publish(this.channel, message);
    }

    public createSubscriber(): RedisSubscriber {
        return new RedisSubscriber({
            channelName: this.channel,
            redisConnectionString: config.redis_connection,
            logger: log,
        });
    }
}
