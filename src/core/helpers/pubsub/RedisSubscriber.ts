import { Subscriber } from "@golemio/core/dist/helpers/data-access/pubsub/subscribers/AbstractSubscriber";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import Redis, { Redis as RedisInstance } from "ioredis";

interface ISubscriberOptions {
    channelName: string;
    redisConnectionString?: string;
    logger: ILogger;
}

export class RedisSubscriber extends Subscriber {
    private readonly redisChannel: string;
    private connection: RedisInstance;
    private logger: ILogger;

    constructor(options: ISubscriberOptions) {
        super();
        this.redisChannel = options.channelName;
        this.logger = options.logger;

        this.connection = new Redis(options.redisConnectionString, {
            connectionName: `Subscriber for ${this.redisChannel}`,
            lazyConnect: true,
            autoResubscribe: true,
        });

        this.connection.on("error", (err) => {
            this.logger.error("Error while connecting to Redis as Subscriber", { err });
        });
    }

    public async subscribe(): Promise<RedisInstance> {
        if (this.connection.status === "ready") {
            this.logger.warn(`Already subscribed to Redis channel ${this.redisChannel}`);
        } else {
            await this.connection.connect();
            await this.connection.subscribe(this.redisChannel);
            this.logger.debug(`Subscribed to Redis channel ${this.redisChannel}`);
        }

        return this.connection;
    }

    public async unsubscribe(): Promise<void> {
        await this.connection.unsubscribe(this.redisChannel);
        this.connection.disconnect();
    }

    public listen(callback: () => void): void {
        this.logger.debug(`Listening to Redis channel ${this.redisChannel}`);
        this.connection.on("message", (channel, message) => {
            try {
                this.logger.debug(`Received Redis message on channel ${channel}`, { message });
                if (channel !== this.redisChannel) {
                    return;
                }

                callback();
            } catch (err) {
                this.logger.error("Error while processing Redis message", { message });
            }
        });
    }
}
