"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import * as core from "express-serve-static-core";
import { config } from "../config";
import { cache, errorMessages, ICache, log } from "../helpers";
import { modelManager } from "../models";
import { tokenManager } from "./";

/**
 * Extended Express Request
 */
export type ExtendedRequest<
    P = core.ParamsDictionary,
    ResBody = any,
    ReqBody = any,
    ReqQuery = core.Query,
    Locals extends Record<string, any> = Record<string, any>
> = Request<P, ResBody, ReqBody, ReqQuery, Locals> & {
    session: {
        limitations?: any;
        rateLimitUser?: any;
        rateLimitRole?: any;
        public?: any;
        user?: any;
    };
};

/**
 * Class which defines middlewares to policy control
 * Used in eventEmitter by events `token_blacklist:reload` and `api_key:revoke`
 */
class AccessPolicyMiddleware {
    private tokenBlacklistCache: ICache;

    constructor() {
        this.tokenBlacklistCache = cache.getCache("token_blacklist");
    }

    /**
     * Checking if user is signed in
     */
    public isSignedIn = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const accessToken = await this.parseTokenFromRequest(req);
            const token = await this.getToken(accessToken);
            req.session = { user: token.entity };
            next();
        } catch (err) {
            next(err);
        }
    };

    /**
     * Checking if user is administrator
     */
    public isAdmin = (req: ExtendedRequest, res: Response, next: NextFunction): void => {
        if (!req.session.user.admin) {
            throw new GeneralError(errorMessages.forbidden, "AccessPolicyMiddleware", undefined, 403);
        }
        next();
    };

    public isAdminOrHimself = (userIdParam: string) => {
        return (req: ExtendedRequest, res: Response, next: NextFunction) => {
            if (!req.session.user.admin) {
                const userId = parseInt(req.params[userIdParam], 10);
                if (userId !== req.session.user.id) {
                    throw new GeneralError(errorMessages.forbidden, "AccessPolicyMiddleware", undefined, 403);
                }
            }
            next();
        };
    };

    /**
     * Checking authorized clients
     */
    public isClientAuthorized = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const token = await this.parseTokenFromRequest(req);
            // check if token is blacklisted
            await this.tokenBlacklistCache.get(token).then((item) => {
                if (item) {
                    throw new GeneralError(errorMessages.unauthorized, "AccessPolicyMiddleware", undefined, 401);
                }
            });

            const parsedToken = await this.getToken(token);
            const { limitations, rateLimitRole } = await modelManager.scopeModel
                .findAllUserScopesForRoute(parsedToken.entity.id, req.method, req.route.path)
                .catch((err) => {
                    if (err instanceof Error && err.message === errorMessages.not_found) {
                        log.info("access denied url", {
                            meta: {
                                method: req.method,
                                path: req.route.path,
                                user: parsedToken.entity.id,
                            },
                        });
                        throw new GeneralError(errorMessages.forbidden, "AccessPolicyMiddleware", undefined, 403);
                    }
                    throw err;
                });

            const rateLimitUser = await modelManager.rateLimitModel.findOne({
                where: {
                    user_id: parsedToken.entity.id,
                },
            });

            req.session = {
                limitations,
                rateLimitUser: rateLimitUser ? { limit: rateLimitUser.limit, timewindow: rateLimitUser.timewindow } : false,
                rateLimitRole: rateLimitRole
                    ? { limit: rateLimitRole.limit, timewindow: rateLimitRole.timewindow, role_id: rateLimitRole.role_id }
                    : false,
                user: parsedToken.entity,
            };

            next();
        } catch (err) {
            next(err);
        }
    };

    /**
     * Restricting public access
     */
    public publicAccess = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const limitations = await modelManager.scopeModel
                .findAllPublicScopesForRoute(req.method, req.route.path)
                .catch((err) => {
                    if (err instanceof Error && err.message === errorMessages.not_found) {
                        log.info("access denied url", {
                            meta: {
                                method: req.method,
                                path: req.route.path,
                            },
                        });
                        throw new GeneralError(errorMessages.forbidden, "AccessPolicyMiddleware", undefined, 403);
                    }
                    throw err;
                });
            req.session = { limitations, public: true };

            next();
        } catch (err) {
            next(err);
        }
    };

    /**
     * Adding token to blacklist cache
     */
    public addBlacklistedToken = (token: string): Promise<any> => {
        log.debug("setting blacklist token", { meta: { token } });
        return this.tokenBlacklistCache.set(token, true, 0);
    };

    /**
     * Reloading blacklist cache
     */
    public reloadBlacklistTokenCache = async (): Promise<void> => {
        try {
            log.debug("reloading blacklist cache");
            const apiKeys = await modelManager.apiKeyModel.scope("deleted").findAll();
            await Promise.all(apiKeys.map((apiKey: any) => this.addBlacklistedToken(apiKey.code)));
        } catch (err) {
            throw err;
        }
    };

    /**
     * Verifing and getting token from access token string
     */
    private getToken = async (accessToken: string): Promise<any> => {
        const token = await tokenManager.verifyToken(accessToken);
        if (!token) {
            throw new GeneralError(errorMessages.unauthorized, "AccessPolicyMiddleware", undefined, 401);
        }
        return token;
    };

    /**
     * Getting token from Request
     */
    private parseTokenFromRequest = async (req: Request): Promise<string> => {
        const accessToken = req.header(config.token_header) || false;
        if (!accessToken) {
            throw new GeneralError(errorMessages.no_access_token, "AccessPolicyMiddleware", undefined, 401);
        }
        return accessToken;
    };
}

const accessPolicyMiddleware = new AccessPolicyMiddleware();

export { accessPolicyMiddleware };
