"use strict";

import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import * as fs from "fs";
import * as jwt from "jsonwebtoken";
import { v4 } from "uuid";
import { config } from "../config";
import { log } from "../helpers";

const ALLOWED_JWT_ALGORITHMS = ["HS256", "HS384", "HS512"];

/**
 * JWT token manager
 */
class TokenManager {
    private certOrSecret: string | Buffer = null;

    constructor() {
        if (config.token_has_cert) {
            try {
                this.certOrSecret = fs.readFileSync(config.token_secret);
            } catch (err) {
                throw new FatalError("Path to cert must be set.", this.constructor.name, err);
            }
        } else {
            this.certOrSecret = config.token_secret;
        }

        // algorithm check
        if (ALLOWED_JWT_ALGORITHMS.indexOf(config.token_algorithm) === -1) {
            throw new FatalError(`JWT algorithm must be one of ${JSON.stringify(ALLOWED_JWT_ALGORITHMS)}`, this.constructor.name);
        }
    }

    /**
     * Create JWT token for user
     */
    public generateToken = (entity: any, expiration: number = null): Promise<any> => {
        return new Promise((resolve, reject) => {
            jwt.sign(
                entity,
                this.certOrSecret,
                {
                    algorithm: config.token_algorithm as jwt.Algorithm,
                    expiresIn: expiration || config.token_expiration,
                    issuer: config.token_issuer,
                    jwtid: v4(),
                },
                (err, token) => {
                    if (err) {
                        return reject(err);
                    }
                    log.debug("token issued", { meta: { entity, token } });
                    return resolve({ token, entity });
                }
            );
        });
    };

    /**
     * Verify token validity
     */
    public verifyToken = (token: string): Promise<any> => {
        return new Promise((resolve) => {
            log.debug("checking token", { meta: { token } });
            jwt.verify(
                token,
                this.certOrSecret,
                {
                    algorithms: [config.token_algorithm as jwt.Algorithm],
                },
                (err, entity) => {
                    if (err) {
                        log.warn(err);
                        return resolve(null);
                    }
                    resolve({
                        entity,
                        token,
                    });
                }
            );
        });
    };
}

const tokenManager = new TokenManager();

export { tokenManager };
