"use strict";
import axios from "@golemio/core/dist/shared/axios";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response } from "@golemio/core/dist/shared/express";
import { matchedData, param, query, validationResult } from "express-validator";
import { errorMessages } from "../helpers";
import { ExtendedRequest } from "./";

/**
 * Middleware for checking request errors
 */
export const checkErrors = (req: ExtendedRequest, res: Response, next: NextFunction): Response | void => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        throw new GeneralError("Validation error", "Validation", JSON.stringify(errors.mapped()), 400);
    }

    req.body = matchedData(req, { locations: ["body"] });

    // TODO: "Middleware for checking errors" but does some other logic here?? enriching req object
    // WTF
    // re-do all this
    if (req.session && req.session.user.id) {
        if (req.method === "PUT") {
            req.body.updated_by_user_id = req.session.user.id;
        } else if (req.method === "POST") {
            req.body.created_by_user_id = req.session.user.id;
        }
    }
    next();
};

/**
 * List of validation middlewares
 * TODO: Check list options? Or Checklist options? Ambiguous function name
 * isn't this just pagination? maybe rename to pagination
 * TODO: Re-do pagination parameters to be same as in OG
 */
export const checkListOptions = [
    query("search").optional(),
    query("limit").optional().isInt({ min: 1 }),
    query("page").optional().isInt({ min: 1 }),
    checkErrors,
    (req: ExtendedRequest<{}, any, any, any>, res: Response, next: NextFunction): void => {
        !req.query.search && (req.query.search = false);
        req.query.limit && !req.query.page && (req.query.page = 1);
        req.query.limit && (req.query.limit = parseInt(req.query.limit, 10));
        if (req.query.limit && req.query.page) {
            req.query.offset = (req.query.page - 1) * req.query.limit;
        }
        next();
    },
];

export const checkOrdering = (allowedFields: string[]) => {
    return [
        query("orderBy").optional(),
        query("orderDir").optional(),
        (req: ExtendedRequest<{}, any, any, any>, res: Response, next: NextFunction): void => {
            req.query.orderBy && (req.query.orderBy = req.query.orderBy.toLowerCase());
            req.query.orderBy && allowedFields.indexOf(req.query.orderBy) === -1 && (req.query.orderBy = false);
            req.query.orderDir && (req.query.orderDir = req.query.orderDir.toUpperCase());
            req.query.orderDir && ["ASC", "DESC"].indexOf(req.query.orderDir) === -1 && (req.query.orderDir = "ASC");
            next();
        },
    ];
};

/**
 * Middleware for checking request if contains param id
 * TODO id is always a number? (isInt validation)
 * TODO: Rename to more descriptive, non-general method name "checkId" could mean anything
 */
export const checkId = (paramName: string | string[], zeroEnabled = false) => {
    let paramNames: string[];
    if (typeof paramName === "string") {
        paramNames = [paramName];
    } else {
        paramNames = paramName;
    }
    return async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        const paramNamesPromisesArr = paramNames.map((item) => {
            new Promise((resolve, reject) => {
                param(item, "err_not_an_id").isInt({ min: zeroEnabled === true ? 0 : 1 })(req, res, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(null);
                });
            });
        });
        await Promise.all(paramNamesPromisesArr);
        next();
    };
};

/**
 * Middleware for checking request if contains valid recaptcha response.
 * Should be called before any form validation.
 */
export const checkRecaptcha = (req: ExtendedRequest, res: Response, next: NextFunction): Response | void => {
    const recaptchaResponse = req.body ? req.body["g-recaptcha-response"] || false : false;
    if (!recaptchaResponse) {
        throw new GeneralError(errorMessages.recaptcha_required, "policy", undefined, 400);
    }

    axios({
        method: "post",
        params: {
            response: recaptchaResponse,
            secret: process.env.RECAPTCHA_SECRET_KEY,
        },
        url: "https://www.google.com/recaptcha/api/siteverify",
    })
        .then((apiResponse) => {
            if (!apiResponse.data.success) {
                throw new GeneralError(errorMessages.invalid_recaptcha, "policy", undefined, 400);
            }
            next();
        })
        .catch(next);
};
