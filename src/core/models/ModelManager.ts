"use strict";

// TODO: Leave for now. Later to replace by Objection.js
// TODO: Can we just get rid of modelManager all together? And load separate
// models independently and not by string (!) keys

import { Sequelize } from "@golemio/core/dist/shared/sequelize";
import { postgresConnector } from "../connectors";
import {
    apiKeyModel,
    ApiKeyModel,
    forgottenPasswordModel,
    ForgottenPasswordModel,
    invitationModel,
    InvitationModel,
    rateLimitModel,
    RateLimitModel,
    refererWhitelistModel,
    RefererWhitelistModel,
    roleModel,
    RoleModel,
    routeModel,
    RouteModel,
    scopeModel,
    ScopeModel,
    userLoginEventModel,
    UserLoginEventModel,
    userModel,
    UserModel,
    userRoleModel,
    UserRoleModel,
    vAllUserRoleRouteDistinctModel,
    VAllUserRoleRouteDistinctModel,
    vDuplicateLimitationsModel,
    VDuplicateLimitationsModel,
    vUserRoleRouteModel,
    VUserRoleRouteModel,
} from "../../resources";

interface IRepositories {
    api_key: ApiKeyModel;
    forgotten_password: ForgottenPasswordModel;
    invitation: InvitationModel;
    rate_limit: RateLimitModel;
    referer_whitelist: RefererWhitelistModel;
    role: RoleModel;
    route: RouteModel;
    scope: ScopeModel;
    user: UserModel;
    user_login_event: UserLoginEventModel;
    user_role: UserRoleModel;
    v_all_user_role_route_distinct: VAllUserRoleRouteDistinctModel;
    v_duplicate_limitations: VDuplicateLimitationsModel;
    v_user_role_route: VUserRoleRouteModel;
}
/**
 * Model Manager class for manipulating with PosgreSQL using Sequelize
 */
class ModelManager {
    private repositories: IRepositories;
    private _sequelize: Sequelize = null;

    public apiKeyModel: ApiKeyModel;
    public forgottenPasswordModel: ForgottenPasswordModel;
    public invitationModel: InvitationModel;
    public rateLimitModel: RateLimitModel;
    public refererWhitelistModel: RefererWhitelistModel;
    public roleModel: RoleModel;
    public routeModel: RouteModel;
    public scopeModel: ScopeModel;
    public userModel: UserModel;
    public userLoginEventModel: UserLoginEventModel;
    public userRoleModel: UserRoleModel;
    public vAllUserRoleRouteDistinctModel: VAllUserRoleRouteDistinctModel;
    public vDuplicateLimitationsModel: VDuplicateLimitationsModel;
    public vUserRoleRouteModel: VUserRoleRouteModel;

    /**
     * Initialization
     */
    public init(): void {
        const sequelize = postgresConnector.getConnection();
        this._sequelize = sequelize;

        this.repositories = {
            api_key: apiKeyModel(this._sequelize),
            forgotten_password: forgottenPasswordModel(this._sequelize),
            invitation: invitationModel(this._sequelize),
            rate_limit: rateLimitModel(this._sequelize),
            referer_whitelist: refererWhitelistModel(this._sequelize),
            role: roleModel(this._sequelize),
            route: routeModel(this._sequelize),
            scope: scopeModel(this._sequelize),
            user: userModel(this._sequelize),
            user_login_event: userLoginEventModel(this._sequelize),
            user_role: userRoleModel(this._sequelize),
            v_all_user_role_route_distinct: vAllUserRoleRouteDistinctModel(this._sequelize),
            v_duplicate_limitations: vDuplicateLimitationsModel(this._sequelize),
            v_user_role_route: vUserRoleRouteModel(this._sequelize),
        };

        Object.keys(this.repositories).forEach((modelName) => {
            if (this.repositories[modelName].associate) {
                this.repositories[modelName].associate(this.repositories);
            }
        });

        this.apiKeyModel = this.repositories.api_key;
        this.forgottenPasswordModel = this.repositories.forgotten_password;
        this.invitationModel = this.repositories.invitation;
        this.rateLimitModel = this.repositories.rate_limit;
        this.refererWhitelistModel = this.repositories.referer_whitelist;
        this.roleModel = this.repositories.role;
        this.routeModel = this.repositories.route;
        this.scopeModel = this.repositories.scope;
        this.userModel = this.repositories.user;
        this.userLoginEventModel = this.repositories.user_login_event;
        this.userRoleModel = this.repositories.user_role;
        this.vAllUserRoleRouteDistinctModel = this.repositories.v_all_user_role_route_distinct;
        this.vDuplicateLimitationsModel = this.repositories.v_duplicate_limitations;
        this.vUserRoleRouteModel = this.repositories.v_user_role_route;
    }

    get sequelize() {
        return this._sequelize;
    }
    public getTransaction() {
        return this.sequelize.transaction();
    }
}

const modelManager = new ModelManager();

export { modelManager };
