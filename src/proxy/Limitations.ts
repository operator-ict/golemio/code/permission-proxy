"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Request } from "@golemio/core/dist/shared/express";
import { clamp, intersection } from "../core/helpers/Utils";
import { DateTime } from "@golemio/core/dist/shared/luxon";

export interface ILimitation {
    block?: boolean;
    max?: number | string;
    min?: number | string;
    default?: number | string;
    source?: string;
    type: string;
    minMaxType?: string;
    values?: any[];
}

type LimitationMethod = (limitation: ILimitation, requestedValue?: string | string[] | number, req?: Request) => any;

interface ILimitationMethods {
    /** checks if the requested value is amongst allowed enumeration of values */
    "in-array": LimitationMethod;
    /** checks if the requested value in the bounds of min / max values */
    "min-max": LimitationMethod;
}

/**
 * checks if the requested value is amongst allowed enumeration of values
 */
const inArrayLimitation: LimitationMethod = (limitation: ILimitation, requestedValue: string | string[]): string[] => {
    const result = requestedValue
        ? intersection([Array.isArray(requestedValue) ? requestedValue : [requestedValue], limitation.values])
        : limitation.values;
    if (result.length === 0) {
        throw new GeneralError("not_allowed", "limitations", "in-array length must be greater than 0", 400);
    }
    return result;
};

/**
 * checks if the requested value in the bounds of min / max values
 */
const minMaxLimitation: LimitationMethod = (limitation: ILimitation, requestedValue: string | string[]): number | string => {
    const type = (limitation.minMaxType as MinMaxTypeEnum) || MinMaxTypeEnum.Number;
    const min = type === MinMaxTypeEnum.Number ? (limitation.min as number) || 0 : new Date(limitation.min).valueOf();
    const max =
        type === MinMaxTypeEnum.Number
            ? (limitation.max as number) || Number.MAX_SAFE_INTEGER
            : new Date(limitation.max).valueOf() || new Date("3999-12-31").valueOf();
    const value =
        type === MinMaxTypeEnum.Number
            ? parseInt(requestedValue as string, 10) || 0
            : new Date(requestedValue as string).valueOf() || 0;

    if (((limitation.source && limitation.source !== "query") || limitation.block) && (value > max || value < min)) {
        throw new GeneralError("not_allowed", "limitations", "invalid min-max values", 400);
    }
    return requestedValue ? formatMinMaxLimitation(clamp(value, min, max), type) : limitation.default || limitation.max;
};

const formatMinMaxLimitation = (value: number, type: MinMaxTypeEnum): number | string => {
    return type === MinMaxTypeEnum.DateTime
        ? DateTime.fromMillis(value).toUTC().toISO()
        : type === MinMaxTypeEnum.Date
        ? DateTime.fromMillis(value).toISODate()
        : value;
};

export const limitations: ILimitationMethods = {
    "in-array": inArrayLimitation,
    "min-max": minMaxLimitation,
};

export enum MinMaxTypeEnum {
    Number = "number",
    Date = "date",
    DateTime = "date-time",
}
