"use strict";

import axios, { AxiosRequestConfig } from "@golemio/core/dist/shared/axios";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { parse as bytesParse } from "bytes";
import { Application, NextFunction, Response, Router } from "@golemio/core/dist/shared/express";
import * as pathToRegexp from "path-to-regexp";
import { fn, col } from "@golemio/core/dist/shared/sequelize";
import { format as formatUrl, parse as parseUrl } from "url";
import { errorMessages, log } from "../core/helpers";
import { extend } from "../core/helpers/Utils";
import { accessPolicyMiddleware, ExtendedRequest } from "../core/middlewares";
import { modelManager } from "../core/models";
import { ILimitation, limitations, rateLimitMiddleware } from "./";
import { config } from "../core/config";

const BLOCK_HEADERS: string[] = ["host", "cookie", "content-length", "x-access-token"];

class Proxy {
    private router: Router = null;

    constructor() {
        //
    }

    public init = (app: Application): void => {
        log.debug("Initiating proxy");

        app.use((req: ExtendedRequest, res: Response, next: NextFunction) => {
            log.debug("using proxy", { meta: { url: req.originalUrl } });
            if (this.router === null) {
                return next();
            }
            this.router(req, res, next);
        });
    };

    /**
     * Reloading all routes from model and applying proxy on them
     */
    public reloadDefinitions = async (): Promise<void> => {
        try {
            const routeModel = modelManager.routeModel;
            this.router = Router();

            const routes = await routeModel.findAll({
                order: [
                    [fn("substring", col("endpoint"), ":"), "DESC"],
                    ["endpoint", "DESC"],
                ],
                raw: true,
            });

            await Promise.all(
                routes.map((route: any) => {
                    // TODO route type
                    log.debug("route initialized", { meta: { route } });
                    return this.router[route.method.toLowerCase()](
                        `${route.endpoint}`, // url
                        (req: ExtendedRequest, res: Response, next: NextFunction) => {
                            // logging middleware
                            log.debug("route called", { meta: { url: req.originalUrl } });
                            next();
                        },
                        route.public // public or authorized middleware
                            ? accessPolicyMiddleware.publicAccess
                            : accessPolicyMiddleware.isClientAuthorized,
                        rateLimitMiddleware,
                        this.proxyRequest(route) // request
                    );
                })
            );
        } catch (err) {
            throw err;
        }
    };

    /**
     * Finding limitation method and using it
     */
    public handleLimitationType = (
        limitation: ILimitation,
        requestedValue?: string | string[] | number,
        req?: ExtendedRequest
    ) => {
        if ({}.hasOwnProperty.call(limitations, limitation.type)) {
            return limitations[limitation.type](limitation, requestedValue, req);
        }
        return requestedValue;
    };

    /**
     * Processing query params and using the limitations on them
     *
     * @param req
     * @param limits req.session.limitations
     */
    private processQueryParams = async (req: ExtendedRequest<{}, any, any, any>, limits: ILimitation[]): Promise<any> =>
        limits.reduce(async (prev: any, current: any) => {
            log.debug("Trying to get property?", {
                meta: {
                    current,
                    query: req.query[current.name],
                    requestedQuery: req.query,
                },
            });
            try {
                const previous = await prev;
                let value = null;
                if (current.source === "params") {
                    // If it's a req.params limitation then just process it
                    await this.handleLimitationType(current, req.params[current.name], req);
                } else {
                    // When it's a query limitation then we need the value
                    value = await this.handleLimitationType(current, req.query[current.name], req);
                }

                if (value !== null && value !== undefined) {
                    previous[current.name] = value;
                    return previous;
                }

                return previous;
            } catch (err) {
                if (err.message === "not_allowed") {
                    throw new GeneralError(errorMessages.forbidden, "Proxy", undefined, 403);
                }
                throw err;
            }
        }, Promise.resolve({}));

    /**
     * Calling the proxy API
     */
    private proxyApiCall = (options: AxiosRequestConfig, req: ExtendedRequest, res: Response): Promise<void> =>
        new Promise(async (resolve, reject) => {
            try {
                const abortController = new AbortController();
                options.signal = abortController.signal;
                options.decompress = false;

                const respFromTarget = await axios(options);

                // Abort api request if client close its connection
                req.on("close", () => {
                    abortController.abort();
                    return resolve();
                });

                res.statusCode = respFromTarget.status;

                // Do not attempt to apply transforms to non-gzipped error responses
                if (respFromTarget.status >= 400 && respFromTarget.headers["content-encoding"] !== "gzip") {
                    if (respFromTarget.headers["content-type"]) {
                        res.set("content-type", respFromTarget.headers["content-type"]);
                    }

                    respFromTarget.data.pipe(res);
                    return resolve();
                }

                for (const key in respFromTarget.headers) {
                    if (respFromTarget.headers.hasOwnProperty(key)) {
                        res.set(key, respFromTarget.headers[key]);
                    }
                }
                respFromTarget.data.pipe(res);
                return resolve();
            } catch (err) {
                reject(err);
            }
        });

    /**
     * Preparing headers for options of proxy API call
     */
    private prepareHeaders = (req: ExtendedRequest): any => {
        const headers = {};
        if (req.ip) {
            headers["x-forwarded-for"] = req.ip;
        }

        if (req.headers && req.headers.host) {
            const hostSplit = req.headers.host.split(":");
            const host = hostSplit[0];
            const port = hostSplit[1];

            if (port) {
                headers["x-forwarded-port"] = port;
            }
            headers["x-forwarded-host"] = host;
        }

        headers["x-forwarded-proto"] = req.secure ? "https" : "http";

        for (const [key, value] of Object.entries(req.headers)) {
            if (!BLOCK_HEADERS.includes(key.toLowerCase())) {
                headers[key] = value;
            }
        }

        return headers;
    };

    /**
     * Preparing options for proxy API call and call proxyApiCall method
     */
    private proxyRequest =
        (route: any) =>
        async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
            try {
                const isRedirect: boolean = route.is_redirect;

                const queryParams = await this.processQueryParams(req, req.session.limitations);
                const parsedUrl = parseUrl(route.target_path || route.endpoint);
                const compiledPath = pathToRegexp.compile(parsedUrl.path);
                const pathname = decodeURIComponent(compiledPath(req.params));

                const url = formatUrl({
                    host: route.target_url,
                    pathname,
                    query: extend({}, req.query, queryParams),
                });
                log.debug("url", { meta: { url } });

                const options: AxiosRequestConfig = {
                    headers: this.prepareHeaders(req),
                    method: route.method,
                    responseType: "stream",
                    url,
                    validateStatus: (status) => status < 500, // Resolve only if the status code is less than 500
                    // defines the max size of the http request content in bytes allowed
                    maxBodyLength: bytesParse(config.proxy_payload_limit),
                    // defines the max size of the http response content in bytes allowed in node.js
                    maxContentLength: bytesParse(config.proxy_payload_limit),
                };

                if (
                    options.method === "POST" ||
                    options.method === "PUT" ||
                    options.method === "DELETE" ||
                    options.method === "PATCH"
                ) {
                    options.data = req; // A req is a readable stream of data in Express
                }

                log.debug("final request", { meta: { options } });
                if (isRedirect) {
                    return res.redirect(307, url);
                }
                return await this.proxyApiCall(options, req, res);
            } catch (err) {
                // TODO: Is it always 403 here?
                if (err.status === 403) {
                    log.info("access denied limitations", {
                        meta: {
                            limitations,
                            requestedParams: req.query,
                            user: req.session.user,
                        },
                    });
                }
                next(err);
            }
        };
}

const proxy = new Proxy();

export { proxy };
