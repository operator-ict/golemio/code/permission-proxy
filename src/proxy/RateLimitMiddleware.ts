import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response } from "@golemio/core/dist/shared/express";
import { RateLimiterRedis } from "rate-limiter-flexible";
import { config } from "../core/config";
import { redisConnector } from "../core/connectors";
import { log } from "../core/helpers";
import { ExtendedRequest } from "../core/middlewares";

export const rateLimitMiddleware = (req: ExtendedRequest, res: Response, next: NextFunction) => {
    const skip =
        !redisConnector.isConnected() ||
        req.session.public ||
        (!req.session.rateLimitUser && !req.session.rateLimitRole) ||
        false;

    if (skip) {
        log.debug("Skipping...");
        return next();
    }

    const configRateLimit = req.session.rateLimitUser || req.session.rateLimitRole;

    const rateLimiter = new RateLimiterRedis({
        duration: configRateLimit.timewindow / 1000, // time needed until a reset
        keyPrefix: `${config.rate_limit_prefix}:`,
        points: configRateLimit.limit, // maximum number of requests per duration
        storeClient: redisConnector.getConnection(),
    });

    const key = req.session.rateLimitUser
        ? `user_${req.session.user.id}:user`
        : `user_${req.session.user.id}:role_${req.session.rateLimitRole.role_id}`;

    rateLimiter
        .consume(key, 1)
        .then(() => {
            return next();
        })
        .catch((rateLimiterRes) => {
            res.set("Retry-After", "" + rateLimiterRes.msBeforeNext / 1000);
            res.set("X-RateLimit-Limit", "" + req.session.rateLimitUser.limit);
            res.set("X-RateLimit-Remaining", "" + rateLimiterRes.remainingPoints);
            res.set("X-RateLimit-Reset", "" + new Date(Date.now() + rateLimiterRes.msBeforeNext).getTime());

            return next(new GeneralError("Too many requests", "rateLimiter", undefined, 429));
        });
};
