"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { userLoginEventLogger } from "../user_login_event/UserLoginEventLogger";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { tokenManager as token } from "../../core/middlewares";
import { modelManager } from "../../core/models";

export class AccessTokenController extends BaseController {
    public login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";

            const user = await modelManager.userModel.findByCredentials(
                (req.body.email as string).toLowerCase(),
                req.body.password
            );
            if (!user) {
                throw new GeneralError(errorMessages.invalid_credentials, this.constructor.name, undefined, 401);
            }
            if (isAdminRoute && !user.admin) {
                throw new GeneralError(errorMessages.forbidden, this.constructor.name, undefined, 403);
            }
            if (user.invitation_accepted_at === null) {
                throw new GeneralError(errorMessages.invitation_not_accepted_yet, this.constructor.name, undefined, 403);
            }
            const result = await token.generateToken(user);

            await userLoginEventLogger.logEvent({
                user_id: user.id,
                login_referer: req.headers.referer,
            });

            res.status(200).json({ token: result.token });
        } catch (err) {
            next(err);
        }
    };

    public verify = (req: Request, res: Response, next: NextFunction) => res.sendStatus(204);
}
