"use strict";

import { body } from "express-validator";
import { errorMessages } from "../../core/helpers";

const loginForm = [
    body("email").exists().withMessage(errorMessages.required).isEmail().withMessage(errorMessages.not_an_email),
    body("password").exists().withMessage(errorMessages.required),
];

export { loginForm };
