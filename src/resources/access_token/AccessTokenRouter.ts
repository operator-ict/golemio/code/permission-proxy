"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { accessPolicyMiddleware, checkErrors, checkRecaptcha } from "../../core/middlewares";
import { AccessTokenController, loginForm } from "./";

class AccessTokenRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: AccessTokenController;

    constructor() {
        this.baseUrl = "";
        this.controller = new AccessTokenController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post(`${this.baseUrl}/login`, checkRecaptcha, loginForm, checkErrors, this.controller.login);

        this.router.get(`${this.baseUrl}/verify`, accessPolicyMiddleware.isSignedIn, this.controller.verify);
    };
}

const accessTokenRouter: Router = new AccessTokenRouter().router;

export { accessTokenRouter };
