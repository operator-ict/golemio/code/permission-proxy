"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response } from "@golemio/core/dist/shared/express";
import { BaseController } from "../../core/controllers";
import { errorMessages, eventEmitter } from "../../core/helpers";
import { ExtendedRequest, tokenManager } from "../../core/middlewares";
import { modelManager } from "../../core/models";

export class ApiKeyController extends BaseController {
    public list = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const apiKeys = await modelManager.apiKeyModel.findAll({
                order: [
                    ["deleted_at", "DESC"],
                    ["created_at", "DESC"],
                ],
                paranoid: false,
                where: {
                    user_id: req.session.user.id,
                },
            });
            res.json(apiKeys);
        } catch (err) {
            next(err);
        }
    };

    public create = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            if (req.session.user.admin) {
                return;
            }
            const count = await modelManager.apiKeyModel.count({
                where: {
                    user_id: req.session.user.id,
                },
            });
            if (count) {
                throw new GeneralError(errorMessages.cannot_create_more_than_one, this.constructor.name, undefined, 403);
            }
            const { token } = await tokenManager.generateToken(
                {
                    email: req.session.user.email,
                    id: req.session.user.id,
                    name: req.session.user.name,
                    surname: req.session.user.surname,
                },
                10000000000
            );
            const apiKey = await modelManager.apiKeyModel.create({
                code: token,
                user_id: req.session.user.id,
            });

            res.status(201).json(apiKey);
        } catch (err) {
            next(err);
        }
    };

    public remove = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const apiKeyId = req.params.id;
            const userId = req.session.user.id;

            const apiKey = await modelManager.apiKeyModel.findOne({
                paranoid: false,
                where: {
                    id: apiKeyId,
                    user_id: userId,
                },
            });
            if (!apiKey) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            if (apiKey.deleted_at !== null) {
                throw new GeneralError(errorMessages.already_deleted, this.constructor.name, undefined, 410);
            }
            eventEmitter.emit("api_key:revoke", { token: apiKey.code });
            await apiKey.destroy();
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
