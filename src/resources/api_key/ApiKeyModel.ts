"use strict";

import {
    DATE,
    STRING,
    Op,
    INTEGER,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface ApiKeySequelizeModel
    extends Model<InferAttributes<ApiKeySequelizeModel>, InferCreationAttributes<ApiKeySequelizeModel>> {
    id: CreationOptional<number>;
    user_id: number;
    code: CreationOptional<string>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
    deleted_at: CreationOptional<number>;
}

export interface ApiKeyModel extends ModelCtor<ApiKeySequelizeModel> {
    associate?: (models: any) => void;
}

const apiKeyModel = (sequelize: Sequelize) => {
    const apiKey: ApiKeyModel = sequelize.define(
        "api_key",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            user_id: {
                type: INTEGER,
                allowNull: false,
            },
            code: {
                type: STRING,
                unique: true,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
            deleted_at: {
                type: DATE,
            },
        },
        {
            indexes: [
                {
                    fields: ["code"],
                    unique: true,
                },
            ],
            paranoid: true,
            scopes: {
                deleted: {
                    paranoid: false,
                    where: {
                        deleted_at: {
                            [Op.ne]: null,
                        },
                    },
                },
            },
            timestamps: true,
        }
    );

    apiKey.associate = (models) => {
        apiKey.belongsTo(models.user);
    };

    return apiKey;
};

export { apiKeyModel };
