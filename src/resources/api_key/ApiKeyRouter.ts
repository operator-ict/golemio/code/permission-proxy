"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { accessPolicyMiddleware, checkErrors, checkId } from "../../core/middlewares";
import { ApiKeyController, createForm } from "./";

class ApiKeyRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: ApiKeyController;

    constructor() {
        this.baseUrl = "/api-keys";
        this.controller = new ApiKeyController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get([this.baseUrl], accessPolicyMiddleware.isSignedIn, this.controller.list);

        this.router.post(this.baseUrl, accessPolicyMiddleware.isSignedIn, createForm, checkErrors, this.controller.create);

        this.router.delete(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            checkId("id"),
            checkErrors,
            this.controller.remove
        );

        // TODO remove
        // Routes for backwards compatibility of the API
        this.router.get("/api-key", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}`);
        });
        this.router.post("/api-key", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}`);
        });
        this.router.delete("/api-key/:id", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/${req.params.id}`);
        });
    };
}

const apiKeyRouter: Router = new ApiKeyRouter().router;

export { apiKeyRouter };
