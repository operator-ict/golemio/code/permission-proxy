"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response } from "@golemio/core/dist/shared/express";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { ExtendedRequest } from "../../core/middlewares";
import { modelManager } from "../../core/models";

export class ChangePasswordController extends BaseController {
    public requestChange = async (req: ExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const oldPassword = req.body.old_password;
            const newPassword = req.body.new_password;
            const userId = req.session.user.id;

            const user = await modelManager.userModel.scope().findByPk(userId);
            const passwordOk = await modelManager.userModel.validate(user.password, oldPassword);
            if (!passwordOk) {
                throw new GeneralError(errorMessages.invalid_password, this.constructor.name, undefined, 403);
            }
            await user.update({
                password: await modelManager.userModel.encryptPassword(newPassword),
            });
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
