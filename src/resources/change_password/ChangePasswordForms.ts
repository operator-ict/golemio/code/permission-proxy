"use strict";

import { body } from "express-validator";
import { errorMessages } from "../../core/helpers";

const requestChangeForm = [
    body("old_password").exists().withMessage(errorMessages.required),
    body("new_password")
        .exists()
        .withMessage(errorMessages.required)
        .isLength({ min: 6 })
        .withMessage(errorMessages.value_not_allowed),
];

export { requestChangeForm };
