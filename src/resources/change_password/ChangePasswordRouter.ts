"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { accessPolicyMiddleware, checkErrors } from "../../core/middlewares";
import { ChangePasswordController, requestChangeForm } from "./";

class ChangePasswordRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: ChangePasswordController;

    constructor() {
        this.baseUrl = "/change-password";
        this.controller = new ChangePasswordController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post(
            `${this.baseUrl}`,
            accessPolicyMiddleware.isSignedIn,
            requestChangeForm,
            checkErrors,
            this.controller.requestChange
        );
    };
}

const changePasswordRouter: Router = new ChangePasswordRouter().router;

export { changePasswordRouter };
