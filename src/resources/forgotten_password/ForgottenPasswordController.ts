"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { v4 as uuidv4 } from "uuid";
import { config } from "../../core/config";
import { BaseController } from "../../core/controllers";
import { errorMessages, eventEmitter } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class ForgottenPasswordController extends BaseController {
    public requestReset = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const email = (req.body.email as string).toLowerCase();
            const routeLink = req.body.route_link;
            const user = await modelManager.userModel.findOne({
                where: {
                    email,
                },
            });
            if (!user) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            const forgottenPassword = await modelManager.forgottenPasswordModel.upsert(
                { user_id: user.id, code: uuidv4(), expiration: modelManager.forgottenPasswordModel.expiration },
                { conflictFields: ["user_id"], returning: true }
            );
            const resetLink = await modelManager.refererWhitelistModel.createLink(
                req.get("referer"),
                `${routeLink}${forgottenPassword[0].code}`
            );
            if (!resetLink) {
                throw new GeneralError(errorMessages.forbidden, this.constructor.name, undefined, 403);
            }
            eventEmitter.emit("mail:send", {
                locals: {
                    resetLink,
                    user,
                },
                recipients: user.email,
                templateName: "resetPassword",
            });
            if (config.node_env !== "production") {
                res.status(201).json({ token: forgottenPassword[0].code, resetLink });
            } else {
                res.sendStatus(201);
            }
        } catch (err) {
            next(err);
        }
    };

    public resetPassword = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        const transaction = await modelManager.getTransaction();
        try {
            const code = req.params.code;
            const password = req.body.password;

            const forgottenPassword = await modelManager.forgottenPasswordModel.findOne({
                attributes: {
                    include: [[modelManager.sequelize.literal("NOW() >= expiration"), "expired"]],
                },
                include: [
                    {
                        as: "user",
                        model: modelManager.userModel.scope(),
                        required: true,
                    },
                ],
                where: {
                    code,
                },
            });
            if (!forgottenPassword) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            if (forgottenPassword.expired) {
                throw new GeneralError(errorMessages.already_expired, this.constructor.name, undefined, 400);
            }

            await forgottenPassword.user.update({ password: modelManager.userModel.encryptPassword(password) }, { transaction });
            await forgottenPassword.destroy({ transaction });
            await transaction.commit();

            res.sendStatus(204);
        } catch (err) {
            await transaction.rollback();
            next(err);
        }
    };

    public codeCheck = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const code = req.params.code;
            const forgottenPassword = await modelManager.forgottenPasswordModel.findOne({
                attributes: {
                    include: [[modelManager.sequelize.literal("NOW() >= expiration"), "expired"]],
                },
                where: {
                    code,
                },
            });
            let error = null;
            if (!forgottenPassword) {
                error = errorMessages.not_found;
            } else if (forgottenPassword.expired) {
                error = errorMessages.already_expired;
            }
            res.status(200).json({ valid: error == null, error });
        } catch (err) {
            next(err);
        }
    };
}
