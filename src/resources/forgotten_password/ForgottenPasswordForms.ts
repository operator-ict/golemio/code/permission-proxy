"use strict";

import { body, param } from "express-validator";
import { errorMessages } from "../../core/helpers";

const codeCheckForm = [param("code").exists().withMessage(errorMessages.required).isUUID(4).withMessage(errorMessages.not_an_id)];

const resetPasswordForm = [
    param("code").exists().withMessage(errorMessages.required).isUUID(4).withMessage(errorMessages.not_an_id),
    body("password").exists().withMessage(errorMessages.required),
];

const requestResetForm = [
    body("email").exists().withMessage(errorMessages.required).isEmail().withMessage(errorMessages.not_an_email),
    body("route_link").exists().withMessage(errorMessages.required),
];

export { codeCheckForm, resetPasswordForm, requestResetForm };
