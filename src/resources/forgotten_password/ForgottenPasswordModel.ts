"use strict";

import {
    STRING,
    DATE,
    VIRTUAL,
    INTEGER,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
    LogicType,
} from "@golemio/core/dist/shared/sequelize";

import { config } from "../../core/config";
import { modelManager } from "../../core/models";

interface ForgottenPasswordSequelizeModel
    extends Model<InferAttributes<ForgottenPasswordSequelizeModel>, InferCreationAttributes<ForgottenPasswordSequelizeModel>> {
    [x: string]: any;
    id: CreationOptional<number>;
    user_id: number;
    code: string;
    expiration: number;
    expired: CreationOptional<boolean>;
}

export interface ForgottenPasswordModel extends ModelCtor<ForgottenPasswordSequelizeModel> {
    associate?: (models: any) => void;
    expiration?: LogicType;
}

const forgottenPasswordModel = (sequelize: Sequelize) => {
    const forgottenPassword: ForgottenPasswordModel = sequelize.define(
        "forgotten_password",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            user_id: {
                type: INTEGER,
                allowNull: false,
                unique: true,
            },
            code: {
                type: STRING,
                allowNull: false,
                unique: true,
            },
            expiration: {
                type: DATE,
                allowNull: false,
            },
            expired: {
                type: VIRTUAL,
            },
        },
        {
            indexes: [
                {
                    fields: ["user_id"],
                    unique: true,
                },
                {
                    fields: ["code"],
                    unique: true,
                },
            ],
            paranoid: false,
            timestamps: false,
        }
    );

    forgottenPassword.expiration = modelManager.sequelize.literal(
        `NOW() + INTERVAL '${config.forgotten_password_expiration} SECOND'`
    );

    forgottenPassword.associate = (models) => {
        forgottenPassword.belongsTo(models.user);
    };

    return forgottenPassword;
};

export { forgottenPasswordModel };
