"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { checkErrors } from "../../core/middlewares";
import { codeCheckForm, ForgottenPasswordController, requestResetForm, resetPasswordForm } from "./";

class ForgottenPasswordRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: ForgottenPasswordController;

    constructor() {
        this.baseUrl = "/forgotten-password";
        this.controller = new ForgottenPasswordController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(`${this.baseUrl}/:code`, codeCheckForm, checkErrors, this.controller.codeCheck);

        this.router.put(`${this.baseUrl}/:code`, resetPasswordForm, checkErrors, this.controller.resetPassword);

        this.router.post(`${this.baseUrl}`, requestResetForm, checkErrors, this.controller.requestReset);
    };
}

const forgottenPasswordRouter: Router = new ForgottenPasswordRouter().router;

export { forgottenPasswordRouter };
