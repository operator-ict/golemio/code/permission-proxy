"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class InvitationController extends BaseController {
    public checkInvitation = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const invitation = await modelManager.invitationModel.findOne({
                attributes: {
                    include: [[modelManager.sequelize.literal("NOW() >= expiration"), "expired"]],
                },
                include: [
                    {
                        as: "user",
                        model: modelManager.userModel.scope(),
                        required: true,
                    },
                ],
                where: {
                    code: req.params.code,
                },
            });
            if (!invitation) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            res.json({
                already_accepted: invitation.user.invitation_accepted_at !== null,
                already_expired: invitation.expired,
                need_password: invitation.user.password === null,
            });
        } catch (err) {
            next(err);
        }
    };

    public confirmInvitation = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const updateObject = { invitation_accepted_at: modelManager.sequelize.fn("NOW"), password: undefined };
            const invitation = await modelManager.invitationModel.findOne({
                attributes: {
                    include: [[modelManager.sequelize.literal("NOW() >= expiration"), "expired"]],
                },
                include: [
                    {
                        as: "user",
                        model: modelManager.userModel.scope(),
                        required: true,
                    },
                ],
                where: {
                    code: req.params.code,
                },
            });
            if (!invitation) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }

            const needPassword: boolean = invitation.user.password === null;
            if (needPassword && !req.body.password) {
                throw new GeneralError(errorMessages.password_required, this.constructor.name, undefined, 400);
            } else if (needPassword) {
                updateObject.password = await modelManager.userModel.encryptPassword(req.body.password);
            }

            if (invitation.expired) {
                throw new GeneralError(errorMessages.already_expired, this.constructor.name, undefined, 410);
            }

            if (invitation.user.invitation_accepted_at !== null) {
                throw new GeneralError(errorMessages.already_accepted, this.constructor.name, undefined, 500);
            }
            await invitation.user.update(updateObject);
            await invitation.destroy();
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
