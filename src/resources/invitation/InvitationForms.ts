"use strict";

import { body, param } from "express-validator";
import { errorMessages } from "../../core/helpers";

// TODO: rename to params, or better yet move to router
// for all "forms"

const confirmInvitationForm = [
    param("code").exists().withMessage(errorMessages.required).isUUID(4).withMessage(errorMessages.not_an_id),
    body("password").optional(),
];

const getInvitationForm = [
    param("code").exists().withMessage(errorMessages.required).isUUID(4).withMessage(errorMessages.not_an_id),
];

export { confirmInvitationForm, getInvitationForm };
