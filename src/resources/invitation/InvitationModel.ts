"use strict";

import {
    STRING,
    DATE,
    VIRTUAL,
    INTEGER,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
    LogicType,
} from "@golemio/core/dist/shared/sequelize";
import { v4 as uuidv4 } from "uuid";
import { config } from "../../core/config";
import { modelManager } from "../../core/models";

interface InvitationSequelizeModel
    extends Model<InferAttributes<InvitationSequelizeModel>, InferCreationAttributes<InvitationSequelizeModel>> {
    [x: string]: any;
    id: CreationOptional<number>;
    user_id: CreationOptional<number>;
    code: string;
    expiration: CreationOptional<number>;
    label: string;
    parent_id: CreationOptional<number>;
    created_by_user_id: CreationOptional<number>;
    updated_by_user_id: CreationOptional<number>;
    default: CreationOptional<boolean>;
    description: CreationOptional<string>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
    deleted_at: CreationOptional<number>;
}

export interface InvitationModel extends ModelCtor<InvitationSequelizeModel> {
    expiration?: LogicType;
    associate?: (models: any) => void;
    refreshInvitation?: (userId: number) => void;
}

const invitationModel = (sequelize: Sequelize) => {
    const invitation: InvitationModel = sequelize.define(
        "invitation",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            user_id: {
                type: INTEGER,
            },
            code: {
                type: STRING,
                allowNull: false,
                unique: true,
            },
            expiration: {
                type: DATE,
                allowNull: false,
            },
            expired: {
                type: VIRTUAL,
            },
        },
        {
            indexes: [
                {
                    fields: ["user_id"],
                    unique: true,
                },
                {
                    fields: ["code"],
                    unique: true,
                },
            ],
            paranoid: false,
            timestamps: false,
        }
    );

    invitation.expiration = modelManager.sequelize.literal(`NOW() + INTERVAL '${config.invitation_expiration} SECOND'`);

    invitation.associate = (models) => {
        invitation.belongsTo(models.user);
    };

    /**
     * Refresh invitation code for specific user
     * @param userId
     */
    invitation.refreshInvitation = (userId: number) =>
        invitation.upsert(
            { code: uuidv4(), expiration: invitation.expiration, user_id: userId },
            { conflictFields: ["user_id"], returning: true }
        );

    return invitation;
};

export { invitationModel };
