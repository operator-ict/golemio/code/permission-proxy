"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { checkErrors } from "../../core/middlewares";
import { confirmInvitationForm, getInvitationForm, InvitationController } from "./";

class InvitationRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: InvitationController;

    constructor() {
        this.baseUrl = "/invitations";
        this.controller = new InvitationController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(`${this.baseUrl}/:code`, getInvitationForm, checkErrors, this.controller.checkInvitation);

        this.router.put(`${this.baseUrl}/:code`, confirmInvitationForm, checkErrors, this.controller.confirmInvitation);

        // Routes for backwards compatibility of the API
        this.router.get("/invitation", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}`);
        });
        this.router.put("/invitation/:code", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/${req.params.code}`);
        });
    };
}

const invitationRouter: Router = new InvitationRouter().router;

export { invitationRouter };
