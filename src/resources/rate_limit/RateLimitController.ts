"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class RateLimitController extends BaseController {
    public detailByUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { userId } = req.params;
            const user = await modelManager.userModel.findOne({
                attributes: [],
                include: [
                    {
                        attributes: ["limit", "timewindow"],
                        model: modelManager.rateLimitModel,
                        required: false,
                        where: {
                            user_id: userId,
                        },
                    },
                ],
                where: {
                    id: userId,
                },
            });
            if (!user) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            res.json(user.rate_limit);
        } catch (err) {
            next(err);
        }
    };

    public upsert = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { userId } = req.params;
            const user = await modelManager.userModel.findOne({
                attributes: ["id"],
                where: {
                    id: userId,
                },
            });
            if (!user) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await modelManager.rateLimitModel.upsert({
                limit: req.body.limit,
                timewindow: req.body.timewindow,
                user_id: +userId,
            });
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
