"use strict";

import { body } from "express-validator";
import { errorMessages } from "../../core/helpers";

const upsertForm = [
    body("limit")
        .exists()
        .withMessage(errorMessages.required)
        .isInt()
        .withMessage(errorMessages.not_a_number)
        .isInt({ min: 0 })
        .withMessage(errorMessages.value_not_allowed),
    body("timewindow")
        .exists()
        .withMessage(errorMessages.required)
        .isInt()
        .withMessage(errorMessages.not_a_number)
        .isInt({ min: 0 })
        .withMessage(errorMessages.value_not_allowed),
];

export { upsertForm };
