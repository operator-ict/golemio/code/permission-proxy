"use strict";

import {
    INTEGER,
    DATE,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface RateLimitSequelizeModel
    extends Model<InferAttributes<RateLimitSequelizeModel>, InferCreationAttributes<RateLimitSequelizeModel>> {
    id: CreationOptional<number>;
    user_id: number;
    role_id: number;
    limit: number;
    timewindow: number;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
}

export interface RateLimitModel extends ModelCtor<RateLimitSequelizeModel> {
    associate?: (models: any) => void;
}

const rateLimitModel = (sequelize: Sequelize) => {
    const rateLimit: RateLimitModel = sequelize.define(
        "rate_limit",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            user_id: {
                type: INTEGER,
                allowNull: true,
                unique: true,
            },
            role_id: {
                type: INTEGER,
                allowNull: true,
                unique: true,
            },
            limit: {
                type: INTEGER,
                allowNull: false,
            },
            timewindow: {
                type: INTEGER,
                allowNull: false,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
        },
        {
            deletedAt: false,
            indexes: [
                {
                    fields: ["user_id"],
                    unique: true,
                },
                {
                    fields: ["role_id"],
                    unique: true,
                },
            ],
            timestamps: true,
        }
    );

    rateLimit.associate = (models) => {
        rateLimit.belongsTo(models.user);
        rateLimit.belongsTo(models.role);
    };

    return rateLimit;
};

export { rateLimitModel };
