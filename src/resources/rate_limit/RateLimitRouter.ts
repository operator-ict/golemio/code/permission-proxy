"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { accessPolicyMiddleware, checkErrors, checkId } from "../../core/middlewares";
import { RateLimitController, upsertForm } from "./";

class RateLimitRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: RateLimitController;

    constructor() {
        this.baseUrl = "/users/:userId/rate-limits";
        this.controller = new RateLimitController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId(["userId"]),
            checkErrors,
            this.controller.detailByUser
        );

        this.router.post(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            upsertForm,
            checkId(["userId"]),
            checkErrors,
            this.controller.upsert
        );

        this.router.put(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            upsertForm,
            checkId(["userId"]),
            checkErrors,
            this.controller.upsert
        );

        // Routes for backwards compatibility of the API
        this.router.put("/user/:userId/rate-limit", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}`);
        });
    };
}

const rateLimitRouter: Router = new RateLimitRouter().router;

export { rateLimitRouter };
