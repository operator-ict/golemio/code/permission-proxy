"use strict";

import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { BaseController } from "../../core/controllers";
import { modelManager } from "../../core/models";

export class RefererWhitelistController extends BaseController {
    public list = (req: Request, res: Response, next: NextFunction) => {
        modelManager.refererWhitelistModel
            .findAll({
                order: [["referer", "DESC"]],
            })
            .then((referers) => res.json(referers))
            .catch(next);
    };
}
