"use strict";

import {
    STRING,
    INTEGER,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface RefererWhitelistSequelizeModel
    extends Model<InferAttributes<RefererWhitelistSequelizeModel>, InferCreationAttributes<RefererWhitelistSequelizeModel>> {
    id: CreationOptional<number>;
    referer: string;
}

export interface RefererWhitelistModel extends ModelCtor<RefererWhitelistSequelizeModel> {
    associate?: (models: any) => void;
    findBySubstring?: (referer: string) => Promise<any>;
    createLink?: (referer: string, path: string) => any;
}

const refererWhitelistModel = (sequelize: Sequelize) => {
    const refererWhitelist: RefererWhitelistModel = sequelize.define(
        "referer_whitelist",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            referer: {
                type: STRING,
                allowNull: false,
            },
        },
        {
            timestamps: false,
        }
    );

    refererWhitelist.findBySubstring = (referer: string) => {
        return refererWhitelist.findOne({
            where: sequelize.literal(`referer = substring(${sequelize.escape(referer)} from 0 for length(referer) + 1)`),
        });
    };

    refererWhitelist.createLink = (referer: string, path: string) => {
        return refererWhitelist.findBySubstring(referer).then((item) => (item ? `${item.referer}${path}` : null));
    };

    refererWhitelist.associate = (models) => {
        refererWhitelist.hasMany(models.user, { foreignKey: "initial_referer" });
    };

    return refererWhitelist;
};

export { refererWhitelistModel };
