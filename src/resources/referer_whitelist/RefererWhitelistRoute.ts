"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { accessPolicyMiddleware } from "../../core/middlewares";
import { RefererWhitelistController } from "./";

class RefererWhitelistRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: RefererWhitelistController;

    constructor() {
        this.baseUrl = "/referer-whitelist";
        this.controller = new RefererWhitelistController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(this.baseUrl, accessPolicyMiddleware.isSignedIn, accessPolicyMiddleware.isAdmin, this.controller.list);
    };
}

const refererWhitelistRouter: Router = new RefererWhitelistRouter().router;

export { refererWhitelistRouter };
