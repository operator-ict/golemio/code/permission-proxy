"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { Op, Order } from "@golemio/core/dist/shared/sequelize";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class RoleController extends BaseController {
    public list = async (req: Request<{}, any, any, any>, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { limit, offset, page, search, orderBy, orderDir } = req.query;
            const searchLike = search ? `%${search}%` : null;
            let order: Order = [["label", "DESC"]];
            if (orderBy) {
                order = [[orderBy, orderDir]];
                if (orderBy === "parent_name") {
                    order = [["parent", "label", orderDir]];
                }
            }
            const result = await modelManager.roleModel.findAndCountAll({
                include: [
                    {
                        as: "parent",
                        model: modelManager.roleModel,
                        required: false,
                    },
                    {
                        as: "children",
                        model: modelManager.roleModel,
                        required: false,
                    },
                ],
                limit,
                offset,
                order,
                where: searchLike && {
                    [Op.or]: [
                        { label: { [Op.iLike]: searchLike } },
                        { description: { [Op.iLike]: searchLike } },
                        // modelManager.sequelize.literal(
                        //     `"parent"."label" ILIKE ${modelManager.sequelize.escape(searchLike)}`),
                    ],
                },
            });
            this.setPaginationHeaders(res, result.count, limit, page).json(result.rows.map((x: any) => this.mapRole(x)));
        } catch (err) {
            next(err);
        }
    };

    public duplicates = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const roles = await modelManager.vDuplicateLimitationsModel.findAll();
            res.json(roles);
        } catch (err) {
            next(err);
        }
    };

    public detail = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const roleId = req.params.id;
            const role = await modelManager.roleModel.findByPk(roleId, {
                include: [
                    modelManager.scopeModel,
                    {
                        as: "parent",
                        model: modelManager.roleModel,
                    },
                    {
                        as: "children",
                        model: modelManager.roleModel,
                    },
                    {
                        attributes: ["limit", "timewindow"],
                        model: modelManager.rateLimitModel,
                        required: false,
                    },
                ],
            });
            if (!role) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            res.json(this.mapRole(role));
        } catch (err) {
            next(err);
        }
    };

    public create = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const parentId = req.body.parent_id ? parseInt(req.body.parent_id, 10) : null;

            let parentRole = null;
            if (parentId) {
                parentRole = await modelManager.roleModel.findByPk(parentId);
            }

            if (parentId) {
                if (!parentRole) {
                    throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
                }
                if (parentRole.parent_id) {
                    // requested parent is already child of some other parent, so it can not be assigned as parent
                    // only 2-level structure is allowed
                    throw new GeneralError(errorMessages.parent_role_not_allowed, this.constructor.name, undefined, 400);
                }
            }
            const role = await modelManager.roleModel.create(
                {
                    ...req.body,
                    rate_limit: req.body.rateLimit
                        ? {
                              limit: req.body.rateLimit.limit,
                              timewindow: req.body.rateLimit.timewindow,
                          }
                        : null,
                },
                {
                    include: [{ model: modelManager.rateLimitModel }],
                }
            );
            res.status(201).json(this.mapRole(role));
        } catch (err) {
            next(err);
        }
    };

    public remove = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const roleId = req.params.id;
            const role = await modelManager.roleModel.findByPk(roleId);
            if (!role) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await modelManager.rateLimitModel.destroy({
                where: { role_id: roleId },
            });
            await role.destroy();

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    public patch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const roleId = req.params.id;
            const parentId = req.body.parent_id ? parseInt(req.body.parent_id, 10) : undefined;

            const role = await modelManager.roleModel.findByPk(roleId, {
                include: [
                    modelManager.scopeModel,
                    {
                        as: "children",
                        model: modelManager.roleModel,
                    },
                ],
            });

            let parentRole = null;
            if (parentId) {
                if (parentId === parseInt(roleId, 10)) {
                    // must not be parent to itself
                    throw new GeneralError(
                        errorMessages.parent_role_not_allowed,
                        this.constructor.name,
                        errorMessages.parent_role_not_allowed,
                        400
                    );
                }

                parentRole = await modelManager.roleModel.findByPk(parentId);
            }

            if (!role) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            if (parentId) {
                const editedRoleIsParent: boolean = role.children && role.children.length > 0;
                if (editedRoleIsParent) {
                    // edited role already has children == is parent of some other roles
                    // only 2-level structure is allowed
                    throw new GeneralError(
                        errorMessages.role_can_not_have_parent,
                        this.constructor.name,
                        errorMessages.role_can_not_have_parent,
                        400
                    );
                }

                if (!parentRole) {
                    throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
                }
                if (parentRole.parent_id) {
                    // requested parent is already child of some other parent, so it can not be assigned as parent
                    // only 2-level structure is allowed
                    throw new GeneralError(
                        errorMessages.parent_role_not_allowed,
                        this.constructor.name,
                        errorMessages.parent_role_not_allowed,
                        400
                    );
                }
            }
            await role.update({
                description: req.body.description,
                label: req.body.label,
                parent_id: parentId,
            });

            if (req.body.rateLimit && req.body.rateLimit.limit != null && req.body.rateLimit.timewindow != null) {
                await modelManager.rateLimitModel.upsert({
                    limit: req.body.rateLimit.limit,
                    timewindow: req.body.rateLimit.timewindow,
                    role_id: +roleId,
                });
            } else {
                await modelManager.rateLimitModel.destroy({
                    where: { role_id: roleId },
                });
            }

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    protected mapRole = (x: any): any => {
        return {
            created_at: x.created_at,
            default: x.default,
            description: x.description,
            id: x.id,
            isParent: x.children && x.children.length > 0,
            label: x.label,
            parent_id: x.parent_id,
            parent_name: x.parent ? x.parent.label : null,
            scopes: x.scopes,
            updated_at: x.updated_at,
            rateLimit: x.rate_limit
                ? {
                      limit: x.rate_limit.limit,
                      timewindow: x.rate_limit.timewindow,
                  }
                : null,
        };
    };
}
