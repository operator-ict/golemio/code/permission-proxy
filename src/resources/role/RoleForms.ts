"use strict";

import { body, ValidationChain } from "express-validator";
import { Request } from "express-validator/src/base";
import { errorMessages } from "../../core/helpers";

const hasRateLimit = (req: Request): boolean => {
    return req.body.rateLimit != null;
};
const isValidRateLimitField = (field: string): ValidationChain => {
    const result = body(field)
        .optional()
        .custom((value, { req }) => {
            return !(hasRateLimit(req) && value === undefined);
        })
        .withMessage(errorMessages.required)
        .custom((value, { req }) => {
            if (hasRateLimit(req)) {
                const nValue = parseInt(value, 10);
                return !isNaN(nValue);
            }
            return true;
        })
        .withMessage(errorMessages.not_an_id)
        .custom((value, { req }) => {
            if (hasRateLimit(req)) {
                const nValue = parseInt(value, 10);
                return !isNaN(nValue) && nValue >= 0;
            }
            return true;
        })
        .withMessage(errorMessages.value_not_allowed);

    return result;
};

const createForm = [
    body("label").exists().not().isEmpty().withMessage(errorMessages.required),
    body("description").optional(),
    body("parent_id").optional({ nullable: true }).isInt({ min: 0 }).withMessage(errorMessages.not_an_id),
    body("rateLimit").optional(),
    isValidRateLimitField("rateLimit.limit"),
    isValidRateLimitField("rateLimit.timewindow"),
];

const patchForm = [
    body("label").optional(),
    body("description").optional(),
    body("parent_id").optional({ nullable: true }).isInt({ min: 0 }).withMessage(errorMessages.not_an_id),
    body("rateLimit").optional(),
    isValidRateLimitField("rateLimit.limit"),
    isValidRateLimitField("rateLimit.timewindow"),
];

export { createForm, patchForm };
