"use strict";

import {
    INTEGER,
    BOOLEAN,
    STRING,
    DATE,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface RoleSequelizeModel extends Model<InferAttributes<RoleSequelizeModel>, InferCreationAttributes<RoleSequelizeModel>> {
    [x: string]: any;
    id: CreationOptional<number>;
    label: string;
    parent_id: CreationOptional<number>;
    created_by_user_id: CreationOptional<number>;
    updated_by_user_id: CreationOptional<number>;
    default: CreationOptional<boolean>;
    description: CreationOptional<string>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
    deleted_at: CreationOptional<number>;
}

export interface RoleModel extends ModelCtor<RoleSequelizeModel> {
    associate?: (models: any) => void;
}

const roleModel = (sequelize: Sequelize) => {
    const role: RoleModel = sequelize.define<RoleSequelizeModel>(
        "role",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            label: {
                type: STRING,
                allowNull: false,
            },
            parent_id: {
                type: INTEGER,
            },
            created_by_user_id: {
                type: INTEGER,
            },
            updated_by_user_id: {
                type: INTEGER,
            },
            default: {
                type: BOOLEAN,
            },
            description: {
                type: STRING,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
            deleted_at: {
                type: DATE,
            },
        },
        {
            defaultScope: {
                attributes: {
                    exclude: ["created_by_user_id", "updated_by_user_id"],
                },
            },
            timestamps: true,
        }
    );

    role.associate = (models) => {
        role.hasMany(models.user_role);
        role.hasMany(models.scope);
        role.belongsTo(models.role, { as: "parent", foreignKey: "parent_id" });
        role.hasMany(models.role, { as: "children", foreignKey: "parent_id" });
        role.hasOne(models.rate_limit);
    };

    return role;
};

export { roleModel };
