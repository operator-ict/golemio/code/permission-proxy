"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import * as url from "url";
import { accessPolicyMiddleware, checkErrors, checkId, checkListOptions, checkOrdering } from "../../core/middlewares";
import { createForm, patchForm, RoleController } from "./";

class RoleRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: RoleController;

    constructor() {
        this.baseUrl = "/roles";
        this.controller = new RoleController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkListOptions,
            checkOrdering(["label", "description", "parent_id", "parent_name", "created_at", "updated_at"]),
            this.controller.list
        );

        this.router.get(
            `${this.baseUrl}/duplicates`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            this.controller.duplicates
        );

        this.router.get(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId("id"),
            checkErrors,
            this.controller.detail
        );

        this.router.post(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            createForm,
            checkErrors,
            this.controller.create
        );

        this.router.patch(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            patchForm,
            checkId("id"),
            checkErrors,
            this.controller.patch
        );

        this.router.delete(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId("id"),
            checkErrors,
            this.controller.remove
        );

        // Routes for backwards compatibility of the API
        this.router.get("/role", (req, res) => {
            res.redirect(
                307,
                url.format({
                    pathname: `/gateway${this.baseUrl}`,
                    query: req.query as any,
                })
            );
        });
        this.router.get("/role/duplicate", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/duplicate`);
        });
        this.router.get("/role/:id", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/${req.params.id}`);
        });
        this.router.post("/role", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}`);
        });
        this.router.patch("/role/:id", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/${req.params.id}`);
        });
        this.router.delete("/role/:id", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/${req.params.id}`);
        });
    };
}

const roleRouter: Router = new RoleRouter().router;

export { roleRouter };
