"use strict";

import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, col, fn } from "@golemio/core/dist/shared/sequelize";
import { RedisPubSubChannel } from "core/helpers/pubsub/RedisPubSubChannel";
import { config } from "../../core/config/config";
import { BaseController } from "../../core/controllers";
import { errorMessages, eventEmitter } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class RouteController extends BaseController {
    constructor(private pubSubChannel?: RedisPubSubChannel) {
        super();
    }

    public list = async (req: Request<{}, any, any, any>, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { limit, offset, page, search, orderBy, orderDir } = req.query;
            const searchLike = search ? `%${search}%` : null;

            let realOrderBy = orderBy;
            if (orderBy && orderBy.toLowerCase() === "public") {
                realOrderBy = fn("COALESCE", col("public"), false);
            }

            const result = await modelManager.routeModel.findAndCountAll({
                limit,
                offset,
                order: realOrderBy ? [[realOrderBy, orderDir]] : [["endpoint", "ASC"]],
                where: searchLike && {
                    [Op.or]: [
                        { endpoint: { [Op.iLike]: searchLike } },
                        { method: { [Op.iLike]: searchLike } },
                        { target_path: { [Op.iLike]: searchLike } },
                        { target_url: { [Op.iLike]: searchLike } },
                    ],
                },
            });
            this.setPaginationHeaders(res, result.count, limit, page).json(result.rows.map((x) => this.mapItem(x)));
        } catch (err) {
            next(err);
        }
    };

    public targets = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const routeTargets = await modelManager.routeModel.findAll({
                // attributes: ["target_url"],
                attributes: [[fn("DISTINCT", col("target_url")), "target_url"]],
                order: [["target_url", "ASC"]],
            });
            res.json(routeTargets.map((x) => x.target_url));
        } catch (err) {
            next(err);
        }
    };

    public detail = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const routeId = req.params.id;
            const route = await modelManager.routeModel.findByPk(routeId);
            if (!route) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            res.json(this.mapItem(route));
        } catch (err) {
            next(err);
        }
    };

    public userList = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const result = await modelManager.vAllUserRoleRouteDistinctModel.findAll({
                // TODO if user not found, return 404 (now 200 and empty array)
                where: {
                    user_id: req.params.userId,
                },
            });
            res.json(result);
        } catch (err) {
            next(err);
        }
    };

    public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const route = await modelManager.routeModel.create(req.body);
            await this.reloadProxyConfiguration();
            res.status(201).json(this.mapItem(route));
        } catch (err) {
            next(err);
        }
    };

    public patch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const routeId = req.params.id;
            const route = await modelManager.routeModel.findByPk(routeId);
            if (!route) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await route.update(req.body);
            await this.reloadProxyConfiguration();
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    public remove = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const route = await modelManager.routeModel.findByPk(req.params.id);
            if (!route) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await route.destroy();
            await this.reloadProxyConfiguration();
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    protected mapItem = (route: any) => {
        if (route) {
            return {
                id: route.id,
                method: route.method,
                endpoint: route.endpoint,
                target_url: route.target_url,
                target_path: route.target_path,
                public: route.public,
                is_redirect: route.is_redirect,
                created_at: route.created_at,
                updated_at: route.updated_at,
            };
        }

        return null;
    };

    private reloadProxyConfiguration = async (): Promise<void> => {
        if (!config.redis_enable || !this.pubSubChannel) {
            await eventEmitter.emitThen("proxy:reload"); // publish changes
        } else {
            await this.pubSubChannel.publishMessage("reload");
        }
    };
}
