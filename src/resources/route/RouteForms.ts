"use strict";

import { body } from "express-validator";
import { errorMessages } from "../../core/helpers";

export const METHODS = ["GET", "POST", "PATCH", "PUT", "DELETE"];

// validates url so it has to start with http or https
// following by host name (should be fully qualified domain name, e.g. domain.com, sub.domain.com
// or alias host name, e.g. localhost, input-gateway-v1 - allowed characters are: A-Za-z0-9._-)
// optionally following by colon and port number that has to consist of 2-4 digits (ie. interval 10-9999)
// Examples:
// http://localhost:3000
// http://input-gateway-v1
// https://some.domain.com
// https://some.domain.com:3000
const routeTargetUrl = /^((https?:\/\/)([\.A-Z\d_-]+)((:\d{2,4})?))$/i;

const startsWithSlash = /^\//;

const createForm = [
    body("endpoint")
        .exists()
        .withMessage(errorMessages.required)
        .matches(startsWithSlash)
        .withMessage(errorMessages.must_start_with_slash),
    body("target_url")
        .exists()
        .withMessage(errorMessages.required)
        .matches(routeTargetUrl)
        .withMessage(errorMessages.protocol_host_port_format_required),
    body("target_path")
        .optional({ nullable: true, checkFalsy: true })
        .matches(startsWithSlash)
        .withMessage(errorMessages.must_start_with_slash),
    body("method").exists().withMessage(errorMessages.required).isIn(METHODS).withMessage(errorMessages.value_not_allowed),
    body("public").optional().isBoolean().withMessage(errorMessages.not_a_boolean),
    body("is_redirect").optional().isBoolean().withMessage(errorMessages.not_a_boolean),
];

const patchForm = [
    body("endpoint").optional().matches(startsWithSlash).withMessage(errorMessages.must_start_with_slash),
    body("target_url").optional().matches(routeTargetUrl).withMessage(errorMessages.protocol_host_port_format_required),
    body("target_path")
        .optional()
        // to enable update with empty string or null, otherwise this would not fulfill next RegExp validation
        .if((value) => value)
        .matches(startsWithSlash)
        .withMessage(errorMessages.must_start_with_slash),
    body("method").optional().isIn(METHODS).withMessage(errorMessages.value_not_allowed),
    body("public").optional().isBoolean().withMessage(errorMessages.not_a_boolean),
    body("is_redirect").optional().isBoolean().withMessage(errorMessages.not_a_boolean),
];

export { createForm, patchForm };
