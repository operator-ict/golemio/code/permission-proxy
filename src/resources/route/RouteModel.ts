"use strict";

import {
    INTEGER,
    STRING,
    BOOLEAN,
    ENUM,
    DATE,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";
import { METHODS } from "./RouteForms";

interface RouteSequelizeModel extends Model<InferAttributes<RouteSequelizeModel>, InferCreationAttributes<RouteSequelizeModel>> {
    id: CreationOptional<number>;
    endpoint: string;
    method: string;
    target_url: string;
    created_by_user_id: CreationOptional<number>;
    updated_by_user_id: CreationOptional<number>;
    is_redirect: CreationOptional<boolean>;
    public: CreationOptional<boolean>;
    target_path: CreationOptional<string>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
    deleted_at: CreationOptional<number>;
}

export interface RouteModel extends ModelCtor<RouteSequelizeModel> {}

const routeModel = (sequelize: Sequelize) => {
    const route: RouteModel = sequelize.define(
        "route",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            endpoint: {
                type: STRING,
                allowNull: false,
            },
            method: {
                type: ENUM(...METHODS),
                allowNull: false,
            },
            target_url: {
                type: STRING,
                allowNull: false,
            },
            created_by_user_id: {
                type: INTEGER,
            },
            updated_by_user_id: {
                type: INTEGER,
            },
            is_redirect: {
                type: BOOLEAN,
            },
            public: {
                type: BOOLEAN,
            },
            target_path: {
                type: STRING,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
            deleted_at: {
                type: DATE,
            },
        },
        {
            defaultScope: {
                attributes: {
                    exclude: ["created_by_user_id", "updated_by_user_id"],
                },
            },
            timestamps: true,
        }
    );

    return route;
};

export { routeModel };
