"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response, Router } from "@golemio/core/dist/shared/express";
import * as url from "url";
import { errorMessages } from "../../core/helpers";
import {
    accessPolicyMiddleware,
    checkErrors,
    checkId,
    checkListOptions,
    checkOrdering,
    ExtendedRequest,
} from "../../core/middlewares";
import { createForm, patchForm, RouteController } from "./";
import { RedisPubSubChannel } from "core/helpers/pubsub/RedisPubSubChannel";

export class RouteRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: RouteController;

    constructor(pubSubChannel?: RedisPubSubChannel) {
        this.baseUrl = "/routes";
        this.controller = new RouteController(pubSubChannel);
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkListOptions,
            checkOrdering([
                "endpoint",
                "method",
                "public",
                "target_path",
                "target_url",
                "is_redirect",
                "created_at",
                "updated_at",
            ]),
            this.controller.list
        );

        this.router.get(
            `${this.baseUrl}/targets`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            this.controller.targets
        );

        this.router.get(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId("id"),
            checkErrors,
            this.controller.detail
        );

        this.router.post(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            createForm,
            checkErrors,
            this.controller.create
        );

        this.router.patch(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            patchForm,
            checkId("id"),
            checkErrors,
            this.controller.patch
        );

        this.router.delete(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId("id"),
            checkErrors,
            this.controller.remove
        );

        // Routes for backwards compatibility of the API
        this.router.get("/route", (req, res) => {
            res.redirect(
                307,
                url.format({
                    pathname: `/gateway${this.baseUrl}`,
                    query: req.query as any,
                })
            );
        });
        this.router.post("/route/publish", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/publish`);
        });
        this.router.post("/route", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}`);
        });
        this.router.delete("/route/:id", (req, res) => {
            res.redirect(307, `/gateway${this.baseUrl}/${req.params.id}`);
        });

        // TODO should be in user router?
        this.router.get(
            `/users/:userId${this.baseUrl}`,
            checkId("userId"),
            accessPolicyMiddleware.isSignedIn,
            (req: ExtendedRequest, res: Response, next: NextFunction) => {
                if (req.session.user.admin) {
                    return next();
                }

                if (parseInt(req.params.userId, 10) !== req.session.user.id) {
                    throw new GeneralError(errorMessages.forbidden, "route", undefined, 403);
                }

                return next();
            },
            this.controller.userList
        );

        // Routes for backwards compatibility of the API
        this.router.get("/user/:userId/route", (req, res) => {
            res.redirect(307, `/gateway/users/${req.params.userId}${this.baseUrl}`);
        });
    };
}
