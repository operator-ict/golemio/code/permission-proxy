"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { and, Op, Order } from "@golemio/core/dist/shared/sequelize";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { modelManager } from "../../core/models";
import { limitations } from "../../proxy";

export class ScopeController extends BaseController {
    private limitationNames: string[];

    constructor() {
        super();
        this.limitationNames = Object.keys(limitations);
    }

    public limitationList = (req: Request, res: Response, next: NextFunction): void => {
        res.json(this.limitationNames);
    };

    public list = async (req: Request<{ roleId: number }, any, any, any>, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { limit, offset, page, search, orderBy, orderDir } = req.query;
            const searchLike = search ? `%${search}%` : null;

            let realOrderBy: Order = [["created_at", "DESC"]];
            if (orderBy) {
                realOrderBy = [[orderBy, orderDir]];
                if (orderBy === "route.endpoint") {
                    realOrderBy = [["route", "endpoint", orderDir]];
                }
            }

            const roleId = req.params.roleId;

            const whereSearch = searchLike ? { "$route.endpoint$": { [Op.iLike]: searchLike } } : {};

            const { count, rows } = await modelManager.scopeModel.findAndCountAll({
                include: [
                    {
                        attributes: ["endpoint", "method"],
                        model: modelManager.routeModel,
                    },
                ],
                limit,
                offset,
                order: realOrderBy,
                where: and({ role_id: roleId }, whereSearch),
            });
            this.setPaginationHeaders(res, count, limit, page).json(rows);
        } catch (err) {
            next(err);
        }
    };

    public detail = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const scopeId = req.params.id;

            const scope = await modelManager.scopeModel.findByPk(scopeId, {
                include: [
                    {
                        attributes: ["endpoint", "method"],
                        model: modelManager.routeModel,
                    },
                ],
            });
            if (!scope) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            res.json(scope);
        } catch (err) {
            next(err);
        }
    };

    public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const roleId = req.params.roleId;
            const scope = await modelManager.scopeModel.create({ ...req.body, role_id: roleId });
            res.status(201).json(this.mapItem(scope));
        } catch (err) {
            next(err);
        }
    };

    public remove = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const scopeId = req.params.id;
            const scope = await modelManager.scopeModel.findByPk(scopeId, {});
            if (!scope) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await scope.destroy();
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    public patch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const roleId = req.params.roleId;
            const scopeId = req.params.id;
            const scope = await modelManager.scopeModel.findByPk(scopeId);
            if (!scope) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await scope.update({ ...req.body, role_id: roleId });
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    protected mapItem = (scope: any) => {
        if (scope) {
            return {
                id: scope.id,
                role_id: scope.role_id,
                route_id: scope.route_id,
                limitations: scope.limitations,
                created_at: scope.created_at,
                updated_at: scope.updated_at,
            };
        }

        return null;
    };
}
