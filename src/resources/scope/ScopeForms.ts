"use strict";

import { body } from "express-validator";
import { Request } from "express-validator/src/base";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { errorMessages } from "../../core/helpers";
import { limitations, MinMaxTypeEnum } from "../../proxy";

const limitationNames = Object.keys(limitations);

const isNumericRegexp = /^((\d+)|((\d+)\.(\d+)))$/;

const getLimitationIndex = (path: string) => {
    const indexBrackets = path.match(/(\[\d+\])/); // index within brackets, e.g. [0]
    const index = parseInt(indexBrackets[0].match(/(\d+)/)[0], 10); // extract number from brackets, e.g. 0
    return index;
};

const getLimitationType = (req: Request, path: string) => {
    const index = getLimitationIndex(path);
    const type: string = req.body.limitations[index].type;
    return type ? type.toLowerCase() : null;
};

const getLimitationMinMaxType = (req: Request, path: string) => {
    const index = getLimitationIndex(path);
    const type: string = req.body.limitations[index].minMaxType;
    return type ? type.toLowerCase() : null;
};

const isInArrayLimitation = (req: Request, path: string) => {
    const type = getLimitationType(req, path);
    return type === "in-array";
};

const isMinMaxLimitation = (req: Request, path: string) => {
    const type = getLimitationType(req, path);
    return type === "min-max";
};

const limitationsValidation = [
    body("limitations").optional().isArray().withMessage(errorMessages.not_an_array),
    body("limitations.*.name").exists().withMessage(errorMessages.required),
    body("limitations.*.type")
        .exists()
        .withMessage(errorMessages.required)
        .isIn(limitationNames)
        .withMessage(errorMessages.value_not_allowed),
    body("limitations.*.minMaxType")
        .if((value, { req, path }) => {
            return isMinMaxLimitation(req, path);
        })
        .exists()
        .withMessage(errorMessages.required)
        .if((value, { req, path }) => {
            return getLimitationMinMaxType(req, path);
        })
        .isIn(Object.values(MinMaxTypeEnum))
        .withMessage(errorMessages.value_not_allowed),
    body("limitations.*.block")
        .if((value, { req, path }) => {
            return isMinMaxLimitation(req, path);
        })
        .exists()
        .withMessage(errorMessages.required)
        .isBoolean()
        .withMessage(errorMessages.not_a_boolean),
    body("limitations.*.values")
        .if((value, { req, path }) => {
            return isInArrayLimitation(req, path);
        })
        .exists()
        .withMessage(errorMessages.required)
        .isArray()
        .withMessage(errorMessages.not_an_array),
    body("limitations.*.min")
        .if((value, { req, path }) => {
            return isMinMaxLimitation(req, path);
        })
        .exists()
        .withMessage(errorMessages.required)
        .custom((value, { req }) => {
            return (req.body.minMaxType === "number" && !value.match(isNumericRegexp)) || !isNaN(new Date(value).getDate());
        })
        .withMessage(errorMessages.value_not_allowed),
    body("limitations.*.max")
        .if((value, { req, path }) => {
            return isMinMaxLimitation(req, path);
        })
        .exists()
        .withMessage(errorMessages.required)
        .custom((value, { req }) => {
            return (req.body.minMaxType === "number" && !value.match(isNumericRegexp)) || !isNaN(new Date(value).getDate());
        })
        .withMessage(errorMessages.value_not_allowed)
        .custom((value, { req, path }) => {
            const index = getLimitationIndex(path);
            const current = req.body.limitations[index];
            return (
                (current.minMaxType === "number" && current.min < parseFloat(value)) ||
                DateTime.fromISO(current.min) < DateTime.fromISO(value)
            );
        })
        .withMessage(errorMessages.min_max_invalid),
    body("limitations.*.default")
        .optional()
        .if((value, { req, path }) => {
            return isMinMaxLimitation(req, path);
        })
        .exists()
        .withMessage(errorMessages.required)
        .custom((value, { req }) => {
            return (req.body.minMaxType === "number" && !value.match(isNumericRegexp)) || !isNaN(new Date(value).getDate());
        })
        .withMessage(errorMessages.value_not_allowed)
        .custom((value, { req, path }) => {
            const index = getLimitationIndex(path);
            const current = req.body.limitations[index];
            return (
                (current.minMaxType === "number" && current.min <= parseFloat(value) && parseFloat(value) <= current.max) ||
                (DateTime.fromISO(current.min) <= DateTime.fromISO(value) &&
                    DateTime.fromISO(current.max) >= DateTime.fromISO(value))
            );
        })
        .withMessage(errorMessages.min_max_invalid),
];

const createForm = [
    body("route_id").exists().withMessage(errorMessages.required).isInt({ min: 0 }).withMessage(errorMessages.not_an_id),
    ...limitationsValidation,
];

const patchForm = [
    body("route_id").exists().withMessage(errorMessages.required).isInt({ min: 0 }).withMessage(errorMessages.not_an_id),
    ...limitationsValidation,
];

export { createForm, patchForm };
