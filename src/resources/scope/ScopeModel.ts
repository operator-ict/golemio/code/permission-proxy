"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import {
    Op,
    INTEGER,
    JSONB,
    DATE,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";
import { errorMessages } from "../../core/helpers";
import { getMaxRateLimit, getUniqueBy, unionBy } from "../../core/helpers/Utils";
import { modelManager } from "../../core/models";
import { limitations } from "../../proxy";

interface ScopeSequelizeModel extends Model<InferAttributes<ScopeSequelizeModel>, InferCreationAttributes<ScopeSequelizeModel>> {
    id: CreationOptional<number>;
    limitations: CreationOptional<string>;
    role_id: number;
    route_id: number;
    created_by_user_id: CreationOptional<number>;
    updated_by_user_id: CreationOptional<number>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
    deleted_at: CreationOptional<number>;
}

export interface ScopeModel extends ModelCtor<ScopeSequelizeModel> {
    findAllPublicScopesForRoute?: (method: string, path: string) => any;
    findAllUserScopesForRoute?: (userId: number, method: string, path: string) => any;
    associate?: (models: any) => void;
}

const scopeModel = (sequelize: Sequelize) => {
    const scope: ScopeModel = sequelize.define(
        "scope",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            limitations: {
                type: JSONB,
            },
            role_id: {
                type: INTEGER,
                allowNull: false,
            },
            route_id: {
                type: INTEGER,
                allowNull: false,
            },
            created_by_user_id: {
                type: INTEGER,
            },
            updated_by_user_id: {
                type: INTEGER,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
            deleted_at: {
                type: DATE,
            },
        },
        {
            defaultScope: {
                attributes: {
                    exclude: ["created_by_user_id", "updated_by_user_id"],
                },
            },
            deletedAt: false,
            indexes: [
                {
                    fields: ["user_id", "scope_id"],
                    unique: true,
                },
            ],
            paranoid: false,
            timestamps: true,
        }
    );

    scope.associate = (models) => {
        // scope.belongsTo(models.user);
        scope.belongsTo(models.route);
        scope.belongsTo(models.role);
    };

    // TODO: this is also too long.
    /**
     *
     * @param {int} userId
     * @param {string} method
     * @param {string} path
     * @returns {Promise<Scope[]>}
     */
    scope.findAllUserScopesForRoute = (userId: number, method: string, path: string) =>
        modelManager.roleModel
            .findAll({
                include: [
                    {
                        model: modelManager.rateLimitModel,
                        required: false,
                        attributes: ["role_id", "limit", "timewindow"],
                    },
                    {
                        model: modelManager.userRoleModel,
                        where: {
                            user_id: userId,
                        },
                    },
                    {
                        as: "parent",
                        model: modelManager.roleModel,
                        include: [
                            {
                                model: modelManager.rateLimitModel,
                                required: false,
                                attributes: ["role_id", "limit", "timewindow"],
                            },
                            {
                                model: scope,
                                include: [
                                    {
                                        model: modelManager.routeModel,
                                        where: {
                                            endpoint: path,
                                            method: method.toUpperCase(),
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: scope,
                        required: true,
                        include: [
                            {
                                model: modelManager.routeModel,
                                where: {
                                    endpoint: path,
                                    method: method.toUpperCase(),
                                },
                            },
                        ],
                        on: {
                            [Op.or]: [
                                { role_id: { [Op.eq]: sequelize.col("role.id") } },
                                { role_id: { [Op.eq]: sequelize.col("role.parent_id") } },
                            ],
                        },
                    },
                ],
            })
            .then((scopes) => {
                if (scopes.length === 0) {
                    throw new GeneralError(errorMessages.not_found, "scope", undefined, 404);
                }

                const limitations = scopes.map((role) => {
                    const limitations = getUniqueBy(
                        role.scopes
                            .filter((item) => (role.parent ? role.parent.id !== item.role_id : true))
                            .map((item) => item.limitations),
                        "name"
                    ).flat();
                    let parentLimitations = [];
                    if (role.parent) {
                        parentLimitations = getUniqueBy(
                            role.parent.scopes.map((item) => item.limitations),
                            "name"
                        ).flat();
                    }

                    return unionBy(limitations, parentLimitations, (item) => item.name);
                });

                const rateLimitRole = scopes.map((role) => {
                    return role.rate_limit
                        ? role.rate_limit.get({ plain: true })
                        : role.parent?.rate_limit
                        ? role.parent.rate_limit.get({ plain: true })
                        : null;
                });

                return { limitations, rateLimitRole };
            })
            .then(({ limitations, rateLimitRole }: { limitations: any[]; rateLimitRole: any[] }) => ({
                limitations: getUniqueBy(limitations, "name").flat(),
                rateLimitRole: getMaxRateLimit(rateLimitRole),
            }));

    /**
     *
     * @param {string} method
     * @param {string} path
     * @returns {Promise<Scope[]>}
     */
    scope.findAllPublicScopesForRoute = (method: string, path: string) =>
        modelManager.scopeModel
            .findAll({
                include: [
                    {
                        model: modelManager.routeModel,
                        where: {
                            endpoint: path,
                            method: method.toUpperCase(),
                            public: true,
                        },
                    },
                ],
            })
            .then((scopes) => scopes.map((resScope) => resScope.limitations))
            .then((limitations: any[]) => getUniqueBy(limitations, "name").flat());

    return scope;
};

export { scopeModel };
