"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import * as url from "url";
import { accessPolicyMiddleware, checkErrors, checkId, checkListOptions, checkOrdering } from "../../core/middlewares";
import { createForm, patchForm, ScopeController } from "./";

class ScopeRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: ScopeController;

    constructor() {
        this.baseUrl = "/roles/:roleId/scopes";
        this.controller = new ScopeController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId("roleId"),
            checkListOptions,
            checkOrdering(["route.endpoint", "updated_at"]),
            checkErrors,
            this.controller.list
        );

        this.router.get(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId(["roleId", "id"]),
            checkErrors,
            this.controller.detail
        );

        this.router.get(
            "/limitations",
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            this.controller.limitationList
        );

        this.router.post(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            createForm,
            checkId("roleId"),
            checkErrors,
            this.controller.create
        );

        this.router.patch(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            patchForm,
            checkId(["roleId", "id"]),
            checkErrors,
            this.controller.patch
        );

        this.router.delete(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId(["roleId", "id"]),
            checkErrors,
            this.controller.remove
        );

        // Routes for backwards compatibility of the API
        this.router.get("/role/:roleId/scope", (req, res) => {
            res.redirect(
                307,
                url.format({
                    pathname: `/gateway/roles/${req.params.roleId}/scopes`,
                    query: req.query as any,
                })
            );
        });
        this.router.get("/role/:roleId/scope/:scopeId", (req, res) => {
            res.redirect(307, `/gateway/roles/${req.params.roleId}/scopes/${req.params.scopeId}`);
        });
        this.router.post("/role/:roleId/scope", (req, res) => {
            res.redirect(307, `/gateway/roles/${req.params.roleId}/scopes`);
        });
        this.router.delete("/role/:roleId/scope/:scopeId", (req, res) => {
            res.redirect(307, `/gateway/roles/${req.params.roleId}/scopes/${req.params.scopeId}`);
        });
    };
}

const scopeRouter: Router = new ScopeRouter().router;

export { scopeRouter };
