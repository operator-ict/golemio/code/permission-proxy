"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { v4 as uuidv4 } from "uuid";
import { config } from "../../core/config";
import { BaseController } from "../../core/controllers";
import { errorMessages, eventEmitter } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class UserController extends BaseController {
    // TODO: method too long. Make it prettier, separate & divide
    public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const email = (req.body.email as string).toLowerCase();
            const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";

            const referer = isAdminRoute
                ? await modelManager.refererWhitelistModel.findByPk(req.body.referer)
                : await modelManager.refererWhitelistModel.findBySubstring(req.get("referer"));
            const roles = isAdminRoute
                ? // for admin panel
                  req.body.roleIds
                    ? await modelManager.roleModel.findAll({
                          attributes: ["id"],
                          raw: true,
                          where: {
                              id: {
                                  [Op.in]: req.body.roleIds,
                              },
                          },
                      })
                    : []
                : // for registration - default roles
                  await modelManager.roleModel.findAll({
                      attributes: ["id"],
                      raw: true,
                      where: {
                          default: true,
                      },
                  });
            let user = await modelManager.userModel.scope().findOne({ where: { email } });
            if (user && user.invitation_accepted_at !== null) {
                throw new GeneralError(errorMessages.email_exists, this.constructor.name, undefined, 409);
            }

            if (user) {
                await user.update({
                    initial_referer: referer ? referer.id : user.initial_referer,
                    password: req.body.password ? modelManager.userModel.encryptPassword(req.body.password) : null,
                });
                await modelManager.invitationModel.refreshInvitation(user.id);
                user = await modelManager.userModel.findByPk(user.id, { include: [modelManager.invitationModel] });
            } else {
                user = await modelManager.userModel.create(
                    {
                        ...req.body,
                        email: (req.body.email as string).toLowerCase(),
                        initial_referer: referer ? referer.id : null,
                        invitation: {
                            code: uuidv4(),
                            expiration: modelManager.invitationModel.expiration,
                        },
                        password: req.body.password ? modelManager.userModel.encryptPassword(req.body.password) : null,
                        rate_limit: req.body.rateLimit
                            ? {
                                  limit: req.body.rateLimit.limit,
                                  timewindow: req.body.rateLimit.timewindow,
                              }
                            : null,
                        user_roles: roles.map((role) => ({ role_id: role.id })),
                    },
                    {
                        include: [
                            { model: modelManager.invitationModel },
                            { model: modelManager.userRoleModel },
                            ...(isAdminRoute ? [{ model: modelManager.rateLimitModel }] : []),
                        ],
                    }
                );
            }

            const activationLink = await modelManager.refererWhitelistModel.createLink(
                isAdminRoute ? referer.referer : req.get("referer"),
                `/auth/account-confirmation/${user.invitation.code}`
            );
            if (!activationLink) {
                throw new GeneralError(errorMessages.forbidden, this.constructor.name, undefined, 403);
            }
            eventEmitter.emit("mail:send", {
                locals: { user, activationLink, tokenExpiration: user.invitation.expiration.toLocaleString("cs-CZ") },
                recipients: user.email,
                templateName: "userInvite",
            });

            if (config.node_env !== "production") {
                res.status(201).json({ code: user.invitation.code, activationLink });
            } else {
                res.sendStatus(201);
            }
        } catch (err) {
            next(err);
        }
    };

    public list = async (req: Request<{}, any, any, any>, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { limit, offset, page, search, orderBy, orderDir } = req.query;
            const searchLike = search ? `%${search}%` : null;

            const result = await modelManager.userModel.findAndCountAll({
                attributes: ["id", "name", "surname", "email", "created_at", "updated_at", "invitation_accepted_at"],
                distinct: true,
                include: [
                    {
                        attributes: ["role_id"],
                        model: modelManager.userRoleModel,
                        required: false,
                    },
                ],
                limit,
                offset,
                order: orderBy ? [[orderBy, orderDir]] : [["created_at", "DESC"]],
                where: searchLike && {
                    [Op.or]: [
                        { name: { [Op.iLike]: searchLike } },
                        { surname: { [Op.iLike]: searchLike } },
                        { email: { [Op.iLike]: searchLike } },
                    ],
                },
            });
            this.setPaginationHeaders(res, result.count, limit, page).json(
                result.rows.map((x: any) => this.mapUser(x, true, true))
            );
        } catch (err) {
            next(err);
        }
    };

    public detail = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const userId = req.params.id;
            const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";
            let attributes = ["id", "name", "surname", "email"];
            let include = [];
            if (isAdminRoute) {
                attributes = [...attributes, "initial_referer", "created_at", "updated_at", "invitation_accepted_at"];
                include = [
                    {
                        attributes: ["role_id"],
                        model: modelManager.userRoleModel,
                        required: false,
                    },
                    {
                        attributes: ["limit", "timewindow"],
                        model: modelManager.rateLimitModel,
                        required: false,
                    },
                ];
            }

            const user = await modelManager.userModel.findByPk(userId, {
                attributes,
                include,
            });
            if (!user) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            res.json(this.mapUser(user, isAdminRoute, false));
        } catch (err) {
            next(err);
        }
    };

    public remove = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const userId = req.params.id;
            const user = await modelManager.userModel.findByPk(userId);
            if (!user) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await user.destroy();

            // delete and revoke user's apikeys
            const apiKeys = await modelManager.apiKeyModel.findAll({
                where: {
                    user_id: userId,
                },
            });
            for (const apiKey of apiKeys) {
                await apiKey.destroy();
                eventEmitter.emit("api_key:revoke", { token: apiKey.code });
            }
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    public patch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const userId = req.params.id;
            const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";

            let includes = [];
            if (isAdminRoute) {
                includes = [
                    {
                        attributes: ["role_id"],
                        model: modelManager.userRoleModel,
                        required: false,
                    },
                    {
                        attributes: ["limit", "timewindow"],
                        model: modelManager.rateLimitModel,
                        required: false,
                    },
                ];
            }
            let user = await modelManager.userModel.findByPk(userId, {
                include: includes,
            });
            if (!user) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            const u: any = {
                name: req.body.name,
                surname: req.body.surname,
            };

            user = await user.update(u);

            if (isAdminRoute) {
                const roleIds = user.user_roles ? user.user_roles.map((r: any) => r.role_id) : [];
                const roleIdsToAdd: number[] = req.body.roleIds.filter((x) => roleIds.indexOf(x) === -1);
                const roleIdsToRemove: number[] = roleIds.filter((x) => req.body.roleIds.indexOf(x) === -1);

                await Promise.all(
                    roleIdsToAdd.map((id: number) => {
                        modelManager.userRoleModel.create({ user_id: +userId, role_id: id });
                    })
                );

                if (roleIdsToRemove.length > 0) {
                    await modelManager.userRoleModel.destroy({
                        where: {
                            [Op.and]: [{ user_id: userId }, { role_id: { [Op.in]: roleIdsToRemove } }],
                        },
                    });
                }

                if (req.body.rateLimit && req.body.rateLimit.limit != null && req.body.rateLimit.timewindow != null) {
                    await modelManager.rateLimitModel.upsert({
                        limit: req.body.rateLimit.limit,
                        timewindow: req.body.rateLimit.timewindow,
                        user_id: +userId,
                    });
                } else {
                    await modelManager.rateLimitModel.destroy({
                        where: { user_id: userId },
                    });
                }
            }
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    // TODO: Documentation! what does this do
    // TODO: var named "x" - pls no. x:any - oh god
    private mapUser = (x: any, isAdmin: boolean, forList: boolean): any => {
        // if !isAdmin, nemel by videt roleIds, rateLimit, initial_referer?
        return {
            created_at: x.created_at,
            email: x.email,
            id: x.id,
            initial_referer: isAdmin ? (forList ? undefined : x.initial_referer) : undefined,
            invitation_accepted_at: x.invitation_accepted_at,
            name: x.name,
            rateLimit: isAdmin
                ? x.rate_limit
                    ? {
                          limit: x.rate_limit.limit,
                          timewindow: x.rate_limit.timewindow,
                      }
                    : forList
                    ? undefined
                    : null
                : undefined,
            roleIds: isAdmin ? (x.user_roles ? x.user_roles.map((r: any) => r.role_id) : []) : undefined,
            surname: x.surname,
            updated_at: x.updated_at,
        };
    };
}
