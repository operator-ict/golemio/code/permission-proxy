"use strict";

import { body, ValidationChain } from "express-validator";
import { Request } from "express-validator/src/base";
import { errorMessages } from "../../core/helpers";

const isAdminRoute = (req: Request): boolean => {
    return req.query.isAdmin === "true" || req.query.isAdmin === "1";
};

const hasRateLimit = (req: Request): boolean => {
    return req.body.rateLimit != null;
};

const isIdAdminOnly = (field: string): ValidationChain => {
    const result = body(field)
        .custom((value, { req }) => {
            if (isAdminRoute(req) && value === undefined) {
                return false;
            }
            return true;
        })
        .withMessage(errorMessages.required)
        .custom((value, { req }) => {
            if (isAdminRoute(req)) {
                const nValue = parseInt(value, 10);
                return !isNaN(nValue) && nValue > 0;
            }
            return true;
        })
        .withMessage(errorMessages.not_an_id);

    return result;
};

const isValidRateLimitFieldAdminOnly = (field: string): ValidationChain => {
    const result = body(field)
        .optional()
        .custom((value, { req }) => {
            if (isAdminRoute(req) && hasRateLimit(req) && value === undefined) {
                return false;
            }
            return true;
        })
        .withMessage(errorMessages.required)
        .custom((value, { req }) => {
            if (isAdminRoute(req) && hasRateLimit(req)) {
                const nValue = parseInt(value, 10);
                return !isNaN(nValue);
            }
            return true;
        })
        .withMessage(errorMessages.not_an_id)
        .custom((value, { req }) => {
            if (isAdminRoute(req) && hasRateLimit(req)) {
                const nValue = parseInt(value, 10);
                return !isNaN(nValue) && nValue >= 0;
            }
            return true;
        })
        .withMessage(errorMessages.value_not_allowed);

    return result;
};

const patchForm = [
    body("name").optional(),
    body("surname").optional(),

    // for admin panel these should be validated:
    body("roleIds")
        .custom((value, { req }) => {
            if (isAdminRoute(req)) {
                if (!value || !Array.isArray(value)) {
                    return false;
                }
            }
            return true;
        })
        .withMessage(errorMessages.required),
    body("roleIds.*").optional().isInt({ min: 0 }).withMessage(errorMessages.not_an_id),
    body("rateLimit").optional(),
    isValidRateLimitFieldAdminOnly("rateLimit.limit"),
    isValidRateLimitFieldAdminOnly("rateLimit.timewindow"),
];

const createForm = [
    // For admin user the referer is required
    isIdAdminOnly("referer"),

    body("email").exists().withMessage(errorMessages.required).isEmail().withMessage(errorMessages.not_an_email),

    // for free registration the password is required.
    body("password")
        .custom((value, { req }) => {
            if (isAdminRoute(req)) {
                return true;
            }
            if (!value) {
                return false;
            }

            return true;
        })
        .withMessage(errorMessages.required),

    ...patchForm,
];

export { createForm, patchForm };
