"use strict";

import { compareSync, genSaltSync, hashSync } from "bcryptjs";
import {
    BOOLEAN,
    STRING,
    INTEGER,
    DATE,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface UserSequelizeModel extends Model<InferAttributes<UserSequelizeModel>, InferCreationAttributes<UserSequelizeModel>> {
    [x: string]: any;
    id: CreationOptional<number>;
    email: string;
    password: CreationOptional<string>;
    admin: boolean;
    initial_referer: CreationOptional<number>;
    invitation_accepted_at: CreationOptional<number>;
    name: CreationOptional<string>;
    surname: CreationOptional<string>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
    deleted_at: CreationOptional<number>;
}

export interface UserModel extends ModelCtor<UserSequelizeModel> {
    findByCredentials?: (email: string, password: string) => Promise<UserSequelizeModel>;
    validate?: (hash: string, password: string) => any;
    encryptPassword?: (password: string) => any;
    associate?: (models: any) => void;
}

const userModel = (sequelize: Sequelize) => {
    const user: UserModel = sequelize.define(
        "user",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            email: {
                type: STRING,
                allowNull: false,
                unique: true,
            },
            password: {
                type: STRING,
            },
            admin: {
                type: BOOLEAN,
                defaultValue: false,
                allowNull: false,
            },
            initial_referer: {
                type: INTEGER,
            },
            invitation_accepted_at: {
                type: DATE,
            },
            name: {
                type: STRING,
            },
            surname: {
                type: STRING,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
            deleted_at: {
                type: DATE,
            },
        },
        {
            defaultScope: {
                attributes: {
                    exclude: ["password"],
                },
            },
            indexes: [
                {
                    fields: ["email"],
                    unique: true,
                },
            ],
            paranoid: true,
        }
    );

    /**
     * Encrypt user password
     * @param {string} password
     * @returns {string}
     */
    user.encryptPassword = (password: string) => {
        const salt = genSaltSync(10);
        return hashSync(password, salt);
    };

    /**
     * Validate user password
     * @param {string} hash
     * @param {string} password
     * @returns {boolean}
     */
    user.validate = (hash: string, password: string) => {
        return compareSync(password, hash);
    };

    /**
     *
     * @param {string} email
     * @param {string} password
     * @returns {Bluebird<User>}
     */
    user.findByCredentials = (email: string, password: string) =>
        user
            .scope()
            .findOne({
                raw: true,
                where: {
                    email,
                },
            })
            .then((resUser) => {
                if (resUser === null || !user.validate(resUser.password, password)) {
                    return null;
                }
                delete resUser.password;
                return resUser;
            });

    user.associate = (models) => {
        user.hasMany(models.api_key);
        user.hasMany(models.user_role);
        user.hasOne(models.invitation);
        user.hasOne(models.rate_limit);
        user.belongsTo(models.referer_whitelist, { foreignKey: "initial_referer" });
    };

    return user;
};

export { userModel };
