"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Response, Router } from "@golemio/core/dist/shared/express";
import * as url from "url";
import { errorMessages } from "../../core/helpers";
import {
    accessPolicyMiddleware,
    checkErrors,
    checkId,
    checkListOptions,
    checkOrdering,
    ExtendedRequest,
} from "../../core/middlewares";
import { createForm, patchForm, UserController } from "./";

class UserRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: UserController;

    constructor() {
        this.baseUrl = "/users";
        this.controller = new UserController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkListOptions,
            checkOrdering(["name", "surname", "email", "created_at", "updated_at", "invitation_accepted_at"]),
            this.controller.list
        );

        this.router.get(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            checkId("id"),
            accessPolicyMiddleware.isAdminOrHimself("id"),
            (req: ExtendedRequest, res: Response, next: NextFunction) => {
                const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";
                if (isAdminRoute) {
                    if (!req.session.user.admin) {
                        throw new GeneralError(errorMessages.forbidden, this.constructor.name, undefined, 403);
                    }
                    next();
                } else {
                    next();
                }
            },
            checkErrors,
            this.controller.detail
        );

        this.router.post(
            `${this.baseUrl}`,
            (req: ExtendedRequest, res: Response, next: NextFunction) => {
                const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";
                if (isAdminRoute) {
                    accessPolicyMiddleware.isSignedIn(req, res, (err) => {
                        if (err) {
                            return next(err);
                        }
                        next();
                    });
                } else {
                    next();
                }
            },
            (req: ExtendedRequest, res: Response, next: NextFunction) => {
                const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";
                if (isAdminRoute) {
                    if (!req.session.user.admin) {
                        throw new GeneralError(errorMessages.forbidden, this.constructor.name, undefined, 403);
                    }
                    next();
                } else {
                    next();
                }
            },
            createForm,
            checkErrors,
            this.controller.create
        );

        this.router.patch(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            patchForm,
            checkId("id"),
            accessPolicyMiddleware.isAdminOrHimself("id"),
            (req: ExtendedRequest, res: Response, next: NextFunction) => {
                const isAdminRoute = req.query.isAdmin === "true" || req.query.isAdmin === "1";
                if (isAdminRoute) {
                    if (!req.session.user.admin) {
                        throw new GeneralError(errorMessages.forbidden, this.constructor.name, undefined, 403);
                    }
                    next();
                } else {
                    next();
                }
            },
            checkErrors,
            this.controller.patch
        );

        this.router.delete(
            `${this.baseUrl}/:id`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId("id"),
            checkErrors,
            this.controller.remove
        );

        // Routes for backwards compatibility of the API

        this.router.post("/user", (req, res) => {
            res.redirect(
                307,
                url.format({
                    pathname: `/gateway${this.baseUrl}`,
                    query: req.query as any,
                })
            );
        });
    };
}

const userRouter: Router = new UserRouter().router;

export { userRouter };
