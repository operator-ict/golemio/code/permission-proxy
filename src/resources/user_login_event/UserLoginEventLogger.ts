"use strict";

import { modelManager } from "../../core/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export interface IUserLoginEvent {
    user_id: number;
    login_referer: string;
}

class UserLoginEventLogger {
    public logEvent = async (event: IUserLoginEvent) => {
        try {
            await modelManager.userLoginEventModel.create(event);
        } catch (error) {
            throw new GeneralError("Error while loggin User login event.", this.constructor.name);
        }
    };
}

const userLoginEventLogger = new UserLoginEventLogger();

export { userLoginEventLogger };
