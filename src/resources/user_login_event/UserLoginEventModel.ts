"use strict";

import {
    Sequelize,
    INTEGER,
    STRING,
    DATE,
    InferAttributes,
    Model,
    InferCreationAttributes,
    CreationOptional,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface UserLoginEventsSequelizeModel
    extends Model<InferAttributes<UserLoginEventsSequelizeModel>, InferCreationAttributes<UserLoginEventsSequelizeModel>> {
    user_id: number;
    login_referer: CreationOptional<string>;
    created_at: CreationOptional<number>;
}

export interface UserLoginEventModel extends ModelCtor<UserLoginEventsSequelizeModel> {}

const userLoginEventModel = (sequelize: Sequelize) => {
    const userLoginEvent: UserLoginEventModel = sequelize.define<UserLoginEventsSequelizeModel>(
        "user_login_event",
        {
            user_id: {
                type: INTEGER,
                allowNull: false,
            },
            login_referer: {
                type: STRING,
            },
            created_at: {
                type: DATE,
            },
        },
        {
            indexes: [
                {
                    fields: ["user_id", "created_at"],
                    type: "UNIQUE",
                    name: "user_login_event_pkey",
                },
            ],
            deletedAt: false,
            updatedAt: false,
            timestamps: true,
        }
    );

    userLoginEvent.removeAttribute("id");

    return userLoginEvent;
};

export { userLoginEventModel };
