"use strict";

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { BaseController } from "../../core/controllers";
import { errorMessages } from "../../core/helpers";
import { modelManager } from "../../core/models";

export class UserRoleController extends BaseController {
    public list = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { userId } = req.params;
            const roles = await modelManager.roleModel.findAll({
                // TODO if user not found, return 404 (now 200 and empty array)
                include: [
                    {
                        attributes: [],
                        model: modelManager.userRoleModel,
                        where: {
                            user_id: userId,
                        },
                    },
                ],
            });
            res.json(roles);
        } catch (err) {
            next(err);
        }
    };

    public upsert = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const userId = req.params.userId;
            const roleId = req.body.role_id;
            await modelManager.userRoleModel.upsert({
                role_id: +roleId,
                user_id: +userId,
            });
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    public upsertPutDeprecated = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { roleId, userId } = req.params;
            await modelManager.userRoleModel.upsert({
                role_id: +roleId,
                user_id: +userId,
            });
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    public remove = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const { roleId, userId } = req.params;
            const userRole = await modelManager.userRoleModel.findOne({
                where: {
                    role_id: roleId,
                    user_id: userId,
                },
            });
            if (!userRole) {
                throw new GeneralError(errorMessages.not_found, this.constructor.name, undefined, 404);
            }
            await userRole.destroy();
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
