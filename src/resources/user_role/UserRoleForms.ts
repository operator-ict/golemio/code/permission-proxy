"use strict";

import { body } from "express-validator";
import { errorMessages } from "../../core/helpers";

const upsertForm = [body("role_id").exists().withMessage(errorMessages.required).isInt().withMessage(errorMessages.not_an_id)];

export { upsertForm };
