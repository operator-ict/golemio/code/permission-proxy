"use strict";

import {
    INTEGER,
    DATE,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface UserRoleSequelizeModel
    extends Model<InferAttributes<UserRoleSequelizeModel>, InferCreationAttributes<UserRoleSequelizeModel>> {
    id: CreationOptional<number>;
    user_id: CreationOptional<number>;
    role_id: CreationOptional<number>;
    created_by_user_id: CreationOptional<number>;
    updated_by_user_id: CreationOptional<number>;
    created_at: CreationOptional<number>;
    updated_at: CreationOptional<number>;
}

export interface UserRoleModel extends ModelCtor<UserRoleSequelizeModel> {
    associate?: (models: any) => void;
}

const userRoleModel = (sequelize: Sequelize) => {
    const userRole: UserRoleModel = sequelize.define(
        "user_role",
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            user_id: {
                type: INTEGER,
            },
            role_id: {
                type: INTEGER,
            },
            created_by_user_id: {
                type: INTEGER,
            },
            updated_by_user_id: {
                type: INTEGER,
            },
            created_at: {
                type: DATE,
            },
            updated_at: {
                type: DATE,
            },
        },
        {
            deletedAt: false,
            indexes: [
                {
                    fields: ["user_id", "role_id"],
                    unique: true,
                },
            ],
            paranoid: false,
            timestamps: true,
        }
    );

    userRole.associate = (models) => {
        userRole.belongsTo(models.user);
        userRole.belongsTo(models.role);
    };

    return userRole;
};

export { userRoleModel };
