"use strict";

import { Router } from "@golemio/core/dist/shared/express";
import { accessPolicyMiddleware, checkErrors, checkId } from "../../core/middlewares";
import { upsertForm, UserRoleController } from "./";

class UserRoleRouter {
    public router: Router = Router();

    protected baseUrl: string;

    protected controller: UserRoleController;

    constructor() {
        this.baseUrl = "/users/:userId/roles";
        this.controller = new UserRoleController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId(["userId"]),
            checkErrors,
            this.controller.list
        );

        this.router.post(
            this.baseUrl,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            upsertForm,
            checkId("userId"),
            checkErrors,
            this.controller.upsert
        );

        this.router.put(
            // TODO: DEPRECATED - should be removed?
            `${this.baseUrl}/:roleId`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId(["userId", "roleId"]),
            checkErrors,
            this.controller.upsertPutDeprecated
        );

        this.router.delete(
            `${this.baseUrl}/:roleId`,
            accessPolicyMiddleware.isSignedIn,
            accessPolicyMiddleware.isAdmin,
            checkId(["userId", "roleId"]),
            checkErrors,
            this.controller.remove
        );

        // Routes for backwards compatibility of the API
        this.router.get("/user/:userId/role", (req, res) => {
            res.redirect(307, `/gateway/users/${req.params.userId}/roles`);
        });
        this.router.put("/user/:userId/role/:roleId", (req, res) => {
            res.redirect(307, `/gateway/users/${req.params.userId}/roles/${req.params.roleId}`);
        });
        this.router.delete("/user/:userId/role/:roleId", (req, res) => {
            res.redirect(307, `/gateway/users/${req.params.userId}/roles/${req.params.roleId}`);
        });
    };
}

const userRoleRouter: Router = new UserRoleRouter().router;

export { userRoleRouter };
