"use strict";

import {
    STRING,
    INTEGER,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface VAllUserRoleRouteDistinctSequelizeModel
    extends Model<
        InferAttributes<VAllUserRoleRouteDistinctSequelizeModel>,
        InferCreationAttributes<VAllUserRoleRouteDistinctSequelizeModel>
    > {
    endpoint: string;
    method: string;
    user_id: number;
    route_id: CreationOptional<number>;
}
export interface VAllUserRoleRouteDistinctModel extends ModelCtor<VAllUserRoleRouteDistinctSequelizeModel> {}

const vAllUserRoleRouteDistinctModel = (sequelize: Sequelize) => {
    const route: VAllUserRoleRouteDistinctModel = sequelize.define(
        "v_all_user_role_route_distinct",
        {
            endpoint: {
                primaryKey: true,
                type: STRING,
            },
            method: {
                primaryKey: true,
                type: STRING,
            },
            user_id: {
                primaryKey: true,
                type: INTEGER,
            },
            route_id: {
                type: INTEGER,
            },
        },
        {
            timestamps: false,
        }
    );

    return route;
};

export { vAllUserRoleRouteDistinctModel };
