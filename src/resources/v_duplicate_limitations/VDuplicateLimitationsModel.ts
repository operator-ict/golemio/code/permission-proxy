"use strict";

import {
    INTEGER,
    STRING,
    JSONB,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface VDuplicateLimitationsSequelizeModel
    extends Model<
        InferAttributes<VDuplicateLimitationsSequelizeModel>,
        InferCreationAttributes<VDuplicateLimitationsSequelizeModel>
    > {
    duplicates: CreationOptional<number>;
    email: string;
    endpoint: string;
    limit_type: string;
    method: string;
    role_ids: CreationOptional<string>;
    target_url: string;
}

export interface VDuplicateLimitationsModel extends ModelCtor<VDuplicateLimitationsSequelizeModel> {}

const vDuplicateLimitationsModel = (sequelize: Sequelize) => {
    const duplicateLimitations: VDuplicateLimitationsModel = sequelize.define(
        "v_duplicate_limitations",
        {
            duplicates: { type: INTEGER },
            email: { type: STRING, primaryKey: true },
            endpoint: { type: STRING, primaryKey: true },
            limit_type: { type: JSONB, primaryKey: true },
            method: { type: STRING, primaryKey: true },
            role_ids: { type: JSONB },
            target_url: { type: STRING, primaryKey: true },
        },
        {
            timestamps: false,
        }
    );

    return duplicateLimitations;
};

export { vDuplicateLimitationsModel };
