"use strict";

import {
    STRING,
    INTEGER,
    JSONB,
    Model,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    Sequelize,
    ModelCtor,
} from "@golemio/core/dist/shared/sequelize";

interface VUserRoleRouteSequelizeModel
    extends Model<InferAttributes<VUserRoleRouteSequelizeModel>, InferCreationAttributes<VUserRoleRouteSequelizeModel>> {
    endpoint: string;
    method: string;
    user_id: number;
    email: CreationOptional<string>;
    label: CreationOptional<string>;
    target_url: CreationOptional<string>;
    limitations: CreationOptional<string>;
    role_id: CreationOptional<number>;
    route_id: CreationOptional<number>;
    role_parent_id: CreationOptional<number>;
}

export interface VUserRoleRouteModel extends ModelCtor<VUserRoleRouteSequelizeModel> {}

const vUserRoleRouteModel = (sequelize: Sequelize) => {
    const route: VUserRoleRouteModel = sequelize.define(
        "v_user_role_route",
        {
            endpoint: {
                primaryKey: true,
                type: STRING,
            },
            method: {
                primaryKey: true,
                type: STRING,
            },
            user_id: {
                primaryKey: true,
                type: INTEGER,
            },
            email: { type: STRING },
            label: { type: STRING },
            target_url: { type: STRING },
            limitations: { type: JSONB },
            role_id: { type: INTEGER },
            route_id: { type: INTEGER },
            role_parent_id: { type: INTEGER },
        },
        {
            timestamps: false,
        }
    );

    return route;
};

export { vUserRoleRouteModel };
