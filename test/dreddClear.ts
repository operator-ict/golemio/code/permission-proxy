import { dropTestSchema } from "./index";

dropTestSchema()
    .then(() => process.exit())
    .catch(() => process.exit());
