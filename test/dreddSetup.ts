import { initDatabase } from "./index";

initDatabase()
    .then(() => process.exit())
    .catch(() => process.exit());
