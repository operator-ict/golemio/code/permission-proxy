const hooks = require("hooks");

let userToken = null;
let adminToken = null;
const store = {};

hooks.beforeAll((transactions) => {
    console.log(transactions);
});

hooks.after("Authentication > Login > Login > Example 2", (transaction) => {
    userToken = JSON.parse(transaction.real.body).token;
});

hooks.after("Authentication > Login > Login > Example 3", (transaction) => {
    adminToken = JSON.parse(transaction.real.body).token;
});

hooks.beforeEach((transaction) => {
    if ((transaction.origin.resourceGroupName === "Users" && ![
        "Create New User (registration)",
        "Get User (self)",
        "Edit User (self)"
    ].includes(transaction.origin.actionName)) ||
        transaction.origin.resourceGroupName === "Rate Limits" ||
        transaction.origin.resourceGroupName === "Referer Whitelist" ||
        transaction.origin.resourceGroupName === "Roles" ||
        transaction.origin.resourceGroupName === "Routes" ||
        transaction.origin.resourceGroupName === "Scopes" ||
        transaction.origin.resourceGroupName === "User's Roles") {
        transaction.request.headers["x-access-token"] = adminToken;
    } else {
        transaction.request.headers["x-access-token"] = userToken;
    }
});

hooks.before("Users > Users > Get All Users", (transaction) => {
    transaction.skip = true;
});

hooks.after("Users > Users > Create New User (registration)", (transaction) => {
    store.invitationToken = JSON.parse(transaction.real.body).code;
});

hooks.after("Users > Users > Create New User (admin access)", (transaction) => {
    store.invitationToken = JSON.parse(transaction.real.body).code;
});

hooks.before("Users > Detail of the User > Get User (self)", (transaction) => {
    transaction.skip = true;
});

hooks.before("Users > Detail of the User > Get User (admin access)", (transaction) => {
    transaction.skip = true;
});

hooks.before("Users > Detail of the User > Edit User (self)", (transaction) => {
    transaction.skip = true;
});

hooks.before("Users > Detail of the User > Edit User (admin access)", (transaction) => {
    transaction.skip = true;
});

hooks.before("Users > Detail of the User > Remove User", (transaction) => {
    transaction.skip = true;
});

hooks.after("Api Keys > Api Keys > Create new Api Key for the User", (transaction) => {
    store.apiKeyId = JSON.parse(transaction.real.body).id;
});

hooks.before("Api Keys > Api Key Detail > Get existing Api Key of the User", (transaction) => {
    transaction.skip = true;
});

hooks.before("Api Keys > Api Key Detail > Remove existing Api Key of the User", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.apiKeyId);
    transaction.fullPath = transaction.fullPath.replace("1", store.apiKeyId);
});

hooks.after("Forgotten Password > Forgotten Password Request > Create New Password Reset Request > Example 1", (transaction) => {
    store.forgottenToken = JSON.parse(transaction.real.body).token;
});

hooks.before("Forgotten Password > Forgotten Password Request Confirmation > Confirm and Set New Password > Example 2", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.forgottenToken);
    transaction.fullPath = transaction.fullPath.replace("1", store.forgottenToken);
});

hooks.before("Invitations > Detail of the Invitation > Get Information about Invitation > Example 2", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.invitationToken);
    transaction.fullPath = transaction.fullPath.replace("1", store.invitationToken);
});

hooks.before("Invitations > Detail of the Invitation > Confirm the Invitation and Optionally Set New Password > Example 2", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.invitationToken);
    transaction.fullPath = transaction.fullPath.replace("1", store.invitationToken);
});

hooks.before("Rate Limits > User's Rate Limit > Get Rate Limit Information", (transaction) => {
    transaction.skip = true;
});

hooks.before("Rate Limits > User's Rate Limit > Create New Rate Limit", (transaction) => {
    transaction.skip = true;
});

hooks.after("Roles > User Roles > Create New User Role > Example 1", (transaction) => {
    store.roleId = JSON.parse(transaction.real.body).id;
});

hooks.after("Roles > User Roles > Create New User Role > Example 3", (transaction) => {
    store.roleId2 = JSON.parse(transaction.real.body).id;
});

hooks.before("Roles > Detail of User Role > Get User Role", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.roleId);
    transaction.fullPath = transaction.fullPath.replace("1", store.roleId);
});

hooks.before("Roles > Detail of User Role > Edit User Role", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.roleId);
    transaction.fullPath = transaction.fullPath.replace("1", store.roleId);
});

hooks.before("Roles > Detail of User Role > Remove User Role", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.roleId);
    transaction.fullPath = transaction.fullPath.replace("1", store.roleId);
});

hooks.after("Routes > Routes > Create New Route > Example 1", (transaction) => {
    store.routeId = JSON.parse(transaction.real.body).id;
});

hooks.after("Routes > Routes > Create New Route > Example 2", (transaction) => {
    store.routeId2 = JSON.parse(transaction.real.body).id;
});

hooks.before("Routes > Detail of Route > Get Route", (transaction) => {
    transaction.skip = true;
});

hooks.before("Routes > Detail of Route > Edit Route", (transaction) => {
    transaction.skip = true;
});

hooks.before("Routes > Detail of Route > Remove Route", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.routeId);
    transaction.fullPath = transaction.fullPath.replace("1", store.routeId);
});

hooks.before("Scopes > Scopes > Create New Scope", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("1", store.roleId2);
    transaction.fullPath = transaction.fullPath.replace("1", store.roleId2);

    const body = JSON.parse(transaction.request.body);
    body.route_id = store.routeId2;
    transaction.request.body = JSON.stringify(body);
});

hooks.after("Scopes > Scopes > Create New Scope", (transaction) => {
    store.scopeId = JSON.parse(transaction.real.body).id;
});

hooks.before("Scopes > Detail of the Scope > Get Scope Detail", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("/scopes/1", `/scopes/${store.scopeId}`).replace("/roles/1", `/roles/${store.roleId2}`);
    transaction.fullPath = transaction.fullPath.replace("/scopes/1", `/scopes/${store.scopeId}`).replace("/roles/1", `/roles/${store.roleId2}`);
});

hooks.before("Scopes > Detail of the Scope > Edit Scope", (transaction) => {
    transaction.skip = true;
});

hooks.before("Scopes > Detail of the Scope > Remove Scope", (transaction) => {
    transaction.request.uri = transaction.request.uri.replace("/scopes/1", `/scopes/${store.scopeId}`).replace("/roles/1", `/roles/${store.roleId2}`);
    transaction.fullPath = transaction.fullPath.replace("/scopes/1", `/scopes/${store.scopeId}`).replace("/roles/1", `/roles/${store.roleId2}`);
});

hooks.before("Scopes > Limitations > Get All Limitations", (transaction) => {
    transaction.skip = true;
});

hooks.before("User's Roles > User's Roles > Assign New Role to User", (transaction) => {
    transaction.skip = true;
});

hooks.before("User's Routes > User's Routes > Get Routes of User", (transaction) => {
    // user id = 2
    transaction.request.uri = transaction.request.uri.replace("/users/1/routes", `/users/3/routes`);
    transaction.fullPath = transaction.fullPath.replace("/users/1/routes", `/users/3/routes`);
});
