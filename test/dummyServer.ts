import * as express from "@golemio/core/dist/shared/express";
import * as compression from "compression";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "../src/core/helpers";

require("dotenv").config();

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/faulty", (_req, res) => {
    const exception = new GeneralError("Some error", "App", new Error("test"), 400);
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.status(400).send(exception.toObject());
});

app.get("/faulty-gzipped", compression(), (_req, res) => {
    const exception = new GeneralError("Some error", "App", new Error(`test `.repeat(200)), 400);
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.status(400).send(exception.toObject());
});

app.use("*", (req, res) => {
    const response = {
        body: req.body,
        method: req.method,
        query: req.query,
        url: req.baseUrl,
    };
    log.info("Dummy req got request", { meta: { response } });
    res.json(response);
});

const port = 3100; // test port

app.listen(port);

log.info("dummy server is listening", { meta: { port } });
