import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase } from "../index";

let testApp;

describe("Misc test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    it("Should return route not found", (done) => {
        testApp.get("/nonexistingroute").expect(404, done);
    });

    it("Should return status", (done) => {
        testApp.get("/gateway/status").expect(200, done);
    });
});
