import { expect } from "chai";
import { proxy } from "../../src/proxy";

describe("Limitations test", () => {
    it("Should process in array limitations", () => {
        expect(proxy.handleLimitationType({ type: "in-array", values: ["praha-1", "praha-2"] }, "praha-2")).to.eql(["praha-2"]);
        expect(() => {
            proxy.handleLimitationType({ type: "in-array", values: ["praha-1"] }, "praha-2");
        }).to.throw();
        expect(
            proxy.handleLimitationType(
                {
                    type: "in-array",
                    values: ["praha-2", "praha-3"],
                },
                ["praha-2", "praha-3"]
            )
        ).to.eql(["praha-2", "praha-3"]);
        expect(proxy.handleLimitationType({ type: "in-array", values: ["praha-3"] }, ["praha-2", "praha-3"])).to.eql(["praha-3"]);
        expect(
            proxy.handleLimitationType(
                {
                    source: "params",
                    type: "in-array",
                    values: ["ICE-003-042-000-008", "ICE-003-042-000-005"],
                },
                ["ICE-003-042-000-005"]
            )
        ).to.eql(["ICE-003-042-000-005"]);
        expect(() =>
            proxy.handleLimitationType(
                {
                    source: "params",
                    type: "in-array",
                    values: ["ICE-003-042-000-006"],
                },
                ["ICE-003-042-000-005"]
            )
        ).to.throw();
        expect(() => {
            proxy.handleLimitationType(
                {
                    type: "in-array",
                    values: ["praha-1", "praha-4"],
                },
                ["praha-2", "praha-3"]
            );
        }).to.throw();
    });

    it("Should process min / max limitations", () => {
        // type number
        expect(proxy.handleLimitationType({ type: "min-max", max: 20000 }, 10000)).to.eql(10000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 2000 }, 10000)).to.eql(10000);
        expect(proxy.handleLimitationType({ type: "min-max", max: 2000 }, 10000)).to.eql(2000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 20000 }, 10000)).to.eql(20000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 2000, max: 100000 }, 10000)).to.eql(10000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 2000, max: 5000 }, 10000)).to.eql(5000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 20000, max: 50000 }, 10000)).to.eql(20000);
        expect(proxy.handleLimitationType({ source: "params", type: "min-max", min: 2000, max: 100000 }, 10000)).to.eql(10000);
        expect(proxy.handleLimitationType({ source: "params", type: "min-max", min: 2000, max: 5000 }, 3000)).to.eql(3000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 0, max: 200, default: 10 })).to.eql(10);
        expect(proxy.handleLimitationType({ type: "min-max", min: 2000, max: 5000 })).to.eql(5000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 2000, max: 5000, default: 3000 }, 10000)).to.eql(5000);
        expect(proxy.handleLimitationType({ type: "min-max", min: 2000, max: 5000, default: 3000 }, 1000)).to.eql(2000);

        // type date
        expect(proxy.handleLimitationType({ type: "min-max", minMaxType: "date", max: "2022-03-01" }, "2022-02-01")).to.eql(
            "2022-02-01"
        );
        expect(proxy.handleLimitationType({ type: "min-max", minMaxType: "date", min: "2022-02-01" }, "2022-03-01")).to.eql(
            "2022-03-01"
        );
        expect(proxy.handleLimitationType({ type: "min-max", minMaxType: "date", max: "2022-02-01" }, "2022-03-01")).to.eql(
            "2022-02-01"
        );
        expect(proxy.handleLimitationType({ type: "min-max", minMaxType: "date", min: "2022-02-01" }, "2022-01-01")).to.eql(
            "2022-02-01"
        );
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date", min: "2022-02-01", max: "2022-03-01" },
                "2022-03-01"
            )
        ).to.eql("2022-03-01");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date", min: "2022-02-01", max: "2022-02-05" },
                "2022-03-01"
            )
        ).to.eql("2022-02-05");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date", min: "2022-02-01", max: "2022-03-01" },
                "2022-01-01"
            )
        ).to.eql("2022-02-01");
        expect(
            proxy.handleLimitationType(
                { source: "params", minMaxType: "date", type: "min-max", min: "2022-02-01", max: "2022-03-01" },
                "2022-03-01"
            )
        ).to.eql("2022-03-01");
        expect(
            proxy.handleLimitationType(
                { source: "params", minMaxType: "date", type: "min-max", min: "2022-02-01", max: "2022-03-01" },
                "2022-02-10"
            )
        ).to.eql("2022-02-10");
        expect(
            proxy.handleLimitationType({
                type: "min-max",
                minMaxType: "date",
                min: "2022-02-01",
                max: "2022-03-01",
                default: "2022-02-10",
            })
        ).to.eql("2022-02-10");
        expect(proxy.handleLimitationType({ type: "min-max", minMaxType: "date", min: "2022-02-01", max: "2022-03-01" })).to.eql(
            "2022-03-01"
        );
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date", min: "2022-02-01", max: "2022-03-01", default: "2022-02-10" },
                "2022-03-15"
            )
        ).to.eql("2022-03-01");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date", min: "2022-02-01", max: "2022-03-01", default: "2022-02-10" },
                "2022-01-01"
            )
        ).to.eql("2022-02-01");

        // type datetime
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", max: "2022-03-01T12:07:00.000Z" },
                "2022-02-01T12:07:00.000Z"
            )
        ).to.eql("2022-02-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", min: "2022-02-01T12:07:00.000Z" },
                "2022-03-01T12:07:00.000Z"
            )
        ).to.eql("2022-03-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", max: "2022-02-01T12:07:00.000Z" },
                "2022-03-01T12:07:00.000Z"
            )
        ).to.eql("2022-02-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", min: "2022-02-01T12:07:00.000Z" },
                "2022-01-01T12:07:00.000Z"
            )
        ).to.eql("2022-02-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", min: "2022-02-01T12:07:00.000Z", max: "2022-03-01T12:07:00.000Z" },
                "2022-03-01T12:07:00.000Z"
            )
        ).to.eql("2022-03-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", min: "2022-02-01T12:07:00.000Z", max: "2022-02-05T12:07:00.000Z" },
                "2022-03-01T12:07:00.000Z"
            )
        ).to.eql("2022-02-05T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                { type: "min-max", minMaxType: "date-time", min: "2022-02-01T12:07:00.000Z", max: "2022-03-01T12:07:00.000Z" },
                "2022-01-01T12:07:00.000Z"
            )
        ).to.eql("2022-02-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                {
                    source: "params",
                    minMaxType: "date-time",
                    type: "min-max",
                    min: "2022-02-01T12:07:00.000Z",
                    max: "2022-03-01T12:07:00.000Z",
                },
                "2022-03-01T12:07:00.000Z"
            )
        ).to.eql("2022-03-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                {
                    source: "params",
                    minMaxType: "date-time",
                    type: "min-max",
                    min: "2022-02-01T12:07:00.000Z",
                    max: "2022-03-01T12:07:00.000Z",
                },
                "2022-02-10T12:07:00.000Z"
            )
        ).to.eql("2022-02-10T12:07:00.000Z");
        expect(
            proxy.handleLimitationType({
                type: "min-max",
                minMaxType: "date-time",
                min: "2022-02-01T12:07:00.000Z",
                max: "2022-03-01T12:07:00.000Z",
                default: "2022-02-10T12:07:00.000Z",
            })
        ).to.eql("2022-02-10T12:07:00.000Z");
        expect(
            proxy.handleLimitationType({
                type: "min-max",
                minMaxType: "date-time",
                min: "2022-02-01T12:07:00.000Z",
                max: "2022-03-01T12:07:00.000Z",
            })
        ).to.eql("2022-03-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                {
                    type: "min-max",
                    minMaxType: "date-time",
                    min: "2022-02-01T12:07:00.000Z",
                    max: "2022-03-01T12:07:00.000Z",
                    default: "2022-02-10T12:07:00.000Z",
                },
                "2022-03-15T12:07:00.000Z"
            )
        ).to.eql("2022-03-01T12:07:00.000Z");
        expect(
            proxy.handleLimitationType(
                {
                    type: "min-max",
                    minMaxType: "date-time",
                    min: "2022-02-01T12:07:00.000Z",
                    max: "2022-03-01T12:07:00.000Z",
                    default: "2022-02-10T12:07:00.000Z",
                },
                "2022-01-01T12:07:00.000Z"
            )
        ).to.eql("2022-02-01T12:07:00.000Z");
    });
});
