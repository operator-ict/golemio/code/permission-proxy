import { expect } from "chai";
import * as jwt from "jsonwebtoken";
import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";
import { modelManager } from "../../src/core/models";

let testApp;
let users;

describe("User test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    it("Should try to log in unknown user", (done) => {
        testApp
            .post("/gateway/login")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    password: "Testing1234",
                })
            )
            .expect(401, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should try to log in without g-recaptcha-response", (done) => {
        testApp
            .post("/gateway/login")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    password: "Testing1234",
                })
            )
            .expect(400, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should try to log without password", (done) => {
        testApp
            .post("/gateway/login")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(400, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let invitationCode;
    it("Should create user", (done) => {
        testApp
            .post("/gateway/users")
            .set("content-type", "application/json")
            .set("Referer", "http://domain1.cz/api-keys/create-user-account")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    password: "Testing1234",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                expect(res.body).to.have.property(
                    "activationLink",
                    `http://domain1.cz/api-keys/auth/account-confirmation/${res.body.code}`
                );
                invitationCode = res.body.code;
                done();
            });
    });

    it("Get invitation information", (done) => {
        testApp
            .get(`/gateway/invitations/${invitationCode}`)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("already_accepted", false);
                expect(res.body).to.have.property("already_expired", false);
                expect(res.body).to.have.property("need_password", false);
                done();
            });
    });

    it("Should try to log in (invitation not accepted)", (done) => {
        testApp
            .post("/gateway/login")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    password: "Testing1234",
                })
            )
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                // expect(res.body).to.have.property("error", "err_invitation_not_accepted_yet");
                done();
            });
    });

    it("Confirm invitation", (done) => {
        testApp
            .put(`/gateway/invitations/${invitationCode}`)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Try to get invitation information (already accepted)", (done) => {
        testApp
            .get(`/gateway/invitations/${invitationCode}`)
            .set("content-type", "application/json")
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Cannot confirm (already accepted)", (done) => {
        testApp
            .put(`/gateway/invitations/${invitationCode}`)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Cannot create user (email exists)", (done) => {
        testApp
            .post("/gateway/users")
            .set("content-type", "application/json")
            .set("Referer", "http://domain1.cz/api-keys/create-user-account")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    password: "Testing1234",
                })
            )
            .expect(409, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should log in", (done) => {
        testApp
            .post("/gateway/login")
            .set("content-type", "application/json")
            .set("Referer", "http://domain1.cz/auth/sign-in")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    password: "Testing1234",
                })
            )
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("token");
                done();
            });
    });

    it("Should be saved User Login Event after user log in", (done) => {
        modelManager.userLoginEventModel
            .findAll({
                offset: 2,
                raw: true,
            })
            .then((savedLoginEvents) => {
                expect(savedLoginEvents).to.be.instanceOf(Array);
                expect(savedLoginEvents.length).to.equal(1);
                expect(savedLoginEvents[0]).to.have.property("user_id").to.equal(4);
                expect(savedLoginEvents[0]).to.have.property("login_referer").to.equal("http://domain1.cz/auth/sign-in");
                done();
            });
    });

    it("Should not log in common user when using isAdmin", (done) => {
        testApp
            .post("/gateway/login?isAdmin=true")
            .set("content-type", "application/json")
            .set("Referer", "http://domain2.cz/admin/login")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    password: "Testing1234",
                })
            )
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should log in admin when using isAdmin", (done) => {
        testApp
            .post("/gateway/login?isAdmin=true")
            .set("content-type", "application/json")
            .set("Referer", "http://domain2.cz/admin/login")
            .send(
                JSON.stringify({
                    email: "admin@admin.com",
                    "g-recaptcha-response": "123",
                    password: "admin",
                })
            )
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should be saved User Login Event when using isAdmin", (done) => {
        modelManager.userLoginEventModel
            .findAll({
                offset: 3,
                raw: true,
            })
            .then((savedLoginEvents) => {
                expect(savedLoginEvents).to.be.instanceOf(Array);
                expect(savedLoginEvents.length).to.equal(1);
                expect(savedLoginEvents[0]).to.have.property("user_id").to.equal(1);
                expect(savedLoginEvents[0]).to.have.property("login_referer").to.equal("http://domain2.cz/admin/login");
                done();
            });
    });

    it("Should fail for missing route_link parameter", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "fdslkfsdlkfldskf@gmail.com",
                })
            )
            .expect(400, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should send request for forgotten password (unknown user)", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "fdslkfsdlkfldskf@gmail.com",
                    "g-recaptcha-response": "123",
                    route_link: "/auth/password-reset/",
                })
            )
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should fail for forgotten password without g-recaptcha-response", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "fdslkfsdlkfldskf@gmail.com",
                })
            )
            .expect(400, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let forgottenPasswordToken;

    it("Should send request for forgotten password", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    route_link: "/auth/password-reset/",
                })
            )
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should send request for forgotten password", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("content-type", "application/json")
            .set("referer", "https://domain2/client-panel/resetuj-mi-heslo")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    route_link: "/auth/password-reset/",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("token");
                forgottenPasswordToken = res.body.token;
                done();
            });
    });

    it("Should regenerate password link", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("content-type", "application/json")
            .set("referer", "https://domain2/client-panel/resetuj-mi-heslo")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    route_link: "/auth/password-reset/",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("token").not.to.equal(forgottenPasswordToken);
                expect(res.body).to.have.property("resetLink");
                forgottenPasswordToken = res.body.token;
                done();
            });
    });

    it("Should regenerate password link again", (done) => {
        testApp
            .post("/gateway/forgotten-password")
            .set("referer", "https://domain2/client-panel/resetuj-mi-heslo")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "karelploc@gmail.com",
                    "g-recaptcha-response": "123",
                    route_link: "/auth/password-reset/",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("token").not.to.equal(forgottenPasswordToken);
                forgottenPasswordToken = res.body.token;
                done();
            });
    });

    it("Get info about the invalid password reset code", (done) => {
        testApp
            .get(`/gateway/forgotten-password/123`)
            .set("content-type", "application/json")
            .expect(400, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get info about the missing password reset code", (done) => {
        testApp
            .get(`/gateway/forgotten-password/d0bef6ce-0189-4337-a288-aabc642462a4`)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("valid").to.equal(false);
                expect(res.body).to.have.property("error").to.equal("err_not_found");
                done();
            });
    });

    it("Get info about the valid password reset code", (done) => {
        testApp
            .get(`/gateway/forgotten-password/${forgottenPasswordToken}`)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("valid").to.equal(true);
                expect(res.body).to.have.property("error").to.equal(null);
                done();
            });
    });

    it("Setup new password", (done) => {
        testApp
            .put(`/gateway/forgotten-password/${forgottenPasswordToken}`)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    password: "JineHeslo123",
                })
            )
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Setup new password (link already used - not found)", (done) => {
        testApp
            .put(`/gateway/forgotten-password/${forgottenPasswordToken}`)
            .set("content-type", "application/json")
            .set("referer", "https://domain2/client-panel/resetuj-mi-heslo")
            .send(
                JSON.stringify({
                    password: "JineHeslo123",
                })
            )
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Setup new password (unknown code)", (done) => {
        testApp
            .put(`/gateway/forgotten-password/6dd73ece-d4d2-4d8b-b098-2d4c962a3500`)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    password: "JineHeslo123",
                })
            )
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should create user with name and surname", (done) => {
        testApp
            .post("/gateway/users")
            .set("content-type", "application/json")
            .set("referer", "https://domain2/client-panel/vytvor-uzivatele")
            .send(
                JSON.stringify({
                    email: "karelploc+props@gmail.com",
                    name: "Karel",
                    password: "Testing1234",
                    surname: "Ploc",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                invitationCode = res.body.code;
                done();
            });
    });

    it("Confirm invitation", (done) => {
        testApp
            .put(`/gateway/invitations/${invitationCode}`)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let token;
    let userId;
    it("Should log in", (done) => {
        testApp
            .post("/gateway/login")
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    email: "karelploc+props@gmail.com",
                    "g-recaptcha-response": "123",
                    password: "Testing1234",
                })
            )
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("token");
                token = res.body.token;
                const user = jwt.decode(res.body.token);
                expect(user).to.have.property("id");
                userId = (user as { [key: string]: any }).id;
                expect(user).to.have.property("name", "Karel");
                expect(user).to.have.property("surname", "Ploc");
                done();
            });
    });

    it("List user roles - should include default role", (done) => {
        testApp
            .get(`/gateway/users/${userId}/roles`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(1);
                done();
            });
    });

    it("Should verify token - valid", (done) => {
        testApp
            .get("/gateway/verify")
            .set("content-type", "application/json")
            .set("x-access-token", token)
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should verify token - invalid", (done) => {
        testApp
            .get("/gateway/verify")
            .set("content-type", "application/json")
            .set("x-access-token", token.substr(0, token.length - 2))
            .expect(401, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    describe("Change password", () => {
        it("Should require auth token", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .expect(401, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should require valid auth token", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .set("x-access-token", token.substr(0, token.length - 2))
                .expect(401, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should require old password", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .set("x-access-token", token)
                .expect(400, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should require new password", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .set("x-access-token", token)
                .send(
                    JSON.stringify({
                        old_password: "xxx",
                    })
                )
                .expect(400, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should require new password of min. length 6", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .set("x-access-token", token)
                .send(
                    JSON.stringify({
                        new_password: "xxx",
                        old_password: "xxx",
                    })
                )
                .expect(400, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should require valid old password", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .set("x-access-token", token)
                .send(
                    JSON.stringify({
                        new_password: "NewTesting1234",
                        old_password: "xxx",
                    })
                )
                .expect(403, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should successfully change password", (done) => {
            testApp
                .post("/gateway/change-password")
                .set("content-type", "application/json")
                .set("x-access-token", token)
                .send(
                    JSON.stringify({
                        new_password: "NewTesting1234",
                        old_password: "Testing1234",
                    })
                )
                .expect(204, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });
    });

    describe("Create user as admin", () => {
        it("Should create user as admin", (done) => {
            testApp
                .post("/gateway/users?isAdmin=true")
                .set("content-type", "application/json")
                .set("x-access-token", users.admin)
                .send(
                    JSON.stringify({
                        email: "karelploc+admin@gmail.com",
                        referer: 1,
                        roleIds: [],
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("code");
                    invitationCode = res.body.code;
                    done();
                });
        });

        it("Confirm invitation - should require password", (done) => {
            testApp
                .put(`/gateway/invitations/${invitationCode}`)
                .set("content-type", "application/json")
                .send(JSON.stringify({}))
                .expect(400, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Confirm invitation", (done) => {
            testApp
                .put(`/gateway/invitations/${invitationCode}`)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        password: "Testing1234",
                    })
                )
                .expect(204, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Cannot create user as admin (email exists)", (done) => {
            testApp
                .post("/gateway/users?isAdmin=true")
                .set("content-type", "application/json")
                .set("x-access-token", users.admin)
                .send(
                    JSON.stringify({
                        email: "karelploc+admin@gmail.com",
                        referer: 1,
                        roleIds: [],
                    })
                )
                .expect(409, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should log in", (done) => {
            testApp
                .post("/gateway/login")
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        email: "karelploc+admin@gmail.com",
                        "g-recaptcha-response": "123",
                        password: "Testing1234",
                    })
                )
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("token");
                    token = res.body.token;
                    const user = jwt.decode(res.body.token);
                    expect(user).to.have.property("id");
                    userId = (user as { [key: string]: any }).id;
                    done();
                });
        });

        it("List user roles - should not include default role", (done) => {
            testApp
                .get(`/gateway/users/${userId}/roles`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.be.instanceOf(Array).and.lengthOf(0);
                    done();
                });
        });
    });
});
