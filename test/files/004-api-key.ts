import { expect } from "chai";
import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp;
let users;

describe("Api key test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    it("Get all api keys (no access key)", (done) => {
        testApp
            .get("/gateway/api-keys")
            .set("content-type", "application/json")
            .expect(401, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get all api keys (invalid key)", (done) => {
        testApp
            .get("/gateway/api-keys")
            .set("content-type", "application/json")
            .set("x-access-token", "fjsl;kjfs;dljflskjf")
            .expect(401, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get all api keys (0 items)", (done) => {
        testApp
            .get("/gateway/api-keys")
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(0);
                done();
            });
    });

    let tokenId;
    let tokenCode;
    it("Create new token", (done) => {
        testApp
            .post("/gateway/api-keys")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .set("x-access-token", users.user)
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                expect(res.body).to.have.property("id");
                tokenId = res.body.id;
                tokenCode = res.body.code;
                done();
            });
    });

    it("Try to create second token (403)", (done) => {
        testApp
            .post("/gateway/api-keys")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .set("x-access-token", users.user)
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get all api keys (1 item)", (done) => {
        testApp
            .get("/gateway/api-keys")
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(1);
                expect(res.body[0]).to.have.property("code");
                expect(res.body[0]).to.have.property("id", tokenId);
                expect(res.body[0]).to.have.property("deleted_at", null);
                done();
            });
    });

    it("Remove token", (done) => {
        testApp
            .delete(`/gateway/api-keys/${tokenId}`)
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Remove token (not active)", (done) => {
        testApp
            .delete(`/gateway/api-keys/${tokenId}`)
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(410, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get all api keys (1 items - non active)", (done) => {
        testApp
            .get("/gateway/api-keys")
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(1);
                expect(res.body[0]).to.have.property("code");
                expect(res.body[0]).to.have.property("id", tokenId);
                expect(res.body[0]).to.have.property("deleted_at").not.to.be.null;
                done();
            });
    });

    let tokenId2;
    let tokenCode2;
    it("Create another new token", (done) => {
        testApp
            .post("/gateway/api-keys")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .set("x-access-token", users.user)
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                expect(res.body).to.have.property("id");
                tokenId2 = res.body.id;
                tokenCode2 = res.body.code;
                done();
            });
    });

    it("Get all api keys (2 items - one active / one non active)", (done) => {
        testApp
            .get("/gateway/api-keys")
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(2);
                expect(res.body[0]).to.have.property("code", tokenCode2);
                expect(res.body[0]).to.have.property("id", tokenId2);
                expect(res.body[0]).to.have.property("deleted_at").to.be.null;

                expect(res.body[1]).to.have.property("code", tokenCode);
                expect(res.body[1]).to.have.property("id", tokenId);
                expect(res.body[1]).to.have.property("deleted_at").not.to.be.null;

                expect(tokenCode).not.to.equal(tokenCode2);
                done();
            });
    });
});
