import { expect } from "chai";
import "mocha";
import { SuperTest, Test } from "supertest";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp: SuperTest<Test>;
let users;

describe.skip("Route test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    let tokenCode;
    it("Create new token", (done) => {
        testApp
            .post("/gateway/api-keys")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .set("x-access-token", users.user)
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                expect(res.body).to.have.property("id");
                tokenCode = res.body.code;
                done();
            });
    });

    it("Get all api routes (forbidden for non admin user)", (done) => {
        testApp
            .get("/gateway/routes")
            .set("x-access-token", users.user)
            .set("content-type", "application/json")
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get all api routes", (done) => {
        testApp
            .get("/gateway/routes")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(3);
                done();
            });
    });

    it("Proxy route does not exist (should return 404 route not found)", (done) => {
        testApp.get("/light").expect(404, (err, res) => {
            expect(err).to.be.null;
            done();
        });
    });

    it("Create new route rule", (done) => {
        testApp
            .post("/gateway/routes")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    endpoint: "/light",
                    method: "GET",
                    target_url: "http://localhost:3100",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should forward message (no role)", (done) => {
        testApp
            .get("/light")
            .set("x-access-token", tokenCode)
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should forward message (error)", (done) => {
        testApp.get("/faulty").expect(400, (_err, res) => {
            expect(res.headers["content-type"]).to.equal("application/json; charset=utf-8");

            const { error_message } = JSON.parse(res.text);
            expect(error_message).to.equal("Some error");
            done();
        });
    });

    it("Should forward message (gzipped error)", (done) => {
        testApp.get("/faulty-gzipped").expect(400, (_err, res) => {
            expect(res.headers["content-encoding"]).to.equal("gzip");

            const { error_message } = JSON.parse(res.text);
            expect(error_message).to.equal("Some error");
            done();
        });
    });

    it("Should forward message", (done) => {
        testApp
            .get("/traffic-cameras")
            .set("x-access-token", tokenCode)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/traffic-cameras");
                expect(res.body).to.have.property("query");
                expect(res.body.query).to.have.property("district").and.be.instanceOf(Array).and.lengthOf(2);
                expect(res.body.query.district[0]).to.equal("praha-7");
                expect(res.body.query.district[1]).to.equal("praha-4");
                done();
            });
    });

    it("Should forward message with unaccessible district", (done) => {
        testApp
            .get("/traffic-cameras?district=praha-2")
            .set("x-access-token", tokenCode)
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should forward message with accessible district", (done) => {
        testApp
            .get("/traffic-cameras?district=praha-4")
            .set("x-access-token", tokenCode)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/traffic-cameras");
                expect(res.body).to.have.property("query");
                expect(res.body.query).to.have.property("district", "praha-4");
                done();
            });
    });

    it("Should forward message with no access", (done) => {
        testApp
            .get("/light")
            .set("x-access-token", tokenCode)
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let routeId;
    it("Create new route rule", (done) => {
        testApp
            .post("/gateway/routes")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    endpoint: "/lights",
                    method: "GET",
                    target_path: "/svetylka",
                    target_url: "http://localhost:3100",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                routeId = res.body.id;
                done();
            });
    });

    it("Create new child scope for lights", (done) => {
        testApp
            .post(`/gateway/roles/1/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [],
                    route_id: routeId,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                done();
            });
    });

    it("Should forward message with whole address translated", (done) => {
        testApp
            .get("/lights")
            .set("x-access-token", tokenCode)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/svetylka");
                expect(res.body).to.have.property("method", "GET");
                done();
            });
    });

    it("Create new parametrized route rule", (done) => {
        testApp
            .post("/gateway/routes")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    endpoint: "/lights/:id",
                    method: "GET",
                    target_path: "/svetylka/:id",
                    target_url: "http://localhost:3100",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                routeId = res.body.id;
                done();
            });
    });

    it("Create new child scope for lights", (done) => {
        testApp
            .post(`/gateway/roles/1/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [],
                    route_id: routeId,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                done();
            });
    });

    it("Should forward message with param", (done) => {
        testApp
            .get("/lights/123")
            .set("x-access-token", tokenCode)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/svetylka/123");
                expect(res.body).to.have.property("method", "GET");
                done();
            });
    });

    it("Should forward message with param", (done) => {
        testApp
            .get("/lights/ahoj")
            .set("x-access-token", tokenCode)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/svetylka/ahoj");
                expect(res.body).to.have.property("method", "GET");
                done();
            });
    });

    it("Remove route", (done) => {
        testApp
            .delete(`/gateway/routes/${routeId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Remove route", (done) => {
        testApp
            .delete(`/gateway/routes/${routeId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Should forward message (route doesn't exist anymore)", (done) => {
        testApp
            .get("/lights/123")
            .set("x-access-token", tokenCode)
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    describe("Test with ip limitation", () => {
        it("Create new route rule", (done) => {
            testApp
                .post("/gateway/routes")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        endpoint: "/lig",
                        method: "GET",
                        target_path: "/svetylka",
                        target_url: "http://localhost:3100",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    routeId = res.body.id;
                    done();
                });
        });

        it("Create new child scope for lights", (done) => {
            testApp
                .post(`/gateway/roles/1/scopes`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        route_id: routeId,
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    done();
                });
        });

        it("Should forward message with correct ip", (done) => {
            testApp
                .get("/lig")
                .set("x-access-token", tokenCode)
                .set("X-Forwarded-For", "192.123.123.1")
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("url", "/svetylka");
                    expect(res.body).to.have.property("method", "GET");
                    done();
                });
        });
    });

    describe("Test with public route", () => {
        it("Create new route rule", (done) => {
            testApp
                .post("/gateway/routes")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        endpoint: "/publiclights",
                        method: "GET",
                        public: true,
                        target_path: "/svetylka",
                        target_url: "http://localhost:3100",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    routeId = res.body.id;
                    done();
                });
        });

        let roleId;
        it("Create new role for public access", (done) => {
            testApp
                .post("/gateway/roles")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        label: "Public lights",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    roleId = res.body.id;
                    done();
                });
        });

        it("Create new scope for public lights", (done) => {
            testApp
                .post(`/gateway/roles/${roleId}/scopes`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        route_id: routeId,
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    done();
                });
        });

        it("Should forward message with incorrect ip", (done) => {
            testApp.get("/publiclights").expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/svetylka");
                expect(res.body).to.have.property("method", "GET");
                done();
            });
        });

        it("Should forward message with correct ip", (done) => {
            testApp
                .get("/publiclights")
                .set("X-Forwarded-For", "192.123.123.1")
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("url", "/svetylka");
                    expect(res.body).to.have.property("method", "GET");
                    done();
                });
        });
    });

    describe("Test with POST request", () => {
        it("Create new route rule", (done) => {
            testApp
                .post("/gateway/routes")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        endpoint: "/posting",
                        method: "POST",
                        public: true,
                        target_path: "/posting",
                        target_url: "http://localhost:3100",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    routeId = res.body.id;
                    done();
                });
        });

        let roleId;
        it("Create new role for public access", (done) => {
            testApp
                .post("/gateway/roles")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        label: "Public posting",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    roleId = res.body.id;
                    done();
                });
        });

        it("Should forward message with body data", (done) => {
            testApp
                .post("/posting")
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        value: 1,
                    })
                )
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("body");
                    expect(res.body.body).to.have.property("value", 1);
                    done();
                });
        });
    });

    describe("Test with POST request (xml file)", () => {
        it("Create new route rule", (done) => {
            testApp
                .post("/gateway/routes")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        endpoint: "/xmlko",
                        method: "POST",
                        public: true,
                        target_path: "/xmlko",
                        target_url: "http://localhost:3100",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    routeId = res.body.id;
                    done();
                });
        });

        let roleId;
        it("Create new role for public access", (done) => {
            testApp
                .post("/gateway/roles")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        label: "Public xmlko",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    roleId = res.body.id;
                    done();
                });
        });

        it("Should forward message with body data", (done) => {
            testApp
                .post("/xmlko")
                .set("content-type", "text/xml")
                .send(
                    `<m disp="http://77.93.194.81:8716/api">
    <spoj lin="999999" alias="155" spoj="4" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035"
            cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426">
        <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast>
        <zast zast="56699" stan="A" prij="" odj="11:01" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast>
        <zast zast="56699" stan="E" prij="" odj="11:02" zpoz_typ="3" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="57515" stan="A" prij="" odj="11:04" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="56700" stan="A" prij="" odj="11:06" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="57515" stan="B" prij="" odj="11:07" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="65151" stan="A" prij="" odj="11:09" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="65163" stan="A" prij="" odj="11:10" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="57511" stan="B" prij="" odj="11:11" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="57512" stan="A" prij="" odj="11:13" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="57513" stan="A" prij="" odj="11:14" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="57514" stan="B" prij="" odj="11:16" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="27902" stan="I" prij="" odj="11:18" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="27902" stan="G" prij="" odj="11:19" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
        <zast zast="56703" stan="C" prij="11:20" odj="" zpoz_typ="0" zpoz_prij="-2147483648"
            zpoz_odj="-2147483648"></zast>
    </spoj>
</m>`
                )
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("body");
                    done();
                });
        });
    });

    describe("Test with redirect", () => {
        it("Create new route rule", (done) => {
            testApp
                .post("/gateway/routes")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        endpoint: "/redir",
                        is_redirect: true,
                        method: "GET",
                        target_path: "/svetylka",
                        target_url: "http://localhost:3100",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    routeId = res.body.id;
                    done();
                });
        });

        it("Should apply proxy rules (missing authorization)", (done) => {
            testApp.get("/redir").expect(401, done);
        });

        it("Should apply proxy rules (missing scope)", (done) => {
            testApp.get("/redir").set("x-access-token", tokenCode).expect(403, done);
        });

        it("Create new child scope for redirect", (done) => {
            testApp
                .post(`/gateway/roles/1/scopes`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        route_id: routeId,
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    done();
                });
        });

        it("Should be redirected", (done) => {
            testApp.get("/redir").set("x-access-token", tokenCode).expect(307, done);
        });
    });
});
