import { expect } from "chai";
import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp;
let users;

describe("Role test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    it("List available roles (forbidden)", (done) => {
        testApp
            .get("/gateway/roles")
            .set("content-type", "application/json")
            .set("x-access-token", users.user)
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let roleId;
    it("List available roles (1 item)", (done) => {
        testApp
            .get("/gateway/roles")
            .set("content-type", "application/json")
            .set("x-access-token", users.admin)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(2);
                expect(res.body[1]).to.have.property("id");
                roleId = res.body[1].id;
                done();
            });
    });

    it("Get role detail", (done) => {
        testApp
            .get(`/gateway/roles/${roleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("label", "Kamery");
                expect(res.body).to.have.property("scopes");
                expect(res.body.scopes).to.be.instanceOf(Array).and.lengthOf(1);
                done();
            });
    });

    let trafficLightRoleId;
    it("Create new role", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Semafory",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                trafficLightRoleId = res.body.id;
                done();
            });
    });

    it("Create new role with rate limit", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "RateLimitRole",
                    rateLimit: {
                        limit: 2,
                        timewindow: 10000,
                    },
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                expect(res.body).to.have.property("rateLimit");
                done();
            });
    });

    it("Update role (Semafory)", (done) => {
        testApp
            .patch(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Semafory 2",
                })
            )
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Update role with rate limit value (Semafory)", (done) => {
        testApp
            .patch(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Semafory 2",
                    rateLimit: {
                        limit: 8,
                        timewindow: 200,
                    },
                })
            )
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get role detail (Semafory)", (done) => {
        testApp
            .get(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("label", "Semafory 2");
                expect(res.body).to.have.property("scopes");
                expect(res.body).to.have.property("rateLimit");
                expect(res.body.scopes).to.be.instanceOf(Array).and.lengthOf(0);
                done();
            });
    });

    it("Remove role (Semafory)", (done) => {
        testApp
            .delete(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Remove role (Semafory - not found)", (done) => {
        testApp
            .delete(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get role detail (Semafory - not found)", (done) => {
        testApp
            .get(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Update role (Semafory - not found)", (done) => {
        testApp
            .patch(`/gateway/roles/${trafficLightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Semafory 2",
                })
            )
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    describe("Role inheritance", () => {
        it("Remove role from user", (done) => {
            testApp
                .delete(`/gateway/users/3/roles/1`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(JSON.stringify({}))
                .expect(204, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        let parentId;
        it("Create new parent role", (done) => {
            testApp
                .post("/gateway/roles")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        label: "Semafory (Praha)",
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    parentId = res.body.id;
                    done();
                });
        });

        let scopeId;
        it("Create new scope for lights", (done) => {
            testApp
                .post(`/gateway/roles/${parentId}/scopes`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        limitations: [
                            {
                                block: true,
                                name: "district",
                                type: "in-array",
                                values: ["praha-7", "praha-4", "praha-1"],
                            },
                            {
                                block: false,
                                max: 100,
                                min: 0,
                                name: "limit",
                                type: "min-max",
                                minMaxType: "number",
                            },
                        ],
                        route_id: 1,
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    scopeId = res.body.id;
                    done();
                });
        });

        let childId;
        it("Create new child role", (done) => {
            testApp
                .post("/gateway/roles")
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        label: "Semafory (Praha - 3)",
                        parent_id: parentId,
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    childId = res.body.id;
                    done();
                });
        });

        it("Create new child scope for lights", (done) => {
            testApp
                .post(`/gateway/roles/${childId}/scopes`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(
                    JSON.stringify({
                        limitations: [
                            {
                                block: true,
                                name: "district",
                                type: "in-array",
                                values: ["praha-3"],
                            },
                            {
                                block: false,
                                default: 50,
                                max: 100,
                                min: 0,
                                name: "limit",
                                type: "min-max",
                                minMaxType: "number",
                            },
                        ],
                        route_id: 1,
                    })
                )
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("id");
                    scopeId = res.body.id;
                    done();
                });
        });

        it("Assign role to user", (done) => {
            testApp
                .put(`/gateway/users/3/roles/${childId}`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(JSON.stringify({}))
                .expect(204, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        let tokenCode;
        it("Create new token", (done) => {
            testApp
                .post("/gateway/api-keys")
                .set("content-type", "application/json")
                .send(JSON.stringify({}))
                .set("x-access-token", users.user)
                .expect(201, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("code");
                    expect(res.body).to.have.property("id");
                    tokenCode = res.body.code;
                    done();
                });
        });

        it("Should forward message with accessible district", (done) => {
            testApp
                .get("/traffic-cameras?district=praha-3")
                .set("x-access-token", tokenCode)
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("url", "/traffic-cameras");
                    expect(res.body).to.have.property("query");
                    expect(res.body.query).to.have.property("district", "praha-3");
                    expect(res.body.query).to.have.property("limit", "50");
                    done();
                });
        });

        it("Reassign role to user", (done) => {
            testApp
                .put(`/gateway/users/3/roles/${parentId}`)
                .set("x-access-token", users.admin)
                .set("content-type", "application/json")
                .send(JSON.stringify({}))
                .expect(204, (err, res) => {
                    expect(err).to.be.null;
                    done();
                });
        });

        it("Should set proper limit (max)", (done) => {
            testApp
                .get("/traffic-cameras?district=praha-1")
                .set("x-access-token", tokenCode)
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("url", "/traffic-cameras");
                    expect(res.body).to.have.property("query");
                    expect(res.body.query).to.have.property("district", "praha-1");
                    expect(res.body.query).to.have.property("limit", "100");
                    done();
                });
        });
    });
});
