import { expect } from "chai";
import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp;
let users;

describe("Scope test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    let lightRoleId;
    it("Create new role", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Svetla",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                lightRoleId = res.body.id;
                done();
            });
    });

    let scopeId;
    it("Create new scope for lights", (done) => {
        testApp
            .post(`/gateway/roles/${lightRoleId}/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [
                        {
                            block: true,
                            name: "district",
                            type: "in-array",
                            values: ["praha-7", "praha-4"],
                        },
                    ],
                    route_id: 1,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                scopeId = res.body.id;
                done();
            });
    });

    it("Create new scope for lights (role not found)", (done) => {
        testApp
            .post(`/gateway/roles/666/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [
                        {
                            block: true,
                            name: "district",
                            type: "in-array",
                            values: ["praha-7", "praha-4"],
                        },
                    ],
                    route_id: 1,
                })
            )
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Get scope detail", (done) => {
        testApp
            .get(`/gateway/roles/${lightRoleId}/scopes/${scopeId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                expect(res.body).to.have.property("limitations").and.be.instanceOf(Array).and.be.lengthOf(1);
                expect(res.body.limitations[0]).to.have.property("name", "district");
                done();
            });
    });

    it("Create new scope for lights (already exist)", (done) => {
        testApp
            .post(`/gateway/roles/${lightRoleId}/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [
                        {
                            block: true,
                            name: "district",
                            type: "in-array",
                            values: ["praha-7", "praha-4"],
                        },
                    ],
                    route_id: 1,
                })
            )
            .expect(409, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Delete scope", (done) => {
        testApp
            .delete(`/gateway/roles/${lightRoleId}/scopes/${scopeId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Create new scope for lights (doesnt exist anymore)", (done) => {
        testApp
            .post(`/gateway/roles/${lightRoleId}/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [
                        {
                            block: true,
                            name: "district",
                            type: "in-array",
                            values: ["praha-3"],
                        },
                        {
                            block: false,
                            max: 100,
                            min: 0,
                            name: "limita",
                            type: "min-max",
                            minMaxType: "number",
                        },
                    ],
                    route_id: 1,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                done();
            });
    });

    let lightRoleId2;
    it("Create another role for lights", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Svetla 2",
                    parent_id: lightRoleId,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                lightRoleId2 = res.body.id;
                done();
            });
    });

    it("Create another scope for lights", (done) => {
        testApp
            .post(`/gateway/roles/${lightRoleId2}/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [
                        {
                            block: true,
                            name: "district",
                            type: "in-array",
                            values: ["praha-6"],
                        },
                    ],
                    route_id: 1,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                scopeId = res.body.id;
                done();
            });
    });

    it("Assign role to user", (done) => {
        testApp
            .put(`/gateway/users/3/roles/${lightRoleId2}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Check if there are any scope collisions", (done) => {
        testApp
            .get(`/gateway/roles/duplicates`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceof(Array).and.lengthOf(1);
                log.info(res.body);
                done();
            });
    });
});
