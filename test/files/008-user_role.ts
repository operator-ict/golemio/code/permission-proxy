import { expect } from "chai";
import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp;
let users;

describe("User role test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    let lightRoleId;
    it("Create new role", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Svetla",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                lightRoleId = res.body.id;
                done();
            });
    });

    it("Assign role to user", (done) => {
        testApp
            .put(`/gateway/users/3/roles/${lightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("List user roles", (done) => {
        testApp
            .get(`/gateway/users/3/roles`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(2);
                done();
            });
    });

    it("Remove role from user", (done) => {
        testApp
            .delete(`/gateway/users/3/roles/${lightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it("Remove role from user (not found)", (done) => {
        testApp
            .delete(`/gateway/users/3/roles/${lightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(404, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });
});
