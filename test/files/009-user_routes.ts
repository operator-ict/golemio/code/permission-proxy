import { expect } from "chai";
import "mocha";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp;
let users;

describe("User route test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    it("Get route list for user", (done) => {
        testApp
            .get(`/gateway/users/3/routes`)
            .set("x-access-token", users.user)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(1);
                done();
            });
    });

    it("Get route list for user (user cannot access routes)", (done) => {
        testApp
            .get(`/gateway/users/1/routes`)
            .set("x-access-token", users.user)
            .set("content-type", "application/json")
            .expect(403, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it('Get route list for user (admin can access users" routes)', (done) => {
        testApp
            .get(`/gateway/users/3/routes`)
            .set("x-access-token", users.user)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(1);
                done();
            });
    });

    let routeId;
    it("Create new route rule", (done) => {
        testApp
            .post("/gateway/routes")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    endpoint: "/lig",
                    method: "GET",
                    target_path: "/svetylka",
                    target_url: "http://localhost:3100",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                routeId = res.body.id;
                done();
            });
    });

    let lightRoleId;
    it("Create new role", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Svetla",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                lightRoleId = res.body.id;
                done();
            });
    });

    it("Create new child scope for lights", (done) => {
        testApp
            .post(`/gateway/roles/${lightRoleId}/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [],
                    route_id: routeId,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                done();
            });
    });

    it("Assign role to user", (done) => {
        testApp
            .put(`/gateway/users/3/roles/${lightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    it('Get route list for user (admin can access users" routes)', (done) => {
        testApp
            .get(`/gateway/users/3/routes`)
            .set("x-access-token", users.user)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(2);
                expect(res.body[0]).to.have.property("endpoint", "/lig");
                expect(res.body[1]).to.have.property("endpoint", "/traffic-cameras");
                done();
            });
    });

    let tokenCode;
    it("Create new token", (done) => {
        testApp
            .post("/gateway/api-keys")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .set("x-access-token", users.user)
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                expect(res.body).to.have.property("id");
                tokenCode = res.body.code;
                done();
            });
    });

    it("Get route list for user (get routes with api key)", (done) => {
        testApp
            .get(`/gateway/users/3/routes`)
            .set("x-access-token", tokenCode)
            .set("content-type", "application/json")
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.be.instanceOf(Array).and.lengthOf(2);
                expect(res.body[0]).to.have.property("endpoint", "/lig");
                expect(res.body[1]).to.have.property("endpoint", "/traffic-cameras");
                done();
            });
    });
});
