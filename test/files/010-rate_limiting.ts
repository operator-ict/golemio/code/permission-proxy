import { expect } from "chai";
import "mocha";
import { config } from "../../src/core/config";
import { log } from "../../src/core/helpers";
import { dropTestSchema, getTestApp, initDatabase, logUsers } from "../index";

let testApp;
let users;

describe("Rate limiting test", () => {
    before(async () => {
        log.info("Wait for app to initialize proxy service.");
        await initDatabase();
        testApp = await getTestApp();
        users = await logUsers();
    });

    after(async () => {
        log.debug("Dropping test DB schema.");
        await dropTestSchema();
    });

    it("Set rate limit for user", (done) => {
        testApp
            .put(`/gateway/users/3/rate-limits`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limit: 3,
                    timewindow: 10000,
                })
            )
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let routeId;
    it("Create new route rule", (done) => {
        testApp
            .post("/gateway/routes")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    endpoint: "/lig",
                    method: "GET",
                    target_path: "/svetylka",
                    target_url: "http://localhost:3100",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                routeId = res.body.id;
                done();
            });
    });

    let lightRoleId;
    it("Create new role", (done) => {
        testApp
            .post("/gateway/roles")
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    label: "Svetla",
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                lightRoleId = res.body.id;
                done();
            });
    });

    it("Create new child scope for lights", (done) => {
        testApp
            .post(`/gateway/roles/${lightRoleId}/scopes`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    limitations: [],
                    route_id: routeId,
                })
            )
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("id");
                done();
            });
    });

    it("Assign role to user", (done) => {
        testApp
            .put(`/gateway/users/3/roles/${lightRoleId}`)
            .set("x-access-token", users.admin)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(204, (err, res) => {
                expect(err).to.be.null;
                done();
            });
    });

    let tokenCode;
    it("Create new token", (done) => {
        testApp
            .post("/gateway/api-keys")
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .set("x-access-token", users.user)
            .expect(201, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("code");
                expect(res.body).to.have.property("id");
                tokenCode = res.body.code;
                done();
            });
    });

    Array.from({ length: 3 }, () => {
        it("Should forward message (request limit)", (done) => {
            testApp
                .get("/lig")
                .set("x-access-token", tokenCode)
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.have.property("url", "/svetylka");
                    expect(res.body).to.have.property("method", "GET");
                    done();
                });
        });
    });

    it("Should forward message (rate limit exceeded)", (done) => {
        testApp
            .get("/lig")
            .set("x-access-token", tokenCode)
            .expect(429, (err, res) => {
                if (!config.redis_enable) {
                    if (res.status !== 200) {
                        expect(err).to.be.null;
                    }
                } else {
                    expect(err).to.be.null;
                }
                done();
            });
    });

    it("Should wait 10 seconds before the next request", (done) => {
        setTimeout(() => {
            done();
        }, 10500);
    });

    it("Should forward message (time limit)", (done) => {
        testApp
            .get("/lig")
            .set("x-access-token", tokenCode)
            .expect(200, (err, res) => {
                expect(err).to.be.null;
                expect(res.body).to.have.property("url", "/svetylka");
                expect(res.body).to.have.property("method", "GET");
                done();
            });
    });
});
