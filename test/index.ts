"use strict";

import * as npmRun from "npm-run";
import { Client } from "pg";
import * as request from "supertest";
import App from "../src/App";
import { config } from "../src/core/config";
import { log } from "../src/core/helpers";
import "./dummyServer";

let appStarted: boolean = false;
let app: App;

export const getTestApp = async () => {
    if (!appStarted) {
        app = new App();
        await app.start();
        appStarted = true;
    }
    return request(app.express);
};

export const clearDatabase = async () => {
    const schema = config.postgres_test_schema;

    if (config.node_env !== "test" || schema === "public") {
        throw new Error("Use test environment and configuration to clean db!");
    }

    const client = new Client({
        connectionString: config.postgres_connection,
    });

    await client.connect();
    await client.query(`DROP SCHEMA IF EXISTS ${schema} CASCADE; CREATE SCHEMA ${schema};`);
    await client.end();
};

export const dropTestSchema = async () => {
    const schema = config.postgres_test_schema;

    if (config.node_env !== "test" || schema === "public") {
        throw new Error("Use test environment and configuration to clean db!");
    }

    const client = new Client({
        connectionString: config.postgres_connection,
    });

    await client.connect();
    await client.query(`DROP SCHEMA IF EXISTS ${schema} CASCADE;`);
    await client.end();
};

export const dbMigrateWithTestData = () => {
    return new Promise((resolve, reject) =>
        npmRun.exec(`npm run migrate-db:test`, (err, _) => {
            if (err) {
                log.error(err);
                return reject(err);
            }
            resolve(null);
        })
    );
};

export const initDatabase = async () => {
    await clearDatabase();
    await dbMigrateWithTestData();
};

const userCredentials = [
    { code: "admin", email: "admin@admin.com", password: "admin" },
    { code: "user", email: "user@user.com", password: "user" },
];

export const logUsers = async () => {
    const usersObj = await Promise.all(
        userCredentials.map((credentials) => {
            return new Promise((resolve, reject) =>
                getTestApp().then((testApp) => {
                    testApp
                        .post("/gateway/login")
                        .set("content-type", "application/json")
                        .send(
                            JSON.stringify({
                                email: credentials.email,
                                "g-recaptcha-response": "123",
                                password: credentials.password,
                            })
                        )
                        .expect(200, (err, res) => {
                            if (err) {
                                return reject(err);
                            }
                            resolve({ token: res.body.token, code: credentials.code });
                        });
                })
            );
        })
    );
    return usersObj.reduce((prev: any, current: any) => {
        prev[current.code] = current.token;
        return prev;
    }, {});
};
